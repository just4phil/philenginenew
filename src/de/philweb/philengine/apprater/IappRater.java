package de.philweb.philengine.apprater;

//--- this should be implemented by a sharedPreferences object

public interface IappRater {

    public int get_AppraterDontShowAgain();
    public void set_AppraterDontShowAgain(int dontshowagain);
    
    public long get_AppraterLaunchCount();
    public void set_AppraterLaunchCount(long number);
 
    public long get_AppraterDateFirstLaunch();
    public void set_AppraterDateFirstLaunch(long number);
    
    
//--- this should be implemented by a sharedPreferences object
//--- implementation could look like this:
//    
//    @Override
//    public int get_AppraterDontShowAgain()    {
//	    return prefs.getInteger( apprater_dontshowagain, 0 );
//    }
//    
//    @Override
//    public void set_AppraterDontShowAgain(int dontshowagain)    {
//    	prefs.putInteger( apprater_dontshowagain, dontshowagain );
//    	prefs.flush();
//    }
// 
//    @Override
//    public long get_AppraterLaunchCount()    {
//	    return prefs.getLong( apprater_launch_count, 0 );
//    }
//    
//    @Override
//    public void set_AppraterLaunchCount(long number)    {
//    	prefs.putLong( apprater_launch_count, number );
//    	prefs.flush();
//    }
// 
//    @Override
//    public long get_AppraterDateFirstLaunch()    {
//	    return prefs.getLong( apprater_date_firstlaunch, 0 );
//    }
//    
//    @Override
//    public void set_AppraterDateFirstLaunch(long number)    {
//    	prefs.putLong( apprater_date_firstlaunch, number );
//    	prefs.flush();
//    }
}
