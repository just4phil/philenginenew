package de.philweb.philengine.apprater;

import com.badlogic.gdx.Gdx;

//------------------------------------------------------------------
//
// TODO: MgrAppRaterManager.dispose() funktioniert noch nicht
//
// FIXME
//
//------------------------------------------------------------------

public class MgrAppRaterManager {

	private static MgrAppRaterManager instance;
	private IappRater prefs = null;
	private boolean debug = false;
	private long date_firstLaunch = 0;
	private long launch_count = 0;
	private int dontshowagain = 0;		// if apprater hint has alrady been seen and disabled (1: apprater dialog is disabled)
	private int daysUntilPrompt = 0;
	private int launchesUntilPrompt = 0;
	private IappRaterCallback callback;
	
	private MgrAppRaterManager(IappRater prefs) {
		this.prefs = prefs;
	}
	
	//--- singleton with lazy initialization
    public static MgrAppRaterManager getInstance(IappRater prefs) {
        if(instance == null){
            instance = new MgrAppRaterManager(prefs);
            Gdx.app.log("AppRaterManager", "new MgrAppRaterManager() constructed");
        }
        return instance;
    }
	    
    //--- should be called once on every appStart in onCreate() to increment launch-counter....
    public void logAppStart() {
		date_firstLaunch = prefs.get_AppraterDateFirstLaunch();
		if (debug) Gdx.app.log("AppRaterManager", ".date_firstLaunch(): " + date_firstLaunch);
		launch_count = prefs.get_AppraterLaunchCount();
		dontshowagain = prefs.get_AppraterDontShowAgain();
		
		if (date_firstLaunch == 0) {
			prefs.set_AppraterDateFirstLaunch(System.currentTimeMillis());
			if (debug) Gdx.app.log("AppRaterManager", "first time launched -> set_AppraterDateFirstLaunch");
		}
		else {
			if (debug) Gdx.app.log("AppRaterManager", "installed since: " + ((System.currentTimeMillis() - date_firstLaunch) / 1000 / 60 / 60 / 24) + " days.");
		}
		launch_count ++;
		prefs.set_AppraterLaunchCount(launch_count);
		if (debug) Gdx.app.log("AppRaterManager", "launch_count set to: " + launch_count);
    }
    
    //--- should be called once on every appStart somewhere after logAppStart() like on main menuScreen....
    public boolean checkAppRater() {
		
    	if (prefs.get_AppraterDontShowAgain() == 0) { 		// check if apprater hint has alrady been seen and disabled
    		if (debug) Gdx.app.log("AppRaterManager", "installed since: " + ((System.currentTimeMillis() - date_firstLaunch) / 1000 / 60 / 60 / 24) + " days.");
			
			//--- check if its time to show apprater
	        if (prefs.get_AppraterLaunchCount() >= launchesUntilPrompt) {
	        	
	        	if (System.currentTimeMillis() >= prefs.get_AppraterDateFirstLaunch() + (daysUntilPrompt * 24 * 60 * 60 * 1000)) {
	        		// show apprater screen
	        		if (debug) Gdx.app.log("AppRaterManager", "show apprater dialog");
	        		if (callback != null) {
	        			prefs.set_AppraterDontShowAgain(1); 	//--- disable apprater dialog
	        			callback.openAppRaterDialog();
	        			return true;
	        		}
	        	}
				else if (debug) Gdx.app.log("AppRaterManager", "its not the time, yet to show dialog");
	        }
			else if (debug) Gdx.app.log("AppRaterManager", "did not start often enough: " + prefs.get_AppraterLaunchCount());
		}
		else if (debug) Gdx.app.log("AppRaterManager", "dialog already seen and disabled.");
    	
//    	dispose(); // fuehrt nur dazu dass mgr immer neu constructed wird
    	return false;
    }
    
    //--- this might be called when the version has upgraded and app rater shall pop up again ....
    public void resetAppRater() {
		prefs.set_AppraterDateFirstLaunch(System.currentTimeMillis());	
		prefs.set_AppraterLaunchCount(0);
		prefs.set_AppraterDontShowAgain(0);
		if (debug) Gdx.app.log("AppRaterManager", "resetAppRater -> done.");
    }
    
    //--- ATTENTION: you will have to call this on your own!!! : you can call this in your dialog method 
    public void callBackButtonYES() {
    	if (debug) Gdx.app.log("AppRaterManager", "callBackButtonYES -> called.");
    	if (callback != null) callback.callBackButtonYES();
    }
    
    //--- ATTENTION: you will have to call this on your own!!! : you can call this in your dialog method 
    public void callBackButtonNO() {
		if (debug) Gdx.app.log("AppRaterManager", "callBackButtonNO -> called.");
		if (callback != null) callback.callBackButtonNO();
    }
    
//----------------- example ----------------------------------
//	messageBox.setButton(DialogInterface.BUTTON_POSITIVE, "OK lets Rate", new OnClickListener() {
//		public void onClick (DialogInterface dialog, int which) {
//			Gdx.app.postRunnable(new Runnable() {
//			    @Override
//			    public void run() {
//			    	MgrAppRaterManager.getInstance(game.getPreferences()).callBackButtonYES();
//			    }
//			});
//			messageBox.dismiss();
//		}
//	});
    
    
    public MgrAppRaterManager setDebug(boolean debug) {
    	this.debug = debug;
    	return this;
    }
    
    //--- IMPORTANT: set this!!
    public MgrAppRaterManager setDaysUntilPrompt(int days) {
    	this.daysUntilPrompt = days;
    	return this;
    }
    
    //--- IMPORTANT: set this!!
    public MgrAppRaterManager setLaunchesUntilPrompt(int launches) {
    	this.launchesUntilPrompt = launches;
    	return this;
    }
    
    //--- IMPORTANT: set this!!
    public MgrAppRaterManager setCallback(IappRaterCallback cb) {
    	this.callback = cb;
    	return this;
    }
    
    // call this after app rater has been shown to free ressources
//    public void dispose() {
//    	if (debug) Gdx.app.log("AppRaterManager", "disposed.");
//    	instance = null;
//    	prefs = null;
//    }
}
