package de.philweb.philengine.assetservice;

import java.util.ArrayList;

import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.audio.Music;

import de.philweb.philengine.common.ResolutionFileResolverChooser.ChooserStrategy;
import de.philweb.philengine.common.ResolutionFileResolverChooser.Resolution;

public class AssetGroup {

	protected int groupID;
	protected ArrayList<AssetLoadingTask> loadingTasks;
	protected IfinishedLoadingCallback callback;
	
//	public AssetGroup(int groupID, Resolution[] resolutions, ChooserStrategy chooserStrategy, IfinishedLoadingCallback callback) {
//		this(groupID, resolutions, chooserStrategy);
//		setCallback(callback);
//	}
//	
//	public AssetGroup(int groupID, Resolution[] resolutions, ChooserStrategy chooserStrategy) {
//		this.groupID = groupID;
//		this.resolutions = resolutions;
//		this.chooserStrategy = chooserStrategy;
//		loadingTasks = new ArrayList<AssetLoadingTask>();
//	}
	
	public AssetGroup(int groupID, IfinishedLoadingCallback callback) {
		this(groupID);
		setCallback(callback);
	}
	
	public AssetGroup(int groupID) {
		this.groupID = groupID;
		loadingTasks = new ArrayList<AssetLoadingTask>();
	}
	
	public AssetGroup addTextureAtlasLoaderTask(int taskID, String path) {
		addTextureAtlasLoaderTask(taskID, path, null, null);
		return this;
	}
	public AssetGroup addTextureAtlasLoaderTask(int taskID, String path, Resolution[] resolutions, ChooserStrategy chooserStrategy) {
		AssetLoadingTask task = new AssetLoadingTask(AssetLoadingTask.TextureAtlasLoaderTask, taskID, path);
		task.setResolutionsResolver(resolutions, chooserStrategy);
		loadingTasks.add(task);
		return this;
	}
	public AssetGroup addTextureLoaderTask(int taskID, String path, TextureParameter param) {
		addTextureLoaderTask(taskID, path, param, null, null);
		return this;
	}
	public AssetGroup addTextureLoaderTask(int taskID, String path, TextureParameter param, Resolution[] resolutions, ChooserStrategy chooserStrategy) {
		AssetLoadingTask task = new AssetLoadingTask(AssetLoadingTask.TextureLoaderTask, taskID, path);
		task.setResolutionsResolver(resolutions, chooserStrategy);
		task.setTextureParameter(param);
		loadingTasks.add(task);
		return this;
	}
	public AssetGroup addOggSoundLoaderTask(int taskID, String path, ArrayList<String> filenames) {
		AssetLoadingTask task = new AssetLoadingTask(AssetLoadingTask.OggSoundLoaderTask, taskID, path);
		task.setFilenameArray(filenames);
		loadingTasks.add(task);
		return this;
	}
	public AssetGroup addOggMusicLoaderTask(int taskID, String path, ArrayList<String> filenames) {
		AssetLoadingTask task = new AssetLoadingTask(AssetLoadingTask.OggMusicLoaderTask, taskID, path);
		task.setFilenameArray(filenames);
		loadingTasks.add(task);
		return this;
	}	
	public AssetGroup addSpineLoaderTask(int taskID, String spineFile, String atlasDir, Resolution[] resolutions, ChooserStrategy chooserStrategy) {
		AssetLoadingTask task = new AssetLoadingTask(AssetLoadingTask.SpineLoaderTask, taskID, spineFile);
		task.setResolutionsResolver(resolutions, chooserStrategy);
		task.setAtlasDirPath(atlasDir);		
		loadingTasks.add(task);
		return this;
	}
	
	// FIXME:  skinfile haengt ab vom atlas der von der resolution abhaengt!!
	public AssetGroup addSkinLoaderTask(int taskID, String skinFile) {
		AssetLoadingTask task = new AssetLoadingTask(AssetLoadingTask.SkinLoaderTask, taskID, skinFile);	
		loadingTasks.add(task);
		return this;
	}	
	public AssetGroup setCallback(IfinishedLoadingCallback callback) {
		this.callback = callback;
		return this;
	}
	public void clearCallback() {
		callback = null;
	}
}
