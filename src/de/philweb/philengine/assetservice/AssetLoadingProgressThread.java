package de.philweb.philengine.assetservice;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;

public class AssetLoadingProgressThread extends Thread implements Runnable {

	AssetManager assetManager;
	AssetService assetService;
	IfinishedLoadingCallback callback;
	boolean alreadyCalled = false; // call only once! 
	String resolvedDirectory;
	
	public AssetLoadingProgressThread(AssetService assetService, IfinishedLoadingCallback callback, String resolvedDirectory) {
		this.assetService = assetService;
		this.callback = callback;
		this.assetManager = assetService.getAssetManager();
		this.resolvedDirectory = resolvedDirectory;
	}
	
	@Override
	public void run() {
		Thread thisThread = Thread.currentThread();
		while (assetService.progressThread == thisThread) {		// VERY IMPORTANT!!! the thread stops working when the gameworld.dispose() sets it to null !!
//			the system might think that the programm is caught forever in a while loop and therefore the program must have crashed! 
//			let the thread sleep so that the system knows the programm hasnt crashed
			
			try {		
				Thread.sleep(20);	//---- workaround to let java have the chance for garbage collection (else: thread might be terminated by system: "spin on suspend" when system thinks the programm has crashed)
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			The basic problem is that Dalvik is a safe-point suspend VM, and uses "stop the world" garbage collection. 
//			This means that, for the GC to operate,	it has to wait for all threads to reach a point where it can be sure
//			that they won't be altering the heap.
//			if the thread isn't responding to the GC thread's request to suspend the system might think the thread has crashed
			//----------------------------------------------
			
			assetService.progress = assetManager.getProgress();
			
		      Gdx.app.postRunnable(new Runnable() {
		          @Override
		          public void run() {
		        	  if(assetManager.update() == true) {
		        		  assetService.progress = 1f;
		        		  assetService.progressThread = null;	// terminate thread
		        		  if (alreadyCalled == false && callback != null) callback.onFinishedLoading(resolvedDirectory);
		        		  alreadyCalled = true;
		        	  }
		          }
		       });
		}
	}
}
