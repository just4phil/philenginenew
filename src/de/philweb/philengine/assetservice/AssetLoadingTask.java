package de.philweb.philengine.assetservice;

import java.util.ArrayList;

import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;

import de.philweb.philengine.common.ResolutionFileResolverChooser.ChooserStrategy;
import de.philweb.philengine.common.ResolutionFileResolverChooser.Resolution;

public class AssetLoadingTask {

	public static final int UNDEFINED			 	= 0;
	public static final int TextureAtlasLoaderTask 	= 1;
	public static final int TextureLoaderTask 		= 2;
	public static final int OggSoundLoaderTask 		= 3;
	public static final int OggMusicLoaderTask 		= 4;
	public static final int SkinLoaderTask 			= 5;
	public static final int BitmapFontLoaderTask 	= 6;
	public static final int SpineLoaderTask 		= 7;
	
	int typeID = UNDEFINED;
	int taskID;
	String filePath;
	String dirPath;
	ArrayList<String> filenames;
	TextureParameter param;	// might be NULL!
	protected Resolution[] resolutions;
	protected ChooserStrategy chooserStrategy;
	
	public AssetLoadingTask(int typeID, int taskID, String filePath) {
		this.typeID = typeID;
		this.taskID = taskID;
		this.filePath = filePath;
	}

	public AssetLoadingTask setResolutionsResolver(Resolution[] resolutions, ChooserStrategy chooserStrategy) {
		this.resolutions = resolutions;
		this.chooserStrategy = chooserStrategy;
		return this;
	}
	
	public AssetLoadingTask setFilenameArray(ArrayList<String> filenames) {
		this.filenames = filenames;
		return this;
	}
	
	// used for spineLoading that needs spineFile (json) and imageatlas (which must be resolved too)
	public AssetLoadingTask setAtlasDirPath(String dirPath) {
		this.dirPath = dirPath;
		return this;
	}
	
	public AssetLoadingTask setTextureParameter(TextureParameter param) {
		this.param = param;
		return this;
	}
}
