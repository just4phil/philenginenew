package de.philweb.philengine.assetservice;

public interface IfinishedLoadingCallback {

	public void onFinishedLoading(String directory);
}
