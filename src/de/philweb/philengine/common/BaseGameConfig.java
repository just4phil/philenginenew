package de.philweb.philengine.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;

public abstract class BaseGameConfig {

	protected Game game;
	protected final int virtualWidthHUD;
	protected final int virtualHeightHUD;
	protected final float aspectRatio;
	protected final float virtualPxPerMtr;
	protected float REAL_PXPERMTR;
	protected float virtualWidthGAME;
	protected float virtualHeightGAME;
	
	protected int virtualTILESIZE;
	protected int TILEDMAPSIZEINTILESX;
	protected int TILEDMAPSIZEINTILESY;
	protected float TILESIZE_MTR;
	protected float PLAYFIELDWIDTH;
	protected float PLAYFIELDHEIGHT;
	protected float PLAYFIELDOFFSETX;
	protected float PLAYFIELDOFFSETY;
	
	public BaseGameConfig(Game game, int virtual_WIDTH_HUD, int virtual_HEIGHT_HUD, float AspectRATIO, float virtual_PXPERMTR) {
		this.game = game;
		this.virtualWidthHUD = virtual_WIDTH_HUD;
		this.virtualHeightHUD = virtual_HEIGHT_HUD;
		this.aspectRatio = AspectRATIO;
		this.virtualPxPerMtr = virtual_PXPERMTR;
		
		calculateGameWidth(virtualWidthHUD / virtualPxPerMtr); 	
	}
		
	public void calculateGameWidth(float WIDTH_GAME) {
		virtualWidthGAME = WIDTH_GAME; 
		virtualHeightGAME = virtualWidthGAME / aspectRatio;	
		REAL_PXPERMTR = virtualWidthHUD / virtualWidthGAME;
		if (game.getDebugMode()) {
			Gdx.app.log("VIRTUAL_WIDTH_GAME", "" + virtualWidthGAME);
			Gdx.app.log("VIRTUAL_HEIGHT_GAME", "" + virtualHeightGAME);
			Gdx.app.log("VIRTUAL_PXPERMTR", "" + virtualPxPerMtr);
			Gdx.app.log("REAL_PXPERMTR", "" + REAL_PXPERMTR);
		}
	}
	
	/** calculateTilesAndPlayfield 
	 * use this method if playfield is more or less the same size as the virtual width game
	 */
	public void calculateTilesAndPlayfield(int VIRTUAL_TILESIZE) {
		int TILESX = (int) ((virtualWidthGAME * virtualPxPerMtr) / VIRTUAL_TILESIZE);
		int TILESY = MathUtils.round(TILESX / 1.807692307692308f);	// FIXME: woher kommt dieser faktor ????
		calculateTilesAndPlayfield(VIRTUAL_TILESIZE, TILESX, TILESY);
	}
	
	/** calculateTilesAndPlayfield 
	 * use this method if playfield is significantly smaller than virtual width game
	 */
	public void calculateTilesAndPlayfield(int VIRTUAL_TILESIZE, int TILESX, int TILESY) {
		this.virtualTILESIZE = VIRTUAL_TILESIZE;
		this.TILEDMAPSIZEINTILESX = TILESX;
		this.TILEDMAPSIZEINTILESY = TILESY;
		TILESIZE_MTR = virtualTILESIZE / virtualPxPerMtr;
		PLAYFIELDWIDTH = TILEDMAPSIZEINTILESX * virtualTILESIZE / virtualPxPerMtr;
		PLAYFIELDHEIGHT = TILEDMAPSIZEINTILESY * virtualTILESIZE / virtualPxPerMtr;
		PLAYFIELDOFFSETX = (virtualWidthGAME - PLAYFIELDWIDTH) * 0.5f;
		PLAYFIELDOFFSETY = (virtualHeightGAME - PLAYFIELDHEIGHT) * 0.5f;
		if (game.getDebugMode()) {
			Gdx.app.log("TILEDMAPSIZEINTILES X", "" + TILEDMAPSIZEINTILESX);
			Gdx.app.log("TILEDMAPSIZEINTILES Y", "" + TILEDMAPSIZEINTILESY);
			Gdx.app.log("PLAYFIELDWIDTH", "" + PLAYFIELDWIDTH);
			Gdx.app.log("PLAYFIELDOFFSET X", "" + PLAYFIELDOFFSETX);
			Gdx.app.log("PLAYFIELDOFFSET Y", "" + PLAYFIELDOFFSETY);
		}
	}
	
	//--- call this if you change virtual game with during gameplay
	public void updateCameraView() {
		game.getCameraManager().setupGAMEcam(virtualWidthGAME, virtualHeightGAME);
		game.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());	// irgendwie noetig fuer korrektes rendering (sonst fehlen tanks)
	}
	
	public void setGameReference(Game game) {
		this.game = game;
	}
	
	public int getVIRTUAL_WIDTH_HUD () {
		return virtualWidthHUD;
	}
	public int getVIRTUAL_HEIGHT_HUD () {
		return virtualHeightHUD;
	}
	public float getVIRTUAL_WIDTH_GAME () {
		return virtualWidthGAME;
	}
	public float getVIRTUAL_HEIGHT_GAME () {
		return virtualHeightGAME;
	}
	public float getTILESIZE_MTR() {
		return TILESIZE_MTR;
	}
	public int getVIRTUAL_TILESIZE () {
		return virtualTILESIZE;
	}
	public int getTILEDMAPSIZEINTILESX () {
		return TILEDMAPSIZEINTILESX;
	}
	public int getTILEDMAPSIZEINTILESY () {
		return TILEDMAPSIZEINTILESY;
	}
	public float getPLAYFIELDOFFSETX () {
		return PLAYFIELDOFFSETX;
	}
	public float getPLAYFIELDOFFSETY () {
		return PLAYFIELDOFFSETY;
	}
	public float getPLAYFIELDWIDTH () {
		return PLAYFIELDWIDTH;
	}
	public float getPLAYFIELDHEIGHT () {
		return PLAYFIELDHEIGHT;
	}
	public float getVirtualPxPerMtr () {
		return virtualPxPerMtr;
	}
	public float getREAL_PXPERMTR() {
		return REAL_PXPERMTR;
	}
//	public float getGROUNDBODYOFFSETX () {
//		return GROUNDBODYOFFSETX;
//	}
//	public float getGROUNDBODYOFFSETY () {
//		return GROUNDBODYOFFSETY;
//	}
	
	
	
//	public void copyValuesToGame() {
//		game.VIRTUAL_WIDTH_HUD 		= VIRTUAL_WIDTH_HUD;
//		game.VIRTUAL_HEIGHT_HUD 	= VIRTUAL_HEIGHT_HUD;
//		game.VIRTUAL_PXPERMTR 		= VIRTUAL_PXPERMTR;
//		game.VIRTUAL_TILESIZE 		= VIRTUAL_TILESIZE;
//		game.GROUNDBODYOFFSETX 		= GROUNDBODYOFFSETX;
//		game.GROUNDBODYOFFSETY 		= GROUNDBODYOFFSETY;
//		game.REAL_PXPERMTR 			= REAL_PXPERMTR; 
//		
//		game.VIRTUAL_WIDTH_GAME 	= this.VIRTUAL_WIDTH_GAME;
//		game.VIRTUAL_HEIGHT_GAME 	= this.VIRTUAL_HEIGHT_GAME;
//		game.TILEDMAPSIZEINTILESX 	= this.TILEDMAPSIZEINTILESX;
//		game.TILEDMAPSIZEINTILESY 	= this.TILEDMAPSIZEINTILESY;
//		game.PLAYFIELDWIDTH 			= this.PLAYFIELDWIDTH;
//		game.PLAYFIELDHEIGHT 		= this.PLAYFIELDHEIGHT;
//		game.PLAYFIELDOFFSETX 		= this.PLAYFIELDOFFSETX;
//		game.PLAYFIELDOFFSETY 		= this.PLAYFIELDOFFSETY;
//		game.AItankPathUpdateTime	= this.AItankPathUpdateTime;
//	}
}
