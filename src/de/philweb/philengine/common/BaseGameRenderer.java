package de.philweb.philengine.common;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

import de.philweb.philengine.gameobjects.BaseGameWorld;
import de.philweb.philengine.maps.MapService;
import de.philweb.philengine.particles2d.MgrParticleFxManager;

public abstract class BaseGameRenderer implements Disposable  {
	
	public Game game;
	public MapService map;
	public BaseGameWorld baseGameWorld;
	public SpriteBatch batch;
	public MgrParticleFxManager _particleFxManager;
	
	public BaseGameRenderer(Game game, MapService map, BaseGameWorld baseGameWorld) {
		this.game = game;
		this.map = map;
		this.baseGameWorld = baseGameWorld;
		this.batch = game.getSpritebatch();
		map.setGameRenderer(this);
	}
	
//	public MapService getMap() {
//		return map;
//	}
	public BaseGameWorld getGameWorld() {
		return baseGameWorld;
	}
	public abstract void render();
}
