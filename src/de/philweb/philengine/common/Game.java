
package de.philweb.philengine.common;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.SnapshotArray;

import de.philweb.philengine.apprater.MgrAppRaterManager;
import de.philweb.philengine.assetservice.AssetService;
import de.philweb.philengine.controllers.MgrControllerConfigManager;
import de.philweb.philengine.listeners.PeDialogEvent;
import de.philweb.philengine.listeners.PeEventListener;
import de.philweb.philengine.listeners.PhilEngineEvent;
import de.philweb.philengine.particles2d.MgrParticleFxManager;
import de.philweb.philengine.screensmenus.AbstractScreen;
import de.philweb.philengine.screensmenus.AssignControllerScreen;
import de.philweb.philengine.spawnsystem.MgrSpawnManager;

public abstract class Game implements ApplicationListener {

// public static enum Platform {
// Windows, Mac, Unix, Solaris, Other, Android, Ouya, Linux;
// }

	// ----- app stores -------------------------
	public static final int APPSTORE_UNDEFINED = 0;
	public static final int APPSTORE_GOOGLE = 1;
	public static final int APPSTORE_OUYA = 2;
	public static final int APPSTORE_AMAZON = 3;
	public static final int APPSTORE_DESKTOP = 5;

	// -------------------------------------------
	public static final int DESKTOP = 1; // = OS
	public static final int ANDROID = 2; // = OS + Device
	public static final int OUYA = 3; // = device
	public static final int AMAZONFIRETV = 4; // = device
	private int isRunningOnOS = 0; // can be android or desktop
	private int isRunningOnDevice = 0; // can be OUYA, android or desktop

	private int isAppStore = APPSTORE_UNDEFINED;
	private boolean adminMode = false; // set to false!!
	private boolean debugMode = false; // set to false!!
	private boolean hasBeenPurchased = false;
	// -------------------------------------------------

	public static final float OUYAsafeZoneCamZoom = 1.08f;
	
	protected BaseGameConfig gameConfig;
	protected GamePreferences _gamePreferences;
	protected static PlatformResolver m_platformResolver = null;
	private final SnapshotArray<PeEventListener> listeners = new SnapshotArray<PeEventListener>(0); // for eventListeners
	
	// ---- managers -------------------
	protected AssetService _assetService;
	protected MgrCameraManager _cameraManager;
	protected MgrControllerConfigManager _controllerConfigManager;
	protected MgrMusicManager _musicManager;
	protected MgrSpawnManager _spawnManager = null;
	protected MgrAppRaterManager _appraterManager = null;
	protected MgrParticleFxManager _particleFxManager = null;
//	protected MgrLanguageManager _langmgr = null;
	protected LanguageManager _langmgr = null;
	
	int gameModeID;
	int gameTypeID;
	
	//--- for google analytics (only on android platforms)
	long usageDurationStart;
	private String clientID;
	public boolean analyticsHasBeenChecked = false;	// used by loadingScreen

	// ------- menus ------------
	public boolean activateMenuSwitches = true;
	AbstractScreen screen;
	public AbstractScreen temporarilySavedScreenWhileControllerAssignScreenIsOn;
	float delta;	
	
	public long start; // just 4 debugging time metrics
	// ==================================================

	public void setScreen (AbstractScreen incomingScreen) {

		if (screen != null) screen.dispose();	// very important!!
		screen = incomingScreen;

		screen.doAfterSetScreen();	// this is helpfull because setScreen is often called with new Screens() that have to be constructed. this makes sure that the screen-constructor has finished
		
		// this.screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}
	
	public abstract AbstractScreen getStartScreen ();

	public abstract void configureStuffAfterAssetLoading ();

	@Override
	public void create () {
		usageDurationStart = System.currentTimeMillis();
		
		//--- set null-platformResolver to avoid nullpointers in cases where there is no resolver used
		setPlatformResolver(new PlatformResolverSTUB(this));
		
		// ----- detect OS --------------
		detectOperatingSystem();
		
		// ---- preferences -------------------
		getPreferences().checkSafeZone();	// ---- initialize safe zone -----------------
		
		// initializeText();

		// ---- configure platformspecific options ------
//		if (getPlatformResolver() != null) getPlatformResolver().configureOptions();	// check unnoetig da stub-resolver

		// ----- start game --------------
		if (detectControllers() == false) Gdx.app.exit(); // wenn kein ouya controller gefunden wird soll das game nicht crashen!
		else screen = getStartScreen();
	}

	// private void initializeText() {
	// I18NBundle b = lm.getCurrentBundle();
	// title = b.get("bookTitle");
	// introduction = b.format("introduction", 81);
	// body = b.get("body");
	//
	// viewport.getCamera().project(point.set(SCENE_WIDTH * .70f, 0, 0));
	// textWidth = point.x;
	// viewport.getCamera().project(point.set(SCENE_WIDTH * .15f, 0, 0));
	// textXOrigin = point.x;
	// }
	
	// private void translate() {
	// if(lm.getCurrentLanguage().compareTo("english") == 0)
	// lm.setCurrentLanguage("spanish");
	// else
	// lm.setCurrentLanguage("english");
	//
	// initializeText();
	// }
	
	
	@Override
	public void render () {
		delta = Gdx.graphics.getDeltaTime();
		screen.update(delta);	// TODO: hat hier mal gecrasht!? (2015-02-01)
		_cameraManager._stage.act(delta);	// TODO: hat hier mal gecrasht!? (2015-01-28)

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		screen.present();

		_cameraManager.useHUDCam();
		_cameraManager._stage.draw();
	}

	@Override
	public void resize (int width, int height) {
		_cameraManager.resize(width, height);
	}

	// ===== detect operating system and device and store ==========================
	public void detectOperatingSystem () {
		switch (isAppStore) {

		case APPSTORE_UNDEFINED:
			Gdx.app.log("ERROR!", "APPSTORE_UNDEFINED");
			break;

		case APPSTORE_GOOGLE:
			setRunningOS(ANDROID);
			setRunningDevice(ANDROID); // first: assume that we are on an android device
			// deviceName = "Android"; // wird fuer die help-screens benoetigt
			break;

		case APPSTORE_OUYA:
			setRunningOS(ANDROID);
			setRunningDevice(OUYA);
			// deviceName = "OUYA"; // wird fuer die help-screens benoetigt
			break;

		case APPSTORE_AMAZON:
			setRunningOS(ANDROID);
			setRunningDevice(AMAZONFIRETV); // first: assume that we are on an android device
			// deviceName = "Android"; // wird fuer die help-screens benoetigt
			break;

		case APPSTORE_DESKTOP:
			setRunningOS(DESKTOP);
			setRunningDevice(DESKTOP);
			// deviceName = "Desktop"; // wird fuer die help-screens benoetigt
			break;
		}
	}
	// -------------------------------------------

	public boolean detectControllers () {

//		_controllerConfigManager = new MgrControllerConfigManager(this, arrayCommands, defaultControllerConfigs); // --- nicht mehr noetig wegen singleton initialization
		
		// --- contrconfmgr need dependency injection for onscreenJoystick after loading of assets!
//		if (_controllerConfigManager != null) {
			_controllerConfigManager.initializeControllers(); // im loadingasynchscreen ist das zu spaet, da der bereits einen inputcontroller benoetigt
	
			// ---- fire tv kann auch mit fire tv remote bedient werden
			if (isAppStore == APPSTORE_AMAZON) {
	
				// wenn kein ouya controller gefunden wird soll das game nicht crashen!
				if (_controllerConfigManager.getControllerList().getKnownControllerCounter() == 1) { // controller 1 on fire tv is the
					// REMOTE! ( not a real controller)
					getPlatformResolver().showToast("Please connect a controller and restart the game!", false);
				}
			}
	
			// ---- crash vermeiden wenn kein ouya-constroller gefunden werden konnte
			if (isAppStore == APPSTORE_OUYA) {
	
				// wenn kein ouya controller gefunden wird soll das game nicht crashen!
				if (_controllerConfigManager.getControllerList().getKnownControllerCounter() == 0) { // getOUYAcontrollerCounter
	
					getPlatformResolver().showToast("Please connect a controller and restart the game!", false);
	
					// ----- thread schalfen lassen da man sonst den toast nicht lesen kann
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
					}
					// -------------------------------------------
	
					return false; // wenn kein ouya controller gefunden wird soll das game nicht crashen!
				}
			}
			return true;
//		}
//		else {
//			return false;
//		}
	}

	// ---- listeners fuer feedback nach dialogbuttons
	public boolean addListener (PeEventListener listener) {
		if (!listeners.contains(listener, true)) {
			listeners.add(listener);
			return true;
		}
		return false;
	}
	public boolean removeListener (PeEventListener listener) {
		return listeners.removeValue(listener, true);
	}
	public Array<PeEventListener> getListeners () {
		return listeners;
	}
	/** Removes all listeners on this actor. */
	public void clearListeners () {
		listeners.clear();
	}

	/** Notifies this actor's listeners of the event. The event is not propagated to any parents. Before notifying the listeners, */
	public boolean notify (PhilEngineEvent event) {
		
		if (((PeDialogEvent)event).getType() == PeDialogEvent.Type.unassignedMenuController) {
			temporarilySavedScreenWhileControllerAssignScreenIsOn = screen;
			setScreen(new AssignControllerScreen(this, ((PeDialogEvent)event).getCallback() ));
			return true;
		}
		else {
			if (listeners.size == 0) return event.isCancelled();
			listeners.begin();
			for (int i = 0, n = listeners.size; i < n; i++) {
				PeEventListener listener = listeners.get(i);
				if (listener.handle(event)) {
					event.handle();
				}
			}
			listeners.end();
			return event.isCancelled();
		}
	}
	// ----------------------------------------

	public void toggleHasBeenPurchased () {
		if (hasBeenPurchased) {
			hasBeenPurchased = false;
		} else {
			hasBeenPurchased = true;
		}
	}
	public boolean isPurchased () {
		return hasBeenPurchased;
	}
	public void setHasBeenPurchased (boolean mode) {
		hasBeenPurchased = mode;
	}
	public void enableHasBeenPurchased () {
		hasBeenPurchased = true;
		m_platformResolver.showToast("Thanks for purchasing and supporting our work!", false);
		// if (PurchaseSystem.hasManager()) PurchaseSystem.dispose();	// never call PurchaseSystem.hasManager() because it binds openIAB via reflection + dont dispose
	}
	public void disableHasBeenPurchased () {
		hasBeenPurchased = false;
	}

	//---- for google analytics -------------------
	
	public String getClientID() {
		return clientID; 
	}
	public void setClientID(String clientID) {
		this.clientID = clientID; 
	}
	// -----------------------------------------

	// inject dependency for gameConfig initialization
	public void setGameConfig(BaseGameConfig config) {
		this.gameConfig = config;
		this.gameConfig.setGameReference(this);
	}
	public BaseGameConfig getGameConfig() {
		return gameConfig;
	}
	//-------------------------------------------
	
	public PlatformResolver getPlatformResolver () {
		return m_platformResolver;
	}
	public static void setPlatformResolver (PlatformResolver platformResolver) {
		m_platformResolver = platformResolver;
	}
	//----------------------------------------------
	
	public int getVIRTUAL_WIDTH_HUD () {
		return gameConfig.virtualWidthHUD;
	}
	public int getVIRTUAL_HEIGHT_HUD () {
		return gameConfig.virtualHeightHUD;
	}
	public float getVIRTUAL_WIDTH_GAME () {
		return gameConfig.virtualWidthGAME;
	}
	public float getVIRTUAL_HEIGHT_GAME () {
		return gameConfig.virtualHeightGAME;
	}
	public int getVIRTUAL_TILESIZE () {
		return gameConfig.virtualTILESIZE;
	}
	public int getTILEDMAPSIZEINTILESX () {
		return gameConfig.TILEDMAPSIZEINTILESX;
	}
	public int getTILEDMAPSIZEINTILESY () {
		return gameConfig.TILEDMAPSIZEINTILESY;
	}
	public float getPLAYFIELDOFFSETX () {
		return gameConfig.PLAYFIELDOFFSETX;
	}
	public float getPLAYFIELDOFFSETY () {
		return gameConfig.PLAYFIELDOFFSETY;
	}
//	public float getGROUNDBODYOFFSETX () {
//		return gameConfig.GROUNDBODYOFFSETX;
//	}
//	public float getGROUNDBODYOFFSETY () {
//		return gameConfig.GROUNDBODYOFFSETY;
//	}
	//----------------------------------------------
	
	
	public MgrMusicManager getMusicManager() {		
		if (_musicManager == null) {
			_musicManager = new MgrMusicManager(this);
		}
		return _musicManager;
	}	
	public MgrCameraManager getCameraManager() {		
		if (_cameraManager == null) {
			_cameraManager = new MgrCameraManager(this);
		}
		return _cameraManager;
	}
	public MgrSpawnManager getSpawnManager() {
		if (_spawnManager == null) {
			_spawnManager = new MgrSpawnManager();
		}
		return _spawnManager;
	}
	public MgrParticleFxManager getParticleFxManager() {
		if (_particleFxManager == null) {
			_particleFxManager = new MgrParticleFxManager(getDebugMode());
		}
		return _particleFxManager;
	}
//	public MgrLanguageManager getLanguageManager() {
//		if (_langmgr == null) {
//			_langmgr = new MgrLanguageManager();
//		}
//		return _langmgr;
//	}
	
	
	public LanguageManager initializeLanguageManager(String languagesName) {
		if (_langmgr == null) {
			_langmgr = new LanguageManager(languagesName);
		}
		return _langmgr;
	}		
	
	public LanguageManager getLanguageManager() {
		return _langmgr;
	}
	
//	public AssetService getAssetService () {
//		return _assetService = AssetService.getInstance(this);
//	}
	public AssetService getAssetService() {
		if (_assetService == null) {
			_assetService = new AssetService(this);
		}
		return _assetService;
	}
	public MgrControllerConfigManager getControllerConfigManager() {
		return _controllerConfigManager = MgrControllerConfigManager.getInstance(this);
	}
	
	public GamePreferences getPreferences() {
		if (_gamePreferences == null) {
			_gamePreferences = new GamePreferences(this);
		}
		return _gamePreferences;
	}
			
//	MgrAppRaterManager is a singleton => reason: easier instantiation in AndroidBaseResolver!!!
//	public MgrAppRaterManager getAppRaterManager(IappRater prefs) {		
//		if (_appraterManager == null) {
//			_appraterManager = new MgrAppRaterManager(prefs);
//		}
//		return _appraterManager;
//	}
	//----------------------------------------------
	
	public int getGameModeID() {
		return gameModeID;
	}
	public void setGameModeID(int gameModeID) {
		this.gameModeID = gameModeID;
	}
	public int getGameTypeID() {
		return gameTypeID;
	}
	public void setGameTypeID(int gameTypeID) {
		this.gameTypeID = gameTypeID;
	}
	
	public Stage getStage () {
		return _cameraManager._stage;
	}
	public SpriteBatch getSpritebatch () {
		return _cameraManager._batch;
	}
	public ModelBatch getModelBatch () {
		return _cameraManager._modelBatch;
	}

	//	public float getPPM () {
	//		return pixelPerMeter;
	//	}
	
	//
	// public float meterToPixels(float meter) { // TODO: Besser als int ausgeben?
	// return (float)meter * pixelPerMeter;
	// }
	// public float pixelsToMeter(float pixels) {
	// return (float)pixels / pixelPerMeter;
	// }

	//------------------------------------
		
	public void setAdminAndDebugMode (boolean isAdminMode, boolean isDebugMode) {
		this.adminMode = isAdminMode;
		this.debugMode = isDebugMode;
	}
	public boolean getDebugMode() {
		return debugMode;
	}
	public boolean getAdminMode() {
		return adminMode;
	}
	public void setAppStore (int isAppStore) {
		this.isAppStore = isAppStore;
	}
	public int getAppStore () {
		return isAppStore;
	}
	public int getRunningOS () {
		return isRunningOnOS;
	}
	public int getRunningDevice () {
		return isRunningOnDevice;
	}
	public void setRunningOS (int osID) {
		isRunningOnOS = osID;
		// if (Tanks.debugMode == true) Gdx.app.log("isRunningOnOS", "DESKTOP");
	}
	public void setRunningDevice (int deviceID) {
		isRunningOnDevice = deviceID;
		// if (Tanks.debugMode == true) Gdx.app.log("isRunningOnDevice", "DESKTOP");
	}	
	
//	public Console getIngameConsole(Skin skin, CommandExecutor cExec, int key) {
//		if (console == null) {
//			console = new Console(skin);
//			console.setCommandExecutor(cExec);
//			console.setKeyID(key);
//		}
//		return console;
//	}	
	
	public void exitGame () {
		Gdx.app.exit();
	}
	// ------------------------------------------------------

	@Override
	public void pause () {
		//---- for google analytics:
		long duration = (long)((System.currentTimeMillis() - usageDurationStart)); // umrechnen in minuten
		getPlatformResolver().sendTrackerTimingEvent("Usage", "app", "onPause()", duration);
		getPlatformResolver().stopGoogleAnalytics();
		
		// screen.pause();
	}

	@Override
	public void resume () {
		//---- for google analytics:
		if (getPreferences().getIntegerFromPrefs(GamePreferences.PREF_ANALYTICS_OPTOUT, 0) == 2) {	// only restart GA if prefs-opt-out is set to 2 (0: didnt choose, 1: opted out, 2: opted in)
			getPlatformResolver().startGoogleAnalytics(getClientID());
			usageDurationStart = System.currentTimeMillis();
		}
		// screen.resume();
	}

	@Override
	public void dispose () {
		// screen.dispose();
		if (_cameraManager != null) _cameraManager.dispose();	// disposes modelbatch in 3d mode
//		if (console != null) console.dispose();
		if (_assetService != null) _assetService.dispose();
	}
}
