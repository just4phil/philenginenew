
package de.philweb.philengine.common;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.badlogic.gdx.Gdx;
 
public class LanguageManager {
        private static final String DEFAULT_LANGUAGE = "en_UK";
       
        private String m_languagesFile = null;
        private HashMap<String, String> m_language = null;
        private String m_languageName = null;
              
        public LanguageManager(String languagesName) {
                this("languages.xml", languagesName);
        }
       
        public LanguageManager(String languagesFile, String languagesName) {           
                // Languages file
                m_languagesFile = languagesFile;
               
                // Create language map
                m_language = new HashMap<String, String>();
               
                // Default language (system language)
                m_languageName = languagesName;
                              
              if (m_languageName.equals("")) m_languageName = java.util.Locale.getDefault().toString();
                
                //  Try to load selected language, if it fails, load default one
                if (!loadLanguage(m_languageName)) {
                        loadLanguage(DEFAULT_LANGUAGE);
                }
        }
       
        public String getLanguagesFile() {
                return m_languagesFile;
        }
       
        public void setLanguagesFile(String languagesFile) {
//        	System.out.println("LanguageManager: setting languages file to " + languagesFile);
                m_languagesFile = languagesFile;
        }
       
        public String getLanguage() {
                return m_languageName;
        }
       
        public boolean loadLanguage() {
                return loadLanguage(m_languageName);
        }
       
        
        public boolean loadLanguage(String languageName) {
//        	System.out.println("LanguageManager: try to load: " + languageName);
                      
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                InputSource input = new InputSource(Gdx.files.internal(m_languagesFile).read());
                input.setEncoding("UTF-8");
                Document document = builder.parse(input);

                NodeList languages = document.getElementsByTagName("language");
                                
             // Iterate over languages, trying to find the selected one
                for (int i = 0, j = languages.getLength(); i < j; i++) {
                   
                	Node language = languages.item(i);
                	
                	if (language.getAttributes().getNamedItem("name").getNodeValue().equals(languageName)) {
                		
                		NodeList strings = language.getChildNodes();	// get all strings
                		
                		for (int k = 0, l = strings.getLength(); k < l; k++) {
                			
                			Node string = strings.item(k);
                			NamedNodeMap attributes = string.getAttributes();
                			
                			if (attributes != null) {
                				
                				String key = attributes.getNamedItem("key").getNodeValue();
    							String value = attributes.getNamedItem("value").getNodeValue();

    							m_language.put(key, value);
                			}
                		}
                		
						m_languageName = languageName;
//						System.out.println("LanguageManager: " + languageName + " language sucessfully loaded");
						
						return true; 
                	}
                }
                
             } catch (ParserConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
             } catch (SAXException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
             } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
             }
            
            return false; 
        }
 
        
        
        public String getString(String key) {
                if (m_language != null) {
                        // Look for string in selected language
                        String string = m_language.get(key);
                       
                        if (string != null) {
                                return string;
                        }
                }
       
                // Key not found, return the key itself
//                System.out.println("LanguageManager: string " + key + " not found");		// TODO: activate for testing
                return key;
        }
 
        // Not compatible with HTML5
        public String getString(String key, Object... args) {
                return String.format(getString(key), args);
        }

}


