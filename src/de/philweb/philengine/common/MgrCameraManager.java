package de.philweb.philengine.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class MgrCameraManager {

	protected Game game;
	protected boolean uses3d = false; // default = 2d
	protected OrthographicCamera _HUDcamera;
	protected Vector3 touchPointHUD;
	protected OrthographicCamera _GAMEcamera;
	protected Vector3 touchPointGAME;
	protected Viewport viewportHUD;
	protected Viewport viewportGAME;
	protected SpriteBatch _batch;
	protected Stage _stage;
	
	// ---- stuff for 3d --------------------------
	protected PerspectiveCamera _GAMEcamera3d;
	protected ModelBuilder _modelBuilder;
	protected ModelLoader _modelObjLoader;
	protected ModelBatch _modelBatch;
	protected final Vector3 tmpV1 = new Vector3();
	
	protected MgrCameraManager(Game game) {
		this.game = game;
		Gdx.app.log("CameraManager", "new MgrCameraManager() constructed");
	}
	
	public void initialize2D () {
		uses3d = false;
		//...
		initialize(
				game.getGameConfig().getVIRTUAL_WIDTH_GAME(), game.getGameConfig().getVIRTUAL_HEIGHT_GAME(),
				game.getGameConfig().getVIRTUAL_WIDTH_HUD(), game.getGameConfig().getVIRTUAL_HEIGHT_HUD());
	}
	
	public void initialize3D () {
		uses3d = true;
		_modelBatch = new ModelBatch();
		_modelBuilder = new ModelBuilder();
		_modelObjLoader = new ObjLoader();
		Bullet.init();
		
		initialize(
				game.getGameConfig().getVIRTUAL_WIDTH_GAME(), game.getGameConfig().getVIRTUAL_HEIGHT_GAME(),
				game.getGameConfig().getVIRTUAL_WIDTH_HUD(), game.getGameConfig().getVIRTUAL_HEIGHT_HUD());
	}
	
	private void initialize (float widthGAME, float heightGAME, int widthHUD, int heightHUD) {
		setupHUDcam(widthHUD, heightHUD);
		setupGAMEcam(widthGAME, heightGAME);
		_batch = new SpriteBatch();
		_stage = new Stage(viewportHUD, _batch); // TODO: checken: besser stage mit batch konstruieren.... sonst 2x batch?
		Gdx.input.setInputProcessor(_stage);
		Gdx.input.setCatchBackKey(true);
	}
	
	private void setupHUDcam (int width, int height) {
		_HUDcamera = new OrthographicCamera();
		viewportHUD = new FitViewport(width, height, _HUDcamera);

		// Center camera
		viewportHUD.getCamera().position.set(viewportHUD.getCamera().position.x + width * 0.5f, viewportHUD.getCamera().position.y
			+ height * 0.5f, 0);
		viewportHUD.getCamera().update();
		viewportHUD.apply();
		
		touchPointHUD = new Vector3();
	}

	public void setupGAMEcam (float width, float height) {
		if (uses3d) {
			// ------ 3d ---------------------
			_GAMEcamera3d = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			// _GAMEcamera3d = new PerspectiveCamera(67, width, height);
			_GAMEcamera3d.position.set(0f, 0f, 0f);
			_GAMEcamera3d.lookAt(0, 0, 0);
			_GAMEcamera3d.near = 1f;
			_GAMEcamera3d.far = 500f;
			_GAMEcamera3d.update();
		} else {
			_GAMEcamera = new OrthographicCamera();
			viewportGAME = new FitViewport(width, height, _GAMEcamera);

			// Center camera
			viewportGAME.getCamera().position.set(viewportGAME.getCamera().position.x + width * 0.5f,
				viewportGAME.getCamera().position.y + height * 0.5f, 0);
			viewportGAME.getCamera().update();
			viewportGAME.apply();

			touchPointGAME = new Vector3();
		}
	}
	
	// ----- used by abstractscreen for zoom with picture up/down
	public void setCameraZoomIncrement (float amount) {
		if (uses3d) {
			_HUDcamera.zoom = _HUDcamera.zoom + amount;
			// _GAMEcamera3d.zoom = _GAMEcamera.zoom + amount;
			_GAMEcamera3d.translate(tmpV1.set(_GAMEcamera3d.direction).scl(-amount * 14));
			_GAMEcamera3d.update();
		} else {
			_HUDcamera.zoom = _HUDcamera.zoom + amount;
			_GAMEcamera.zoom = _GAMEcamera.zoom + amount;
		}

		// Gdx.app.log("cam zoom: " + _GAMEcamera.zoom, " // hud zoom: " + _HUDcamera.zoom);
		updateViewports();
	}

	// ---- used by prefs to set ouya safe zone -----
	public void setCameraZoomAbsolute (float absolute) {
		if (uses3d) {
			_HUDcamera.zoom = absolute;
			// _GAMEcamera3d.zoom = absolute;

			_GAMEcamera3d.translate(tmpV1.set(_GAMEcamera3d.direction).scl(-absolute * 14));
			_GAMEcamera3d.update();
		} else {
			_HUDcamera.zoom = absolute;
			_GAMEcamera.zoom = absolute;
		}
		updateViewports();
	}
	
	// public void setGAMECameraZoomIncrement(float amount) {
	//
	// if (uses3d) {
	// // _GAMEcamera3d.zoom = _GAMEcamera.zoom + amount;
	// }
	// else {
	// _GAMEcamera.zoom = _GAMEcamera.zoom + amount;
	// }
	//
	// // Gdx.app.log("cam zoom: " + _GAMEcamera.zoom, " // hud zoom: " + _HUDcamera.zoom);
	// viewportGAME.apply();
	// }

	public void updateViewports () {
		if (uses3d) {
			viewportHUD.apply();
			// viewportGAME.apply();
		} else {
			viewportHUD.apply();
			viewportGAME.apply();
		}
	}
	
	// public void useGAMECam(PolygonSpriteBatch batch) {
	//
	// _GAMEcamera.update();
	//
	// // set viewport
	// // if (viewportGAME != null) Gdx.gl.glViewport((int) viewportGAME.x, (int) viewportGAME.y, (int) viewportGAME.width, (int)
	// viewportGAME.height);
	// if (viewportHUD != null) Gdx.gl.glViewport((int) viewportHUD.x, (int) viewportHUD.y, (int) viewportHUD.width, (int)
	// viewportHUD.height);
	// batch.setProjectionMatrix(_GAMEcamera.combined);
	// }

	public void useGAMECam () {

		viewportGAME.apply();

		// if (viewportHUD != null) Gdx.gl.glViewport((int) viewportHUD.x, (int) viewportHUD.y, (int) viewportHUD.width, (int)
		// viewportHUD.height);
		_batch.setProjectionMatrix(_GAMEcamera.combined);
		_batch.setColor(1f, 1f, 1f, 1f);
	}

	public void useHUDCam () {

		viewportHUD.apply();

		// if (viewportHUD != null) Gdx.gl.glViewport((int) viewportHUD.x, (int) viewportHUD.y, (int) viewportHUD.width, (int)
		// viewportHUD.height);
		_batch.setProjectionMatrix(_HUDcamera.combined);
		_batch.setColor(1f, 1f, 1f, 1f);
	}

	// ------------------------------------------------------

	public Vector3 unprojectGAME (int click_X, int click_Y) {

		// _GAMEcamera.unproject(touchPointGAME.set(click_X, click_Y, 0), viewportGAME.x, viewportGAME.y, viewportGAME.width,
		// viewportGAME.height);
		// _GAMEcamera.unproject(touchPointGAME.set(click_X, click_Y, 0), viewportHUD.x, viewportHUD.y, viewportHUD.width,
		// viewportHUD.height);
		touchPointGAME.x = click_X;
		touchPointGAME.y = click_Y;
		return viewportGAME.unproject(touchPointGAME);
	}

	public Vector3 unprojectHUD (int click_X, int click_Y) {

		// _HUDcamera.unproject(touchPointHUD.set(click_X, click_Y, 0), viewportHUD.x, viewportHUD.y, viewportHUD.width,
		// viewportHUD.height);
		touchPointHUD.x = click_X;
		touchPointHUD.y = click_Y;
		return viewportHUD.unproject(touchPointHUD);
	}
	// --------------------------------
		
	public void resize (int width, int height) {
		if (uses3d) {
			viewportHUD.update(width, height);
			// viewportGAME.update(width, height);
		} else {
			viewportHUD.update(width, height);
			viewportGAME.update(width, height);
		}
	}
	//-------------------------------------------
	
	public OrthographicCamera getGAMECamera () {
		return _GAMEcamera;
	}

	public PerspectiveCamera getGAMECamera3d () {
		return _GAMEcamera3d;
	}

	public OrthographicCamera getHUDCamera () {
		return _HUDcamera;
	}
	
	public Stage getStage () {
		return _stage;
	}

	public SpriteBatch getSpritebatch () {
		return _batch;
	}

	public ModelBatch getModelBatch () {
		return _modelBatch;
	}
	
	public ModelBuilder getModelBuilder () {
		return _modelBuilder;
	}

	public ModelLoader getModelLoader () {
		return _modelObjLoader;
	}
	
//	public void setUses3d (boolean uses3d) {
//		this.uses3d = uses3d;
//	}
	
	public void dispose () {
		if (uses3d) {
			_modelBatch.dispose();
		} 
		else {	}
	}

}
