
package de.philweb.philengine.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.pay.PurchaseManager;
import com.badlogic.gdx.pay.PurchaseManagerConfig;
import com.badlogic.gdx.pay.PurchaseObserver;
import com.badlogic.gdx.pay.PurchaseSystem;

public abstract class PlatformResolver {

	public Game game;
	public String toastText;
	public int duration;
	protected PurchaseManager mgr;
	PurchaseObserver purchaseObserver;
	PurchaseManagerConfig config;
	
	public PlatformResolver (Game game) {
		this.game = game;
	}
	
	public void initializeIAP (PurchaseManager mgr, PurchaseObserver purchaseObserver, PurchaseManagerConfig config) {
		this.mgr = mgr;
		this.purchaseObserver = purchaseObserver;
		this.config = config;
	}

	public void installIAP() {
		// set and install the manager manually
		if (mgr != null) {
			PurchaseSystem.setManager(mgr);
			mgr.install(purchaseObserver, config);	// dont call PurchaseSystem.install() because it may bind openIAB!
			Gdx.app.log("gdx-pay", "calls purchasemanager.install() manually");
		} 
		else {
			Gdx.app.log("gdx-pay", "initializeIAP(): purchaseManager == null => call PurchaseSystem.hasManager()");
			if (PurchaseSystem.hasManager()) { // install and get the manager automatically via reflection
				this.mgr = PurchaseSystem.getManager();
				Gdx.app.log("gdx-pay", "calls PurchaseSystem.install() via reflection");
				PurchaseSystem.install(purchaseObserver, config); // install the observer
				Gdx.app.log("gdx-pay", "installed manager: " + this.mgr.toString());
			}
		}
	}
	
	public void requestPurchase (String productString) {						
		if (mgr != null) {
			mgr.purchase(productString);	// dont call PurchaseSystem... because it may bind openIAB!
			Gdx.app.log("gdx-pay", "calls purchasemanager.purchase()");
			sendTrackerEvent("IAP", "requestPurchase: ok", "ok", (long)1);	// inform google analytics
		} else {
			Gdx.app.log("gdx-pay", "ERROR: requestPurchase(): purchaseManager == null");
			game.getPlatformResolver().showToast("Uups... no purchase manager found. Please restart the game!", true);
			sendTrackerEvent("IAP", "requestPurchase: FAILED - mgr == NULL!!!", "FAILED", (long)1);	// inform google analytics
		}
	}
    
	public void requestPurchaseRestore () {
		if (mgr != null) {
			mgr.purchaseRestore();	// dont call PurchaseSystem.purchaseRestore(); because it may bind openIAB!
			Gdx.app.log("gdx-pay", "calls purchasemanager.purchaseRestore()");
			sendTrackerEvent("IAP", "requestPurchaseRestore: ok", "ok", (long)1);	// inform google analytics
		} else {
			Gdx.app.log("gdx-pay", "ERROR: requestPurchaseRestore(): purchaseManager == null");
			game.getPlatformResolver().showToast("No purchase manager found. Please restart the game if you have trouble!", true);
			sendTrackerEvent("IAP", "requestPurchaseRestore: FAILD - mgr == NULL!!!", "FAILED", (long)1);	// inform google analytics
		}
	}
	
	public PurchaseManager getPurchaseManager () {
		return mgr;
	}
		
	public void dispose () {
		if (mgr != null) {
			Gdx.app.log("gdx-pay", "calls purchasemanager.dispose()");
			mgr.dispose();		// dont call PurchaseSystem... because it may bind openIAB!
			mgr = null;
		}
	}
	
	public void configureOptions () {	}
	public void showToast (String toastText, boolean longDuration) {	}
	public void showEULA() {}
	public void showAppRaterDialog () {}
	public void showAnalyticsDialog() {}
	public void openAppStore() {}
	
	public void share(String shareHeadline, String string, String string2) {}
	public void askShare(String dialogSubject, String dialogText, String shareHeadline, String shareTextSubject, String shareText) {}
	public void email() {}
	public void showUrlDialog(String dialogSubject, String dialogText, String dialogURL) {}
	public void showHintDialog(String dialogSubject, String dialogText) {}	
	public void showRateDialog(String string, String string2, String url) {}
	public void showBUYdualPlayerDialog(String string, String string2) {}
	public void askExit(String dialogSubject, String dialogText) {}
	public void askDelete(String dialogSubject, String dialogText) {}	// delete a gamestate
	public void openLink(String url) {}
	
	public void startGoogleAnalytics(String userID) { }
	public void stopGoogleAnalytics() { }
	public void googleAnalyticsOptOUT() {}
	public void sendTrackerScreen(String screen) { }
	public void sendTrackerEvent(String eventCategory, String eventAction, String eventLabel, long value) { }
	public void sendTrackerTimingEvent(String eventCategory, String timingName, String eventLabel, long milliseconds) { }
}
