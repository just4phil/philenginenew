
package de.philweb.philengine.controllers;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.common.MgrCameraManager;
import de.philweb.philengine.controllers.MgrControllerConfigManager.ControllerType;
import de.philweb.philengine.gameobjects.GoGameObject;

/** @author just4phil */
public class AbstractController {

	protected ControllerType controllerType = ControllerType.UNDEFINED;
	public static final float RETURNFALSE = 0f; // -9999f;
	public float returnfloat;
	ArrayList<Rectangle> arrayTouchzoneHasBeenPressed = new ArrayList<Rectangle>();

	public Game game;
	private MgrControllerConfigManager ctrConfMgr;
	private MgrCameraManager camMgr;
	protected GoGameObject gameObject; // the owner gameObject
	public OrthographicCamera guiCam;
	public String name;
	protected World box2dworld = null;
	public Touchpad touchpad;
	public boolean useTouchpad;

	public ControllerConfig2 controllerConfig;
	public Controller controller = null;
	public boolean controllerDisconnected = false; // if controller disconnects this will be true

	public static final float ONSCREENSTICK_SIZE = 200;
	public float ONSCREENSTICK_X;
	public float ONSCREENSTICK_Y;

	public float axisValue;
	public float axis0Value;
	public float axisLeftX;
	public float axisLeftY;
	public float accel = 0;
	public int touchState = 0;

	float calculatedVELOCITY;
	int breite;
	int hoehe;
	public Vector3 touchPoint;
	float touchCoord_Y;
	public int i;
	int click_X;
	int click_Y;
	int multiTouchCommands[] = new int[2];
	public String button; // used in getButtonToIndex()

	// for value touch
	float maxAt;
	float max;
	float pos;

	// ----- just 4 debugging with onscreen text -----------
//	public AbstractScreen screen;
//
//	public void setGameScreen (AbstractScreen scr) {
//		this.screen = scr;
//	}

	// -------------------------------------------------------

	public AbstractController (Game game, String name) {

		this.game = game;
		this.name = name;
		this.ctrConfMgr = game.getControllerConfigManager(); // controllerConfigManager;
		this.camMgr = game.getCameraManager();
		
		ONSCREENSTICK_X = 0;
		ONSCREENSTICK_Y = (game.getVIRTUAL_HEIGHT_HUD() * 0.5f) - (ONSCREENSTICK_SIZE * 0.5f);

		setSize();
	}

	void setSize () {
		touchPoint = new Vector3();
		breite = game.getVIRTUAL_WIDTH_HUD();
		hoehe = game.getVIRTUAL_HEIGHT_HUD();
	}

	public void setController (Controller contr) {
		controller = contr;
		name = contr.getName();
	}

	public String getDescription () {
		return "";
	}

	public String getButtonToIndex (int index) {
		return "";
	}

	public void setBox2dworld (World box2dworld) {
		this.box2dworld = box2dworld;
	}

	public void setGameObject (GoGameObject gameObject) {
		this.gameObject = gameObject;
	}

	public void update (float deltaTime) {
	}

	public float getInputForCommand (int cmd) {
		return RETURNFALSE;
	}

	public float getTouchForCommand (Command command) {

		returnfloat = RETURNFALSE;

		switch (command.character) {

		case Command.CHARACTER_UP:
			returnfloat = getTouchUp(command.touchZone);
			break;

		case Command.CHARACTER_DOWN:
			returnfloat = checkMultiTouchInRectangle(command.touchZone, 3, command.lengthForHundredPercent);
			break;
		}
		return returnfloat;
	}

	public float checkSingleTouchDownInRectangle (Rectangle rectangle) {
		return checkMultiTouchInRectangle(rectangle, 1, 0);
	}

	// lengthForHundredPercent: if 0: value is immediately 1f, if 50: value if 1f after 50% of the rectangle size
	public float checkMultiTouchInRectangle (Rectangle rectangle, int multiTouchCounter, int lengthForHundredPercent) {
		float returnvalue = 0f;
		for (i = 0; i < multiTouchCounter; i++) {
			if (Gdx.input.isTouched(i)) {
				touchPoint = camMgr.unprojectHUD(Gdx.input.getX(i), Gdx.input.getY(i));
				if (rectangle.contains(touchPoint.x, touchPoint.y)) {
					if (lengthForHundredPercent == 0) {
						returnvalue = 1f; // --- no value-zone ... only touch/no touch :)
					} else {
						maxAt = max * Math.abs(lengthForHundredPercent) / 100;

						if (rectangle.getHeight() > rectangle.getWidth()) {
							max = rectangle.getHeight();
							if (lengthForHundredPercent > 0)
								pos = touchPoint.y - rectangle.y;
							else if (lengthForHundredPercent < 0) pos = rectangle.y + rectangle.getHeight() - touchPoint.y;
						} else {
							max = rectangle.getWidth();
							if (lengthForHundredPercent > 0)
								pos = touchPoint.x - rectangle.x;
							else if (lengthForHundredPercent < 0) pos = rectangle.x + rectangle.getWidth() - touchPoint.x;
						}
						returnvalue = pos / maxAt;
						if (returnvalue > 1f) returnvalue = 1f;
					}
// screen.showText("touchPoint.y: " + Math.round(touchPoint.y) + " // rectangle.y: " + Math.round(rectangle.y) +
// " // returnvalue: " + returnvalue);
				}
			}
		}
		return returnvalue;
	}

	public float getTouchUp (Rectangle rectangle) {
		float returnFloat = RETURNFALSE;

		if (Gdx.input.isTouched()) {
			touchPoint = camMgr.unprojectHUD(Gdx.input.getX(), Gdx.input.getY());
			if (rectangle.contains(touchPoint.x, touchPoint.y)) {
				if (!arrayTouchzoneHasBeenPressed.contains(rectangle)) {
					arrayTouchzoneHasBeenPressed.add(rectangle);
				}
				returnFloat = 0f;
			}
		} else {
			if (arrayTouchzoneHasBeenPressed.contains((Rectangle)rectangle)) {
				arrayTouchzoneHasBeenPressed.remove(rectangle);
				returnFloat = 1f;
			}
		}
		return returnFloat;
	}

	public boolean isControllerDisconnected () {
		return controllerDisconnected;
	}

	public void setControllerDisconnected (boolean disconnected) {
		controllerDisconnected = disconnected;
	}

	// this is used for example to check if its an AI controller.....
	public ControllerType getControllerType() {
		return controllerType;
	}
	public void setControllerType(ControllerType type) {
		controllerType = type;
	}
	
// public float checkSingleTouchUpInRectangle(Rectangle rectangle) {
// float returnvalue = 0f;
// if (Gdx.input.justTouched()) {
// touchPoint = game.unprojectHUD(Gdx.input.getX(i), Gdx.input.getY(i));
// if (rectangle.contains(touchPoint.x, touchPoint.y)) returnvalue = 1f;
// }
// return returnvalue;
// }

// public float getTouchValueForCommand(Command command) {
//
// for (i = 0; i < 3; i++) {
// if (Gdx.input.isTouched(i)) {
// touchPoint = game.unprojectHUD(Gdx.input.getX(i), Gdx.input.getY(i));
// if (command.touchZone.contains(touchPoint.x, touchPoint.y)) {
// direction = -Math.min(1f, (command.touchZone.x + command.touchZone.width - command.touchZone.x) / 100);
// }
// // else if (game.getPreferences().rightBounds.contains(touchPoint.x, touchPoint.y)) {
// // direction = Math.min(1f, (touchPoint.x - game.getPreferences().rightBounds.x) / 100);
// // }
// }
// }
// return direction;
// }

}
