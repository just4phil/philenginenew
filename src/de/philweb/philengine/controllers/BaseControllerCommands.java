package de.philweb.philengine.controllers;

import java.util.HashMap;

public abstract class BaseControllerCommands {

	protected Command[] arrayCommands = null;
	protected HashMap<String, ControllerConfig2> arrayConfigs;
	
	public BaseControllerCommands() {
		arrayCommands = new Command[0];
		arrayConfigs = new HashMap<String, ControllerConfig2>();
	}
	
	// Bei Arrays von Objekten und somit auch geschachtelten Arrays ist zu beachten, dass die Arrays sowohl bei Object.clone() als auch bei System.arraycopy() flach kopiert werden, d. h., es werden nur die Objektreferenzen kopiert und nicht die Objekte selbst.
	public Command[] getNewCommandsArray() {	// Achtung: array.clone() erstellt zwar eigenstaendiges array, aber objekt-elemente werden nur referenziert
		
		Command[] returnArray = new Command[arrayCommands.length];
		for (int i = 0; i < arrayCommands.length; i++) {
			returnArray[i] = Command.deepCopy(arrayCommands[i]);
		}
		return returnArray;
	}
	public HashMap<String, ControllerConfig2> getDefaultConfigs() {
		return arrayConfigs;
	}
	
//	public ControllerConfig2 copyWithoutReferences() {		
//		Json json = new Json();		
//		String jsonText = json.prettyPrint( this );
//		json = new Json();	// TODO: ist das noch mal noetig?
//		ControllerConfig2 newconfig = json.fromJson(ControllerConfig2.class, jsonText );
//		return newconfig;
//	}
	
	public abstract ControllerConfig2 getDefaultConfigAndroid();
	
	public abstract ControllerConfig2 getDefaultConfigKEYS_PL1();
	
	public abstract ControllerConfig2 getDefaultConfigKEYS_PL2();
	
	public abstract ControllerConfig2 getDefaultConfigOUYAatOUYA();
	
	public abstract ControllerConfig2 getDefaultConfigAmazonFireAtOUYA();
	
	public abstract ControllerConfig2 getDefaultConfigFireTVRemote();
	
	public abstract ControllerConfig2 getDefaultConfigPS4AtFireTV();
	
}







//// ---- used by renderer for debug rendering --------------
//public static ArrayList<Rectangle> getClickBoundsArray () {
//	ArrayList<Rectangle> arrayClickBounds = new ArrayList<Rectangle>();
////	arrayClickBounds.add(pauseBounds);
//	return arrayClickBounds;
//}
//
//public abstract Command[] getNewCommandsArray ();
//
////public static ControllerConfig2 getNewControllerConfig (Command[] arrayCommands) {
////	return new ControllerConfig2(arrayCommands);
////}
////
////public static ControllerConfig2 getNewControllerConfig () {
////	return new ControllerConfig2(getNewCommandsArray());
////}
//
//// ======================================
//// ATTENTION: ADD NEW CONFIGS HERE !!!!
//// ======================================
//public static HashMap<String, ControllerConfig2> getDefaultConfigsMap () {
//	HashMap<String, ControllerConfig2> arrayConfigs = new HashMap<String, ControllerConfig2>();
//	arrayConfigs.put(MgrControllerConfigManager.ControllerDesktopPL1, setDefaultConfigKEYS_PL1(getNewControllerConfig()));
//	arrayConfigs.put(MgrControllerConfigManager.ControllerAndroid, setDefaultConfigAndroid(getNewControllerConfig()));
//	return arrayConfigs;
//}
//
//// -------------------------------------------------------------------------------------
//
//public static ControllerConfig2 setDefaultConfigAndroid (ControllerConfig2 config) {
//	config.ID = MgrControllerConfigManager.UNCONFIGURED;
//	config.name = "android";
//	config.controllerDescription = MgrControllerConfigManager.ControllerAndroid;
//	config.arrayCommands[CMD_PLAYPAUSE].touchZone = pauseBounds;
//	config.arrayCommands[CMD_PLAYPAUSE].character = Command.CHARACTER_UP;
//	config.arrayCommands[CMD_PLAYPAUSE].buttonIndex = 0;
//	config.arrayCommands[CMD_PLAYPAUSE].type = Command.TYPE_TOUCH;
//	return config;
//}
//
//public static ControllerConfig2 setDefaultConfigKEYS_PL1 (ControllerConfig2 config) {
//	config.ID = MgrControllerConfigManager.UNCONFIGURED;
//	config.name = "DPAD";
//	config.controllerDescription = MgrControllerConfigManager.ControllerDesktopPL1;
//
//	for (int i = 0; i < COUNT; i++) {
//		config.arrayCommands[i].type = Command.TYPE_KEY;
//		config.arrayCommands[i].character = Command.CHARACTER_UP;
//		config.arrayCommands[i].lengthForHundredPercent = 0;
//	}
//	config.arrayCommands[CMD_MENULEFT].buttonIndex = Keys.DPAD_LEFT;
//	config.arrayCommands[CMD_MENURIGHT].buttonIndex = Keys.DPAD_RIGHT;
//	config.arrayCommands[CMD_MENUUP].buttonIndex = Keys.DPAD_UP;
//	config.arrayCommands[CMD_MENUDOWN].buttonIndex = Keys.DPAD_DOWN;
//	config.arrayCommands[CMD_ENTERMENU].buttonIndex = Keys.ENTER;
//	config.arrayCommands[CMD_QUIT].buttonIndex = Keys.ESCAPE;
//	config.arrayCommands[CMD_PLAYPAUSE].buttonIndex = Keys.F5;
//	config.arrayCommands[CMD_PLAYPAUSE].touchZone = pauseBounds;
//	return config;
//}