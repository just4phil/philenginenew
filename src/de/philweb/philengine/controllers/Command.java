package de.philweb.philengine.controllers;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Json;

public class Command {
	
	public final static byte TYPE_UNDEFINED = 0;
	public final static byte TYPE_BUTTON	= 1;	// controller button
	public final static byte TYPE_AXIS		= 2;	// controller axis
	public final static byte TYPE_KEY		= 3;	// keyboard
	public final static byte TYPE_TOUCH		= 4;	// touchscreen
	
	public final static byte CHARACTER_UNDEFINED = 0;
	public final static byte CHARACTER_UP		= 1;	// button/touch up
	public final static byte CHARACTER_DOWN		= 2;	// button/touch down
	public final static byte CHARACTER_AXIS_X	= 3;	// button/touch down
	public final static byte CHARACTER_AXIS_Y	= 4;	// button/touch down
	public final static byte CHARACTER_AXIS_Z	= 5;	// button/touch down
//	public final static byte CHARACTER_VALUE	= 3;	// touch zone from 0.0f to 1.0f
	
	public final static byte RETURN_UNDEFINED 	= 0;
	public final static byte RETURN_POSITIVE	= 1;	// (only axis) if axis in this direction is > 0 // or if button/key should return positive
	public final static byte RETURN_NEGATIVE	= 2;	// (only axis) if axis in this direction is < 0 // or if button/key should return negative
	
	public String description;
	public byte ID ;
	public int buttonIndex;
	public byte type;
	public byte character;	// this is for touch up / touch down / touch value
	public byte returnType;	// this tells if a value should be negative / positiv
	public float treshold;	// dead-zone of axis (like 0.15f)
	public Rectangle touchZone;
	public int lengthForHundredPercent;	// if != = => touch zone from 0.0f to 1.0f // ONLY WORKS IF character == CHARACTER_DOWN
	//int gestureType;
	//float minDuration;
	
	
	/**
	 * @param ID 
	 * @param buttonIndex ... index of button or axis or kecode
	 * @param type.... TYPE_UNDEFINED, TYPE_BUTTON, TYPE_AXIS, TYPE_KEY
	 * @param treshold
	 * @param touchZone Rectangle
	 * @param character ... this is for touch up / touch down / touch value
	 */
	public Command(String description, byte ID, int buttonIndex, byte type, float treshold, Rectangle touchZone, byte character) {

		this.description = description;
		this.ID = ID;
		this.buttonIndex = buttonIndex;
		this.type = type;
		this.character = character;
		this.treshold = treshold;
		this.touchZone = touchZone;
		this.lengthForHundredPercent = 0;
	}

	//--- no arg constructor to enable cloning of controllerconfig ----------
	public Command() {

		this.description = "";
		this.ID = 0;
		this.buttonIndex = 0;
		this.type = TYPE_UNDEFINED;
		this.character = CHARACTER_UNDEFINED;
		this.returnType = RETURN_UNDEFINED;
		this.treshold = 0f;
		this.touchZone = null;
		this.lengthForHundredPercent = 0;
	}
	
	public static Command deepCopy(Command cmd2copy) {		
		Json json = new Json();		
		String jsonText = json.prettyPrint( cmd2copy );
		json = new Json();	// TODO: ist das noch mal noetig?
		Command cmd = json.fromJson(Command.class, jsonText );
		return cmd;
	}
	
	public Command deepCopy() {		
		return deepCopy(this);
	}
}
