package de.philweb.philengine.controllers;

import de.philweb.philengine.common.Game;



public class ControllerAndroidOnScreen extends AbstractController {

//	//------------------------------- defaults ----------------------------------------------------
//	static final int default_fire 				= OuyaCodes.BUTTON_R1; 					// default is BUTTON_R1
//	static final int default_suicide 			= OuyaCodes.BUTTON_L2; 					// default is BUTTON_L2 
//	static final int default_weapondown 		= OuyaCodes.BUTTON_L1; 					// default is BUTTON_L1 
//	static final int default_laserkill			= OuyaCodes.BUTTON_R2; 					// default is BUTTON_R2 
//	static final int default_axisLeftRight		= OuyaCodes.AXIS_LEFT_X;				// default is AXIS_LEFT_X
//	static final int default_moveRight 			= ControllerConfigManager.DEACTIVATED; 	// default is *** deactivated *** 
//	static final int default_moveLeft 			= ControllerConfigManager.DEACTIVATED; 	// default is *** deactivated *** 
//	static final int default_axisjump 			= OuyaCodes.AXIS_LEFT_Y;				// default is AXIS_LEFT_Y
//	static final int default_jumpHigh 			= ControllerConfigManager.DEACTIVATED; 	// default is *** deactivated *** 
//	static final int default_jumpLow 			= ControllerConfigManager.DEACTIVATED; 	// default is *** deactivated *** 
//	static final int default_playpause 			= OuyaCodes.BUTTON_O; 					// default is BUTTON_O 
//	static final int default_quit 				= OuyaCodes.BUTTON_A; 					// default is BUTTON_A 
//	static final int default_restart			= OuyaCodes.BUTTON_Y; 					// default is BUTTON_Y 
//	static final int default_togglemusic		= OuyaCodes.BUTTON_DPAD_LEFT; 			// default is BUTTON_DPAD_LEFT 
//	static final int default_togglesound		= OuyaCodes.BUTTON_DPAD_RIGHT; 			// default is BUTTON_DPAD_RIGHT 
//	static final int default_menuAxisLeftRight	= OuyaCodes.AXIS_LEFT_X;				// default is AXIS_LEFT_X
//	static final int default_menuAxisUpDown		= OuyaCodes.AXIS_LEFT_Y;				// default is AXIS_LEFT_Y
//	static final int default_menuprevious		= OuyaCodes.BUTTON_DPAD_UP; 			// default is BUTTON_DPAD_UP 
//	static final int default_menunext			= OuyaCodes.BUTTON_DPAD_DOWN; 			// default is BUTTON_DPAD_DOWN 
//	static final int default_menupleft			= OuyaCodes.BUTTON_DPAD_LEFT; 			// default is BUTTON_DPAD_LEFT 
//	static final int default_menuright			= OuyaCodes.BUTTON_DPAD_RIGHT; 			// default is BUTTON_DPAD_RIGHT
//	static final int default_entermenu			= OuyaCodes.BUTTON_O; 					// default is BUTTON_O 
	

	float calculatedVELOCITY;
	
	final static float calibrationMax = 1.0f;					// wert für max-ausschlag des controllers
	final static float calibrationAndroidMax = 3.2858f;			// ab diesem wert (normiert) ist volle velocity, damit man den controller nicht voll ausschlagen muss
	final static float calibrationFactor = 1.75f;				// verstärker für velocity
	final static float correctionFactor = 1.45f;				// 1.0f;
	final static float normalization = calibrationAndroidMax / calibrationMax;	// auf gleiche range wie bei android normalisieren!
	
	final static float borderLook = 0.05f; 				// grenze +/- zum drehen der blickrichtung
	final static float borderWalk = 0.1f; 				// grenze +/- zum starten des laufens
	
	final static float accel_calib_min = 0f;
	final static float accel_calib_max = 1f; //3.3f;
	
	
	final static float JUMPTRESHOLD = 0.5f;
	//---------------------------------------------------------
	
	
	public ControllerAndroidOnScreen(Game game, String name) {
		
		super(game, name);    
		
	}
	//---------------------------------------------------------
	
	
	public String getDescription() {

		return MgrControllerConfigManager.ControllerAndroidOnScreen;
	}
	
	
//	@Override
//	public boolean getplayPause() {
//		
//		returnTogglePlayPause = false;	
//		
//		if (Gdx.input.justTouched()) {
//		
////			touchPoint = bubblr.unprojectHUD(Gdx.input.getX(), Gdx.input.getY());
////			if (OverlapTester.pointInRectangle(bubblr.prefs.pauseBounds, touchPoint.x, touchPoint.y)) {
//				returnTogglePlayPause = true;
////			}
//		}		
//		return returnTogglePlayPause;
//	}
//
//
//
//	@Override
//	public boolean getQuit() {
//		
//		returnQuit = false;
//
//		if (Gdx.input.isKeyPressed(Keys.BACK) == true) {
//			quitHasBeenPressed = true;
//		}
//		else {
//			if (quitHasBeenPressed == true) { 
//				quitHasBeenPressed = false;
//				returnQuit = true;
//			}
//		}
//		
//		
//		if (Gdx.input.isTouched(0)) {
//				
////			guiCam.unproject(touchPoint.set(Gdx.input.getX(0), Gdx.input.getY(0), 0));
//			touchPoint = game.unprojectHUD(Gdx.input.getX(), Gdx.input.getY());
//			
////			if (OverlapTester.pointInRectangle(game.prefs.quitBounds, touchPoint.x, touchPoint.y)) {
////				quitHasBeenPressed = true;
////			}
////			else {
////				if (quitHasBeenPressed == true) { 
////					quitHasBeenPressed = false;
////					returnQuit = true;
////				}
////			}
//		}		
//		return returnQuit;
//	}
//
// 
//
//	@Override
//	public boolean getMenuNext() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//
//	@Override
//	public boolean enterMenu() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//
//	@Override
//	public boolean getMenuPrevious() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//	@Override
//	public String getButtonToIndex(int index) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//
//
//	@Override
//	public boolean getMenuRight() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//
//	@Override
//	public boolean getMenuLeft() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//	@Override
//	public int getID() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//
//	@Override
//	public float getAccelaration() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//
//	@Override
//	public float getDirection() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//
//	@Override
//	public boolean getFireCannon() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//	@Override
//	public boolean getPlaceBomb() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//	@Override
//	public float getCannonDirection() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public boolean getRotateCannon() {
//		returnRotateCannon = false;
////		
////		if (Gdx.input.isKeyPressed(controllerConfig.laserKillSwitch)) {
////			rotateCannonHasBeenPressed = true; 
////		}
////		else {
////			if (rotateCannonHasBeenPressed == true) { 
////				rotateCannonHasBeenPressed = false;
////				returnRotateCannon = true;
////			}
////		}	
//		return returnRotateCannon;
//	}
//	
}


