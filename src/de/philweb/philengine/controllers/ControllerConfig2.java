package de.philweb.philengine.controllers;

import com.badlogic.gdx.utils.Json;


public class ControllerConfig2 {
	
	public int ID;
	public String name;
	public String controllerDescription;
	public int anzahl;
	public Command[] arrayCommands;
	public boolean isSystemConfig;
	
	ControllerConfig2() {
		this.ID							= MgrControllerConfigManager.UNCONFIGURED;
		this.name 						= "";
		this.controllerDescription	= "";
//		this.arrayCommands 			= new Command[15];	// FIXME // TODO: ------------------------------------ this is hard coded!! dependency to game project!!!
//		this.anzahl 					= this.arrayCommands.length;	
		isSystemConfig					= true;
	}
	
	// used by controllerCommands
	public ControllerConfig2(String name, String controllerDescription, Command[] arrayCommands) {
		this.ID							= MgrControllerConfigManager.UNCONFIGURED;
		this.name 						= name;
		this.controllerDescription	= controllerDescription;
		this.arrayCommands 			= arrayCommands;
		this.anzahl 					= arrayCommands.length;	
		this.isSystemConfig			= true;
	}
	
//	// no-arg constructor to enable cloning/copying
//	ControllerConfig2() {
//		this.ID							= MgrControllerConfigManager.UNCONFIGURED;
//		this.name 						= "";
//		this.controllerDescription	= "";
//		this.arrayCommands 			= new Command[15];	// FIXME // TODO: ------------------------------------ this is hard coded!! dependency to game project!!!
//		this.anzahl 					= this.arrayCommands.length;	
//		isSystemConfig					= true;
//	}
//	
//	public ControllerConfig2(Command[] arrayCommands) {
//		this.ID							= MgrControllerConfigManager.UNCONFIGURED;
//		this.name 						= "";
//		this.controllerDescription	= "";
//		this.arrayCommands 			= arrayCommands;
//		this.anzahl = arrayCommands.length;	
//		this.isSystemConfig			= true;
//	}
	
//	public static ControllerConfig2 getNewControllerConfig (Command[] arrayCommands) {
//		return new ControllerConfig2(arrayCommands);
//	}

//	public static ControllerConfig2 getNewControllerConfig () {
//		return new ControllerConfig2(getNewCommandsArray());
//	}
		
	public ControllerConfig2 deepCopy() {		// copyWithoutReferences
		Json json = new Json();		
		String jsonText = json.prettyPrint( this );
		json = new Json();	// TODO: ist das noch mal noetig?
		ControllerConfig2 newconfig = json.fromJson(ControllerConfig2.class, jsonText );
		return newconfig;
	}	
}





//package de.philweb.philengine.controllers;
//
//import com.badlogic.gdx.utils.Json;
//
//
//
//public class ControllerConfig2 {
//	
//	public int ID;
//	public String name;
//	public String controllerDescription;
//	public int anzahl;
//	public Command[] arrayCommands;
//	public boolean isSystemConfig;
//	
//	public ControllerConfig2(Command[] arrayCommands) {
//
//		this.ID						= MgrControllerConfigManager.UNCONFIGURED;
//		this.name 					= "";
//		this.controllerDescription	= "";
//		this.arrayCommands = arrayCommands;
//		this.anzahl = arrayCommands.length;	
//
//		isSystemConfig			= true;
//	}
//	
//	// no-arg constructor to enable cloning/copying
//	ControllerConfig2() {
//
//		this.ID						= MgrControllerConfigManager.UNCONFIGURED;
//		this.name 					= "";
//		this.controllerDescription	= "";
//		this.arrayCommands 			= new Command[15];	// this is hard coded!! dependency to game project!!!
//		this.anzahl 				= this.arrayCommands.length;	
//		isSystemConfig				= true;
//	}
//	
//	public ControllerConfig2 copyWithoutReferences() {		
//		Json json = new Json();		
//		String jsonText = json.prettyPrint( this );
//		
//		json = new Json();	// TODO: ist das noch mal noetig?
//		ControllerConfig2 newconfig = json.fromJson(ControllerConfig2.class, jsonText );
//		
//		return newconfig;
//	}
//	
//}

