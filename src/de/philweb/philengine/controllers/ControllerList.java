package de.philweb.philengine.controllers;

import java.util.ArrayList;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;

import de.philweb.philengine.common.Game;

public class ControllerList {

	Game game;
	public ArrayList<AbstractController> list = new ArrayList<AbstractController>();
	int listCounter = -1;
	int maxItems = 0;
//	private int countOUYAcontrollers;
	private int countKnownControllers;
	int controllerCount;
	MgrControllerConfigManager contrConfMgr;
	ControllerConfig2 controllerConfig_default;
	
	
	//__ TODO: configs und isDefault muss aus JSON geladen werden und darf nur gesetzt werden, wenn aus JSON nicht (komplett) vorhanden!
	
	
	public ControllerList(Game game, MgrControllerConfigManager contrConfMgr, ControllerConfigList controllerConfigList) {
		
		AbstractController gameController = null;
		
		this.game = game;
		this.contrConfMgr = contrConfMgr;
		
		controllerCount = Controllers.getControllers().size;					// and no the other controllers
		
		//--- now fill the arrays / lists ....
		
		gameController = new ControllerNone(game, MgrControllerConfigManager.ControllerNONE);
		gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerNONE, MgrControllerConfigManager.ConfigDefault, 0, true);
		list.add(gameController);
		
		
		if (game.getRunningDevice() == Game.ANDROID) {
			
			gameController = new ControllerAndroid(game, MgrControllerConfigManager.ControllerAndroid);
			gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerAndroid, MgrControllerConfigManager.ConfigDefault, 0, true);
//			gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerAndroid, MgrControllerConfigManager.ConfigAndroidFireRight, 0, true);
			list.add(gameController);
			
//			gameController = new ControllerAndroidOnScreen(game, MgrControllerConfigManager.ControllerAndroidOnScreen);
//			gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerAndroidOnScreen, MgrControllerConfigManager.ConfigAndroidFireLeft, 0, true);
//			gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerAndroidOnScreen, MgrControllerConfigManager.ConfigAndroidFireRight, 0, true);
//			list.add(gameController);
		}
		
		if (game.getRunningDevice() == Game.AMAZONFIRETV) {
			
			gameController = new ControllerFireTVRemote(game, MgrControllerConfigManager.ControllerFireTVRemote);
			gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerFireTVRemote, MgrControllerConfigManager.ConfigDefault, 0, true);
			list.add(gameController);
		}
		
		if (game.getRunningDevice() == Game.DESKTOP) {
			
			
			//------- TODO: just 4 testing onscreen stick under desktop --------------------------------------------------
//			gameController = new ControllerAndroidOnScreen(game, MgrControllerConfigManager.ControllerAndroidOnScreen, contrConfMgr);
//			gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerAndroidOnScreen, MgrControllerConfigManager.ConfigAndroidFireRight, 0, true);
//			gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerAndroidOnScreen, MgrControllerConfigManager.ConfigAndroidFireLeft, 0, true);
//			list.add(gameController);
			//------- TODO: just 4 testing onscreen stick under desktop --------------------------------------------------
			
			
			
			//------ first desktop controller for player 1 ----------------------
			gameController = new ControllerDesktop(game, MgrControllerConfigManager.ControllerDesktopPL1);
			gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerDesktopPL1, "default", 1, true);
//			gameController.lfdNummer = countDuplicateControllers(gameController.getDescription());
			list.add(gameController);
			
			//------ second desktop controller for player 2 ----------------------
			gameController = new ControllerDesktop(game, MgrControllerConfigManager.ControllerDesktopPL2);
			gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(MgrControllerConfigManager.ControllerDesktopPL2, "default", 2, true);
//			gameController.lfdNummer = countDuplicateControllers(gameController.getDescription());
			list.add(gameController);
		}
			
		updateGamePadControllers();
	}	
	
	protected void updateGamePadControllers() {
		
		boolean isControllerKnown = false;
		AbstractController gameController = null;
		
		countKnownControllers = 0;
		if (game.getRunningDevice() == Game.ANDROID) countKnownControllers = 1; // on mobile there is touch screen
		else if (game.getRunningDevice() == Game.AMAZONFIRETV) countKnownControllers = 1; // on Fire TV there is the remote that can be used as a controller too but wont be detected as a controller
		else if (game.getRunningDevice() == Game.DESKTOP) countKnownControllers = 2; // on desktop there are 2x keys/mouse
		
		
		controllerCount = Controllers.getControllers().size;
		
		//------ and now the other controllers
		for (int i = 0; i < controllerCount; i++) {
			
			Controller contr = Controllers.getControllers().get(i);
			
			//---- count known controllers -----
//			if (OuyaCodes.ID.equals(contr.getName())) countOUYAcontrollers ++;
			isControllerKnown = isControllerAKnownController(contr.getName());
			if (isControllerKnown) countKnownControllers ++;	
			
			//--- 1. check if controller is already bound in one of the existing peControllers ----
			if (isGamepadAlreadyBound(contr) == false) {		//---- only add new controllers
			
				//--- 2. controller is new ... so construct a peController
				gameController = new ControllerGamepad(game, contr);
				
				//------ (nur) fuer bekannte controller eine default config anlegen ---------------------			
				if (isControllerKnown) {
//					countKnownControllers ++;		
					gameController.controllerConfig = contrConfMgr.generateAndRegisterNewConfig(gameController.controller.getName(), "default", 0, true);
					
//					//---- count ouya controllers -----
//					if (OuyaCodes.ID.equals(contr.getName())) countOUYAcontrollers ++;
				}
				// gameController.lfdNummer = countDuplicateControllers(gameController.getDescription());	// war mal gedacht zur visuellen nummerierung ?
				
				//--- 3. check if one of the controllers has been disconnected and should be replaced by this one
				int ind = indexOfDisconnectedControllerByName(contr);
				if (ind > -1) {
					list.set(ind, gameController);
				}
				
				else {	// no disconnected controller available, .... so simply add this new one to the list
					list.add(gameController);
				}
			}
			else {	
				//--- not neccessary because if gamepad is bound it cant be a disconnected one, because re-connected controllers always have new objectID
			}
		}
	}
	
	//--- TODO: these are the known controllers .... add more .....
	boolean isControllerAKnownController(String name) {
		if (	MgrControllerConfigManager.ControllerSaitek.equals(name) || 
				MgrControllerConfigManager.ControllerAmazonFire.equals(name) ||	
				MgrControllerConfigManager.ControllerPS4.equals(name) ||	
				MgrControllerConfigManager.ControllerOUYA.equals(name)	
			) {
			game.getPlatformResolver().sendTrackerEvent("Controllers", "Controller found", name, (long)1);
			return true;
		}
		else {
			game.getPlatformResolver().sendTrackerEvent("Controllers", "unknown Controller found", name, (long)1);
			return false;
		}
	}
	
	boolean isGamepadAlreadyBound(Controller contr) {
		boolean returnbool = false;
		
		for (int x = 0; x < list.size(); x++) {
			if (list.get(x).controller == contr) {
				returnbool = true;
				break;
			}
		}
		return returnbool;
	}
	
	//---- searches for controllers with the given name and checks the disconnection status
	//---- returns the list-index of the first disconnected one found .... or -1 if false
	int indexOfDisconnectedControllerByName(Controller contr) {
		int returnInt = -1;
		AbstractController searchController = null;
		
		for (int x = 0; x < list.size(); x++) {
			searchController = list.get(x);
			if (searchController.controller != null && searchController.controller.getName().equals(contr.getName())) {
				if (searchController.isControllerDisconnected() == true) {
					returnInt = x;
					break;
				}
			}
		}
		return returnInt;
	}
	
	public String[] getStringList() {
	
		int size = list.size();
		String[] stringList = new String[size];
		String descr;
		
		for (int i = 0; i < size; i++) {
			
			descr = list.get(i).getDescription();
			
//			if (list.get(i).lfdNummer > 1) descr = descr + list.get(i).lfdNummer;
			
			stringList[i] = descr;
		}
		
		return stringList;
	}
	
//	private int countDuplicateControllers(String description) {
//		
//		int counter = 1;
//		
//		for (int i = 0; i < list.size(); i++) {
//			
//			if (list.get(i).getDescription().equals(description)) counter ++;
//		}
//		
//		return counter;
//	}
		
//	public int getOUYAcontrollerCounter() {
//		return countOUYAcontrollers;
//	}
	
	public int getKnownControllerCounter() {
		return countKnownControllers;
	}
}
