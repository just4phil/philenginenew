package de.philweb.philengine.controllers;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.Json;

import de.philweb.philengine.common.FileSystem;
import de.philweb.philengine.common.Game;
import de.philweb.philengine.common.MgrCameraManager;


public class MgrControllerConfigManager {
	
	public enum ControllerType {
		UNDEFINED, TOUCHSCREEN,	GAMEPAD, KEYBOARD, AI
	}
	
	protected static MgrControllerConfigManager instance;
	Game game;
	public int id = 0; // zum nummerieren bei newConfig()
	
	protected BaseControllerCommands controllerCommands;
	private Command[] arrayCommands;	// this will be set by dependency injection from the game and will be used to generate configs...
	protected HashMap<String, ControllerConfig2> arrayDefaultControllerConfigs;
	
	//---- commands for menuNavigation (game can store specific commands here for automatic usage in abstractSubMenu)
	public boolean autoCheckNavCommands = false; 	// will be set to true if values are set... abstractsubmenu will check this before checking commands to avoid crashes
	public int commandMenuRight = -1;
	public int commandMenuLeft = -1;
	public int commandMenuUp = -1;
	public int commandMenuDown = -1;
	public int commandEnterMenu = -1;
	
	//---- OnScreenJoystick ------
	Touchpad touchpad = null;	// this will be set by dependencyInjection ans will be handled to the controller if the androidOnScreenController is choosen
	
	private static final String CONFIG_DIR 	= ".configs";
	FileHandle profileDataFile;
	private MgrCameraManager camMgr;
	
	public static final String NULL = "NULL";
	
	public static final String ControllerNONE = "- none -";
	public static final String ControllerDesktopPL1 = "Keys/Mouse PL1";
	public static final String ControllerDesktopPL2 = "Keys/Mouse PL2";
	public static final String ControllerAndroid = "Accelerometer"; 	// "Touchscreen";
	public static final String ControllerAndroidOnScreen = "OnScreen-Stick";
	public static final String ControllerSaitek = "Saitek Cyborg Evo";
	public static final String ControllerAmazonFire = "Amazon Fire Game Controller";
	public static final String ControllerFireTVRemote = "Amazon Fire TV Remote";
	public static final String ControllerPS4 = "Sony Computer Entertainment Wireless Controller";
	public static final String ControllerOUYA = OuyaCodes.ID;
	
	public static final String ConfigDefault = "default";
//	public static final String ConfigAndroidFireRight = "fire right";	
//	public static final String ConfigAndroidFireLeft = "fire left";	
	
	public ControllerList controllerList;
	public ControllerConfigList controllerConfigList;
	
	AbstractController menuController;
	AbstractController inputController_PL1;
	AbstractController inputController_PL2;
	AbstractController inputController_PL3;
	AbstractController inputController_PL4;
	
	public static final int UNCONFIGURED = -1;		// soll bei den controllercodes angeben ob eine achse deaktiviert wurde damit buttons genutzt werden können
													// evtl. überflüssig weil man ja auch beides gleichzeitg nutzen könnte?
	int isSomeOneUsingTouchscreen;
	 
	//----- bounds für jump/fire button -------------
	public Vector2	 	jumpButtonLocation; 
	public Rectangle	jumpButtonClickRegion;
	public Vector2	 	fireButtonLocation; 
	public Rectangle	fireButtonClickRegion;
	
	//------- for controller assign screen ---------------------------------
	public Texture controllerAssignScreenTexture;
	
	public MgrControllerConfigManager(Game game) {		
		this.game = game;
		this.camMgr = game.getCameraManager();
		
		Controllers.getControllers();
	}
	
	//--- singleton with lazy initialization
    public static MgrControllerConfigManager getInstance(Game game) {
        if(instance == null) {
            instance = new MgrControllerConfigManager(game);
            Gdx.app.log("ControllerConfigManager", "new MgrControllerConfigManager() constructed");
        }
        return instance;
    }
    		
	/** first initialization
	 * game specific defaultControllerConfigs in an array and initializes the controllerMgr with it
	 * @param HashMap<String, ControllerConfig2> */	
	public void initialize(BaseControllerCommands controllerCommands) { 	
		this.controllerCommands = controllerCommands;
		this.arrayCommands = controllerCommands.getNewCommandsArray();
		this.arrayDefaultControllerConfigs = controllerCommands.getDefaultConfigs();
	}
	
	public void setControllerAssignScreenTexturePath(String controllerAssignScreenTexturePath) {
		controllerAssignScreenTexture = new Texture(Gdx.files.internal(controllerAssignScreenTexturePath), true);
		controllerAssignScreenTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}
	
	/** setMenuNavigationCommands
	 * commands for menuNavigation (game can store specific commands here for automatic usage in abstractSubMenu)
	 * optional */	
	public void setMenuNavigationCommands(int right, int left, int up, int down, int enter) {
		commandMenuRight = right;
		commandMenuLeft = left;
		commandMenuUp = up;
		commandMenuDown = down;
		commandEnterMenu = enter;
		autoCheckNavCommands = true;
	}
	
	public void addListener(ControllerListener listener) {
		Controllers.addListener(listener);
	}
	
	public void addDefaultConfig(String key, ControllerConfig2 config) {
		arrayDefaultControllerConfigs.put(key, config);
	}
	
	public void initializeControllers() { 	
		refreshControllers();
//		debugPrintConfigList();
		menuController = getInputController(1, camMgr.getHUDCamera());
	}
	
	//---- this method can be called to refresh all controllers after a new controller has been switched on 
	public void refreshControllers() {
		controllerConfigList 	= loadControllerConfigList(); 					
		controllerList 			= new ControllerList(game, this, controllerConfigList);	//getAvailableControllers();	// needs ControllerConfigList first!
		id = controllerConfigList.list.size();
	}
	
	public void updateGamePadControllers() {
		getControllerList().updateGamePadControllers();
	}
		
	//---- if inputcontroller1 != touchscreen => then show menuNavigationRectangle
	public void setBubblrActivateMenuSwitches() {
		
		game.activateMenuSwitches = true;
		
		if (ControllerAndroid.equals(inputController_PL1.getDescription()) || ControllerAndroidOnScreen.equals(inputController_PL1.getDescription())) {
		
			game.activateMenuSwitches = false;
		}
	}
	
	/** returns the controllerconfiguration for the type ControllerDescription based on the preferences value
	 * <p>
	 * The ControllerDescription parameter specifies the controllertype
	 * <p>
	 * @param ControllerDescription the type of controller to filter the controllerlist */
	public ControllerConfig2 getLastUsedConfig(int player, String ControllerDescription) {

		ControllerConfig2 config;
		ControllerConfig2 returnConfig = null;
		String searchConfig = game.getPreferences().getLastUsedConfigForController(player, ControllerDescription);

		int configCount = controllerConfigList.list.size();
		
		if (!"".equals(searchConfig)) {		// wenn prefs abfrage kein ergebnis gebracht hat, muss die config auch nicht gesucht werden
			
			for (int i = 0; i < configCount; i++) {
				
				config = controllerConfigList.list.get(i);
				
				if (config.name.equals(searchConfig)) {
					
					if (config.controllerDescription.equals(ControllerDescription)) {
						
						returnConfig = config;
						return returnConfig;
					}
				}
			}
		}
		
		//---- wenn in prefs keine passende config gefunden wurde, dann suche sysconfig
		if (returnConfig == null) {
						
			for (int i = 0; i < configCount; i++) {
				
				config = controllerConfigList.list.get(i);
				if (ControllerDescription.equals(config.controllerDescription)) {
					
					returnConfig = config;	// BUGFIX: 14.01.2014: da jetzt default configs unbekannter controller keine systemConfigs mehr sind, 
											// muss hier auf jeden fall die erstbeste config gespeichert werden, sonst nullpointer
		
					if (config.isSystemConfig == true) {
						
						returnConfig = config;
						return returnConfig;
					}
				}
			}
		}		
		return returnConfig;
	}
	
	//-----------------
	// checkForAutoConfigMode is needed because during init of contr.config.mgr checkForAutoConfigMode does not work
	public void setControllerConfig(int player, ControllerConfig2 controllerConfig, boolean checkForAutoConfigMode) {
		if (player == 1) {
			inputController_PL1.controllerConfig = controllerConfig;
		}
		if (player == 2) {
			inputController_PL2.controllerConfig = controllerConfig;
		}
		if (player == 3) {
			inputController_PL3.controllerConfig = controllerConfig;
		}
		if (player == 4) {
			inputController_PL4.controllerConfig = controllerConfig;
		}
		
		// hier nicht möglich da sonst crash wegen konstruktor des managers
//		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements
		

		//--- securitycheck bzgl. android wird in der methode gemacht
		// TODO: 2014-10-02: enable this!!----------------------------------------------------------------------------
//		if (checkForAutoConfigMode == true) checkForAutoConfigMode(player);	// checkForAutoConfigMode is needed because during init of contr.config.mgr checkForAutoConfigMode does not work
	}
	
	public void setCamera(int player, OrthographicCamera guiCam) {
		
		if (player == 1 && inputController_PL1 != null) inputController_PL1.guiCam = guiCam;
		if (player == 2 && inputController_PL2 != null) inputController_PL2.guiCam = guiCam;
		if (player == 3 && inputController_PL3 != null) inputController_PL3.guiCam = guiCam;
		if (player == 4 && inputController_PL4 != null) inputController_PL4.guiCam = guiCam;
	}
	
	public void setControllingDevice(int player, Controller controller) {
		if (player == 1) inputController_PL1.controller = controller;
		if (player == 2) inputController_PL2.controller = controller;
		if (player == 3) inputController_PL3.controller = controller;
		if (player == 4) inputController_PL4.controller = controller;
	}
	
	public int getControllerCount() {
		
		int controllerCount = Controllers.getControllers().size;
		if (game.getRunningDevice() == Game.ANDROID) controllerCount++;	// Android has Tilt/Touch by default (only add 1 controller for android not both android/androidonscreen)
		if (game.getRunningDevice() == Game.DESKTOP) controllerCount = controllerCount + 2;	// Desktop has Keys/Mouse by default (add 2 controllers for desktop!!!)
			
		return controllerCount; 
	}

	public String[] getControllerConfigStringList(String controllerDescription) {
		
		String[] stringList;
		int maxItems = 0;
		int counter = 0;
		
		//----------- determine how many items we will need ------------------------------------
		for (int i = 0; i < controllerConfigList.list.size(); i++) {
			
			if (controllerDescription.equals(controllerConfigList.list.get(i).controllerDescription)) maxItems++;
		}
		
		stringList = new String[maxItems];
		
		//-----------fill the special list ------------------------------------
		for (int i = 0; i < controllerConfigList.list.size(); i++) {
			
			if (controllerDescription.equals(controllerConfigList.list.get(i).controllerDescription)) {
				
				stringList[counter] = controllerConfigList.list.get(i).name;
				counter++;
			}
		}
		
		return stringList;
	}
	
	
    public AbstractController getMenuController() {
		return menuController;
    }
	
    public void setMenuController(AbstractController controller) {
		menuController = controller;
    }
    
    // here setMenuController searches the correct AbstractController
    public void setMenuController(Controller controller) {
		menuController = getInputController(controller);
    }
    
	// TODO
    public AbstractController getInputController(int player, OrthographicCamera guiCam) {
    	AbstractController returnController = null;
    	
		if (player == 1) {
			setCamera(1, guiCam);
			returnController = inputController_PL1;
		}
		if (player == 2) {
			setCamera(2, guiCam);
			returnController = inputController_PL2;
		}
		if (player == 3) {
			setCamera(3, guiCam);
			returnController = inputController_PL3;
		}
		if (player == 4) {
			setCamera(4, guiCam);
			returnController = inputController_PL4;
		}
		return returnController;
    }
    
    
    public AbstractController getInputController(int player) {
    	AbstractController returnController = null;
    	
 		if (player == 1) returnController = inputController_PL1;
 		if (player == 2) returnController = inputController_PL2;
 		if (player == 3) returnController = inputController_PL3;
 		if (player == 4) returnController = inputController_PL4;
 		
 		return returnController;
     }

    //---- get philEngineController by finding the inner controller -----
    public AbstractController getInputController(Controller controller) {
    	AbstractController returnController = null;
    	
		for (int i = 0; i < controllerList.list.size(); i++) {
			Controller listController = controllerList.list.get(i).controller;
			if (listController != null && listController == controller) {
				returnController = controllerList.list.get(i);
				break;
			}
		}
 		return returnController;
     }
    
    //---- set Controller as disconnected -----
    public void setControllerConnectionStatus(Controller controller, boolean isControllerDisconnected) {
    	    	
		for (int i = 0; i < controllerList.list.size(); i++) {
			AbstractController peController = controllerList.list.get(i);
			if (peController.controller != null && peController.controller == controller) {		// disconnected controller is not null, but reports FALSE boolean
				peController.setControllerDisconnected(isControllerDisconnected);
			}
		}
     }
    //---- set Controller as disconnected -----
    public boolean isControllerDisconnected(Controller controller) {
    	boolean returnbool = false;
		for (int i = 0; i < controllerList.list.size(); i++) {
			AbstractController peController = controllerList.list.get(i);
			if (peController != null) {		// disconnected controller is not null, but reports FALSE boolean
				returnbool = peController.isControllerDisconnected();
			}
		}
		return returnbool;
     }
    
    public void setInputController(int player, AbstractController contr) {
    	
 		if (player == 1) inputController_PL1 = contr;
 		if (player == 2) inputController_PL2 = contr;
 		if (player == 3) inputController_PL3 = contr;
 		if (player == 4) inputController_PL4 = contr;
 		
// 		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements
     }

    
    public ControllerConfig2 getControllerConfig(int player) {
    	ControllerConfig2 returnconfig = null;
    	
 		if (player == 1) returnconfig = inputController_PL1.controllerConfig;
 		if (player == 2) returnconfig = inputController_PL2.controllerConfig;
 		if (player == 3) returnconfig = inputController_PL3.controllerConfig;
 		if (player == 4) returnconfig = inputController_PL4.controllerConfig;
 		
 		return returnconfig;
     }
    
    
	public void deleteConfig(int player, ControllerConfig2 controllerConfig) {
		
		getControllerConfigList().deleteConfig(controllerConfig);

		ControllerConfig2 newConf = null;
		
		if (player == 1) {
			newConf = getLastUsedConfig(player, inputController_PL1.getDescription());
			if (newConf == null) newConf = generateAndRegisterNewConfig(inputController_PL1.getDescription(), "new", player, false);
			setControllerConfig(player, newConf, true);
		}
		if (player == 2) {
			newConf = getLastUsedConfig(player, inputController_PL2.getDescription());
			if (newConf == null) newConf = generateAndRegisterNewConfig(inputController_PL2.getDescription(), "new", player, false);
			setControllerConfig(player, newConf, true);
		}
		if (player == 3) {
			newConf = getLastUsedConfig(player, inputController_PL3.getDescription());
			if (newConf == null) newConf = generateAndRegisterNewConfig(inputController_PL3.getDescription(), "new", player, false);
			setControllerConfig(player, newConf, true);
		}
		if (player == 4) {
			newConf = getLastUsedConfig(player, inputController_PL4.getDescription());
			if (newConf == null) newConf = generateAndRegisterNewConfig(inputController_PL4.getDescription(), "new", player, false);
			setControllerConfig(player, newConf, true);
		}
		
//		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements
	}	
    
	public ControllerList getControllerList() {
		return controllerList;
	}
	
	public ControllerConfigList getControllerConfigList() {	
		return controllerConfigList;
	}
	
	
  public void switchController(int player, AbstractController contr, OrthographicCamera guiCam, Controller device, ControllerConfig2 config, boolean checkForAutoConfigMode) {

 		if (player == 1) inputController_PL1 = contr;
 		if (player == 2) inputController_PL2 = contr;
 		if (player == 3) inputController_PL3 = contr;
 		if (player == 4) inputController_PL4 = contr;
 		
 		setControllerConfig(player, config, checkForAutoConfigMode);
 		setCamera(player, guiCam);
 		setControllingDevice(player, device);
// 		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements
  }
	
  //--- dependency-injection for the onscreen joystick to avoid dependencies between this manager and assets of the project
  public void setTouchpadForOnScreenJoystick(Touchpad touchpad) {
	  
	this.touchpad = touchpad;  
  }
  
  public boolean isOnScreenJoystickAvailable() {
	  
	  boolean available = false;
	  if (touchpad != null) available = true;
	  
	  return available;
  }
  
	/** enables / disables the on screen joystick for the given controller. Afterwards it checks if jump/fire-buttons have to be drawn and sets correct position depending on the config
	 * <p>
	 * <p>used by gamescreen
	 * <p>
	 * @param player
	 * @param contr */
  public void switchOnScreenStick(int player, AbstractController contr) {

		if (ControllerAndroidOnScreen.equals(contr.getDescription())) {
		
//			TouchpadStyle touchpadStyle = new TouchpadStyle(new FixedTextureRegionDrawable(Assets.joystickBackground, 140, 140), new FixedTextureRegionDrawable(Assets.joystickKnob, 34, 34)); 
//			contr.touchpad = new Touchpad(20, touchpadStyle);
			contr.touchpad = touchpad;
			contr.useTouchpad = true;
		}
		else {
			contr.useTouchpad = false;
			contr.touchpad = null;
		}
		//------------------------------------------------------------------
//		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements and where
  }
  
  	//---------------------------------------------------------------------------
  
  	// used bei controllerchooserscreen.new(), controllerchooserscreen.checkChoosenController(), ControllerList, ...
  	// configName = "new" => "New" + AutoID // configName = "default" => wert aus configDefaults wird übernommen // ansonsten wird der uebernommene name verwendet
	public ControllerConfig2 generateAndRegisterNewConfig(String controllerDescription, String configName, int player, boolean isSystemConfig) {
		
		ControllerConfig2 newConfig = getClonedDefaultConfig(controllerDescription);
		if (newConfig != null) {
			newConfig.controllerDescription = controllerDescription;
			newConfig.isSystemConfig = isSystemConfig;
		
			//---- jetzt neuen namen vergeben, da sonst ggf. der name der default config vergeben worden ist
			if (!"default".equals(configName)) {
	
				if ("new".equals(configName)) {
					id++;
					newConfig.name = "new" + id;
				}
				else {
					newConfig.name = configName;
				}
			}
			//------------------------------------
			
			getControllerConfigList().addConfig(newConfig);		
		}

		return newConfig;
	}
  
	//---------------------------------------------------------------------------
  
	/** returns the last used controller or alternatively a defaultcontroller if the last used is not existent in the current controllerlist
	 * <p>
	 * <p>
	 * <p>
	 * @param searchController this is the wanted controller
	 * @param alternativController put the name of the default controller here (should be always in the list)
	 * @param deviceNumber if there are multiple devices with the same name/description then this is the number of the returned device  */
	public AbstractController getController(String searchController, String alternativController, int deviceNumber) {
		
		AbstractController returnContr = null;
		
		//------ zuerst nach dem gewuenschten controller suchen
		if (!"".equals(searchController)) returnContr = getController(searchController, deviceNumber);
		
		//--- falls ergebnislos, dann nach dem alternativcontroller suchen
		if (returnContr == null) returnContr = getController(alternativController, deviceNumber);
		
		return returnContr;
		
	}
	
	/** returns the a controller from the current controllerlist by the given name
	 * <p>
	 * <p>
	 * <p>
	 * @param contr this is the wanted controllername
	 * @param deviceNumber if there are multiple devices with the same name/description then this is the number of the returned device  */
	public AbstractController getController(String contr, int deviceNumber) {

		int deviceCounter = 0;
		AbstractController returnContr = null;
		AbstractController tempContr;
		int size = controllerList.list.size();

		for (int i = 0; i < size; i++) {
			
			tempContr = controllerList.list.get(i);
			if (tempContr.getDescription().equals(contr)) {
				
				deviceCounter ++;
				
				if (deviceCounter == deviceNumber) {
					
					returnContr = tempContr; 
					break; 
				}
			}
		}	
		return returnContr;
	}
		
	public int countControllersWithSameDescription(String description) {
		
		AbstractController returnContr = null;
		int size = controllerList.list.size();
		int i;
		int counter = 0;
		
		for (i = 0; i < size; i++) {
			
			returnContr = controllerList.list.get(i);
			if (returnContr.getDescription().equals(description)) {
				
				counter++; 
			}
		}
		return counter;
	}
	
	public int findControllerIndexByControllerID(AbstractController gameController) {
		
		AbstractController returnContr = null;
		int size = controllerList.list.size();
		int i;
		
		for (i = 0; i < size; i++) {
			
			returnContr = controllerList.list.get(i);
			if (returnContr.getDescription().equals(gameController.getDescription())) {
				
				if (returnContr.controller == gameController.controller) break; 
			}
		}
		return i;
	}  
  
	  public boolean isUsingOnScreenStick(int player) {
		  boolean returnBoolean = false;
		  
			if (player == 1) returnBoolean = inputController_PL1.useTouchpad;
			if (player == 2) returnBoolean = inputController_PL2.useTouchpad;
			if (player == 3) returnBoolean = inputController_PL3.useTouchpad;
			if (player == 4) returnBoolean = inputController_PL4.useTouchpad;
			
			return returnBoolean;
	  } 
  
//--------------------------------------------------------
  
  
  
	public void saveConfigsAsDefault() {		
		game.getPreferences().setLastUsedController(1, inputController_PL1.getDescription());
		game.getPreferences().setLastUsedController(2, inputController_PL2.getDescription());
		game.getPreferences().setLastUsedController(3, inputController_PL3.getDescription());
		game.getPreferences().setLastUsedController(4, inputController_PL4.getDescription());
		
		game.getPreferences().setLastUsedConfigForController(1, inputController_PL1.getDescription(), inputController_PL1.controllerConfig.name);
		game.getPreferences().setLastUsedConfigForController(2, inputController_PL2.getDescription(), inputController_PL2.controllerConfig.name);
		game.getPreferences().setLastUsedConfigForController(3, inputController_PL3.getDescription(), inputController_PL3.controllerConfig.name);
		game.getPreferences().setLastUsedConfigForController(4, inputController_PL4.getDescription(), inputController_PL4.controllerConfig.name);
	}
		 
  //---------------------------------------------------
  
  
  	private ControllerConfigList getUserconfigs() {
		
  		ControllerConfigList list = new ControllerConfigList();
  		
		for (int i = 0; i < controllerConfigList.size(); i++) {
			
			if (controllerConfigList.list.get(i).isSystemConfig == false) {
				
				list.addConfig(controllerConfigList.list.get(i));
			}
		}
  		
  		return list; 		
  	}
  
 
	public void saveConfigsToJson()  {
    	
    	if (FileSystem.isDirAvailable(CONFIG_DIR, game.getDebugMode())) {
			
    		String path = CONFIG_DIR + "/" + "configs";
			profileDataFile = Gdx.files.local( path );
	      
			Json json = new Json();
//			json.setUsePrototypes(false); 			// force to save all data!
			
			// convert the given gameState to text
//			String profileAsText = json.toJson( controllerConfigList, ControllerConfigList.class );
			
			String profileAsText = json.prettyPrint( getUserconfigs() );
	      
			// encode the text
//	    	String profileAsCode = Base64Coder.encodeString( profileAsText );	// "verschlüsseln" damit nicht jeder dummy editieren kann
	  
	    	// write the profile data file
			profileDataFile.writeString( profileAsText, false );
//			Gdx.app.log("ControllerConfigs", "Configs have been written");
    	}
    }
    
	
    //----- load GameState from JSON or return a blank GameState ----------------------
    private ControllerConfigList loadControllerConfigList()  {
               	
        if (FileSystem.isDirAvailable(CONFIG_DIR, game.getDebugMode())) {
        	
        	if (isConfigAvailable(false)) {
        		
        		return loadConfigfromJson(game.getDebugMode());
        	}
        	else return new ControllerConfigList(); 
        }
        else return new ControllerConfigList();
    }
    
   
	private ControllerConfigList loadConfigfromJson(boolean log)  {
	        
		ControllerConfigList list = new ControllerConfigList();
		
		// create the handle for the profile data file
		String path = CONFIG_DIR + "/" + "configs";
		profileDataFile = Gdx.files.local( path );
	
	    // create the JSON utility object
	    Json json = new Json();
	
	    // check if the profile data file exists
	    if( profileDataFile.exists() ) {
	
	        // load the profile from the data file
	        try {
	
	            // read the file as text
	            String profileAsCode = profileDataFile.readString();
	
	            // decode the contents
	//            String profileAsText = Base64Coder.decodeString( profileAsCode );	// "entschlüsseln" damit nicht jeder dummy editieren kann
	
	            // restore the state
	//            json.setElementType(GameStateList.class, "list", GameState.class);
	            list = json.fromJson(ControllerConfigList.class, profileAsCode );
	            
	            
	            if (log) Gdx.app.log("configs", "configs have been successfully read!");
	            
	        } catch( Exception e ) {
	
	            // log the exception
	        	if (log) Gdx.app.log("ERROR", "Unable to parse existing profile data file", e );
	
	            // recover by creating a fresh new profile data file;
	            // note that the player will lose all game progress
	//            profile = new Profile();
	//            persist( profile );
	
	        }
	
	    } else {
	    	if (log) Gdx.app.log("configs", "File doesnt exist!");
	    	// create a new profile data file
	//        profile = new Profile();
	//        persist( profile );
	    }
	
	    return list;
	}
	
	
	private boolean isConfigAvailable(boolean log) {
		
		boolean isConfigAvailable = false;
		
		// create the handle for the profile data file
		String path = CONFIG_DIR + "/" + "configs";
		profileDataFile = Gdx.files.local( path );
	
	    // check if the profile data file exists
	    if( profileDataFile.exists() ) {
	    
	    	if (log) Gdx.app.log("configs", "file exists");
	    	
	    	if( profileDataFile.isDirectory() == false ) {
	    		
	    		isConfigAvailable = true;
	    		if (log) Gdx.app.log("configs", "config exists");
	    	}
	    	else {
	    		if (log) Gdx.app.log("configs", "config is a DIR!");
	    	}
	    }
	    else {
	    	if (log) Gdx.app.log("configs", "file does NOT exist!");
	    }
	
		return isConfigAvailable;
	}

    //---------------------------------------------------
	
	void checkIfSomeOneIsUsingTouchscreen() {
		
		isSomeOneUsingTouchscreen = 0;
		
		if (inputController_PL1 != null && 
				(inputController_PL1.getDescription().equals(MgrControllerConfigManager.ControllerAndroid) || 
				inputController_PL1.getDescription().equals(MgrControllerConfigManager.ControllerAndroidOnScreen))) {
			
			isSomeOneUsingTouchscreen = 1;
		}
		if (inputController_PL2 != null && 
				(inputController_PL2.getDescription().equals(MgrControllerConfigManager.ControllerAndroid) ||
				inputController_PL2.getDescription().equals(MgrControllerConfigManager.ControllerAndroidOnScreen))) {
			
			isSomeOneUsingTouchscreen = 2;	
		}
	}

	public int isSomeOneUsingTouchscreen(boolean update) {
		
		if (update == true) checkIfSomeOneIsUsingTouchscreen();
				
		return isSomeOneUsingTouchscreen;
	}
	
	//---------------------------------------------
	
	public void save_all() {
		
		saveConfigsAsDefault();
		saveConfigsToJson();
	}
	
	/**
	 * copie the injected arraz of game commands to use the copz for an new controller config
	 */
	private Command[] getNewCommandsArray() {
		Command[] array2 = new Command[arrayCommands.length];
		System.arraycopy(arrayCommands, 0, array2, 0, arrayCommands.length);
		return array2;
	}
	
	private ControllerConfig2 getClonedDefaultConfig(String controllerDescription) {
		if (arrayDefaultControllerConfigs.containsKey(controllerDescription)) {
			ControllerConfig2 newconf = arrayDefaultControllerConfigs.get(controllerDescription).deepCopy();
			return newconf;
		}
		return null;
	}
	
	public Command[] getArrayCommands() {
		return arrayCommands;
	}
}


//==========================================================


//public class MgrControllerConfigManager {
//	
//	public enum ControllerType {
//		UNDEFINED,
//		TOUCHSCREEN,
//		GAMEPAD,
//		KEYBOARD,
//		AI
//	}
//	
//	protected static MgrControllerConfigManager instance;
//	Game game;
//	public int id = 0; // zum nummerieren bei newConfig()
//	
//	protected BaseControllerCommands controllerCommands;
//	private Command[] arrayCommands;	// this will be set by dependency injection from the game and will be used to generate configs...
//	protected HashMap<String, ControllerConfig2> arrayDefaultControllerConfigs;
//		
//	//---- OnScreenJoystick ------
//	Touchpad touchpad = null;	// this will be set by dependencyInjection ans will be handled to the controller if the androidOnScreenController is choosen
//	
//	private static final String CONFIG_DIR 	= ".configs";
//	FileHandle profileDataFile;
//	private MgrCameraManager camMgr;
//	
//	public static final String NULL = "NULL";
//	
//	public static final String ControllerNONE = "- none -";
//	public static final String ControllerDesktopPL1 = "Keys/Mouse PL1";
//	public static final String ControllerDesktopPL2 = "Keys/Mouse PL2";
//	public static final String ControllerAndroid = "Accelerometer"; 	// "Touchscreen";
//	public static final String ControllerAndroidOnScreen = "OnScreen-Stick";
//	public static final String ControllerSaitek = "Saitek Cyborg Evo";
//	public static final String ControllerAmazonFire = "Amazon Fire Game Controller";
//	public static final String ControllerFireTVRemote = "Amazon Fire TV Remote";
//	public static final String ControllerPS4 = "Sony Computer Entertainment Wireless Controller";
//	public static final String ControllerOUYA = OuyaCodes.ID;
//	
//	public static final String ConfigDefault = "default";
////	public static final String ConfigAndroidFireRight = "fire right";	
////	public static final String ConfigAndroidFireLeft = "fire left";	
//	
//	public ControllerList controllerList;
//	public ControllerConfigList controllerConfigList;
//	
//	AbstractController menuController;
//	AbstractController inputController_PL1;
//	AbstractController inputController_PL2;
//	AbstractController inputController_PL3;
//	AbstractController inputController_PL4;
//	
//	public static final int UNCONFIGURED = -1;		// soll bei den controllercodes angeben ob eine achse deaktiviert wurde damit buttons genutzt werden können
//													// evtl. überflüssig weil man ja auch beides gleichzeitg nutzen könnte?
//	int isSomeOneUsingTouchscreen;
//	 
//	//----- bounds für jump/fire button -------------
//	public Vector2	 	jumpButtonLocation; 
//	public Rectangle	jumpButtonClickRegion;
//	public Vector2	 	fireButtonLocation; 
//	public Rectangle	fireButtonClickRegion;
//	
//	public MgrControllerConfigManager(Game game) {		
//		this.game = game;
//		this.camMgr = game.getCameraManager();
//	}
//	
////	public MgrControllerConfigManager(Game game, Command[] arrayCmd, HashMap<String, ControllerConfig2> defaultControllerConfigs) {		
////	
////		this.game = game;
////		this.arrayCommands = arrayCmd;	// this will be set by dependencyInjection and used to make new configs by COPYING this array
////		this.arrayDefaultControllerConfigs = defaultControllerConfigs;
////		this.camMgr = game.getCameraManager();
////	}
//	
//	//--- singleton with lazy initialization
//    public static MgrControllerConfigManager getInstance(Game game) {
//        if(instance == null) {
//            instance = new MgrControllerConfigManager(game);
//            Gdx.app.log("ControllerConfigManager", "new MgrControllerConfigManager() constructed");
//        }
//        return instance;
//    }
//    		
//	/** first initialization
//	 * game specific defaultControllerConfigs in an array and initializes the controllerMgr with it
//	 * @param HashMap<String, ControllerConfig2> */	
//	public void initialize(BaseControllerCommands controllerCommands) { 	
//		this.controllerCommands = controllerCommands;
//		this.arrayCommands = controllerCommands.getNewCommandsArray();
//		this.arrayDefaultControllerConfigs = controllerCommands.getDefaultConfigs();
//	}
//	
//	public void addDefaultConfig(String key, ControllerConfig2 config) {
//		arrayDefaultControllerConfigs.put(key, config);
//	}
//	
//	public void initializeControllers() { 	
//		refreshControllers();
////		debugPrintConfigList();
////		getDefaultController();
//		menuController = getInputController(1, camMgr.getHUDCamera());
//	}
//	
//	//---- this method can be called to refresh all controllers after a new controller has been switched on 
//	public void refreshControllers() {
//		controllerConfigList 	= loadControllerConfigList(); 					
//		controllerList 			= new ControllerList(game, this, controllerConfigList);	//getAvailableControllers();	// needs ControllerConfigList first!
//		id = controllerConfigList.list.size();
//	}
//	
//	public void updateGamePadControllers() {
//		getControllerList().updateGamePadControllers();
//	}
//	
//
////	public void nullAllControllers() {
////		inputController_PL1 	= null;
////		inputController_PL2 	= null;
////		inputController_PL3 	= null;
////		inputController_PL4 	= null;
////	}
//	
////	public void getDefaultController() {
////		
////		AbstractController setContr;
////		ControllerConfig2 setConfig;
////		
////		inputController_PL1 	= getDefaultController(1, true);
////		inputController_PL2 	= getDefaultController(2, true);
////		inputController_PL3 	= getDefaultController(3, true);
////		inputController_PL4 	= getDefaultController(4, true);
////		
////		if (inputController_PL1 == null) return;	// wenn kein ouya controller gefunden wird soll das game nicht crashen!
////		
////		//---- check ob zufeallig 2 mal android gewaehlt wurde ....  dann player 2 controller loeschen. (kann passieren wenn vorher ein anderer controller in verwendung war)
////		if (inputController_PL1.getDescription().equals(ControllerAndroid) || inputController_PL1.getDescription().equals(ControllerAndroidOnScreen)) {
////			if (inputController_PL2.getDescription().equals(ControllerAndroid) || inputController_PL2.getDescription().equals(ControllerAndroidOnScreen)) {
////				inputController_PL2 = getController(ControllerNONE, 1);
////			}
////		}
////		if (inputController_PL1 != null) {
////			setConfig 	= getLastUsedConfig(1, inputController_PL1.getDescription());
////			if (setConfig == null) {	// security-check für den fall der faelle, dass zu einem lastUsedController keine config mehr da ist ..... dann suche nach default controller
////				inputController_PL1 	= getDefaultController(1, false);
////				setConfig 	= getLastUsedConfig(1, inputController_PL1.getDescription());
////			}
////			switchController(1, inputController_PL1, null, inputController_PL1.controller, setConfig, false);
////		}
////		if (inputController_PL2 != null) {
////			setConfig	= getLastUsedConfig(2, inputController_PL2.getDescription());
////			if (setConfig == null) {	// security-check für den fall der faelle, dass zu einem lastUsedController keine config mehr da ist ..... dann suche nach default controller // darf eigentlich nicht passieren dass ein controller lastUsed war aber keine config existiert
////				inputController_PL2 	= getDefaultController(2, false);
////				setConfig	= getLastUsedConfig(2, inputController_PL2.getDescription());
////			}
////			switchController(2, inputController_PL2, null, inputController_PL2.controller, setConfig, false);
////		}
////		if (inputController_PL3 != null) {
////			setConfig	= getLastUsedConfig(3, inputController_PL3.getDescription());
////			if (setConfig == null) {	// security-check für den fall der faelle, dass zu einem lastUsedController keine config mehr da ist ..... dann suche nach default controller // darf eigentlich nicht passieren dass ein controller lastUsed war aber keine config existiert
////				inputController_PL3 	= getDefaultController(3, false);
////				setConfig	= getLastUsedConfig(3, inputController_PL3.getDescription());
////			}
////			switchController(3, inputController_PL3, null, inputController_PL3.controller, setConfig, false);
////		}
////		if (inputController_PL4 != null) {
////			setConfig	= getLastUsedConfig(4, inputController_PL4.getDescription());
////			if (setConfig == null) {	// security-check für den fall der faelle, dass zu einem lastUsedController keine config mehr da ist ..... dann suche nach default controller // darf eigentlich nicht passieren dass ein controller lastUsed war aber keine config existiert
////				inputController_PL4 	= getDefaultController(4, false);
////				setConfig	= getLastUsedConfig(4, inputController_PL4.getDescription());
////			}
////			switchController(4, inputController_PL4, null, inputController_PL4.controller, setConfig, false);
////		}
////		setBubblrActivateMenuSwitches();
////	}
//	
//	//----------------------------------------------------------------------------------------------
////	public void debugPrintConfigList() {
////		
////		for (int i = 0; i < controllerConfigList.list.size(); i++) {
////
////			Gdx.app.log("", "" 
////					+ controllerConfigList.list.get(i).controllerDescription
////					+ " (" + controllerConfigList.list.get(i).name + ") // "
////					+ "isSystem: " + controllerConfigList.list.get(i).isSystemConfig + " // "
////					);
////		}
////	}
//	//----------------------------------------------------------------------------------------------
//	
//	
//	
////	// muss fuer nicht vorhandene Player / controller ControllerNONE zurueck geben !!!!
////	public AbstractController getDefaultController(int player, boolean tryLastUsed) {		
////		
////		String searchController = "";
////		AbstractController returnController = null;
////		
////		if (tryLastUsed) searchController = game.getPreferences().getLastUsedController(player);
////		
////		switch (game.getRunningDevice()) {
////		
////		case Game.AMAZONFIRETV:	
////
////			// if now known controllers -> use fire tv remote 
////			if (controllerList.getKnownControllerCounter() == 1) {
////			
////				if (player == 1) returnController = getController(searchController, ControllerFireTVRemote, 1);
////				if (player > 1) returnController = getController(ControllerNONE, 1);	// wenn == null dann crasht das game wenn gamemode die controller checken will mit nullpointer
////			}
////			
////			else {
////				
////				if (player == 1) {
////					if (tryLastUsed) {
////						returnController = getController(searchController, ControllerAmazonFire, 1); 
////					}
////					else returnController = getController(ControllerAmazonFire, 1);
////					
////					if (returnController == null) {
////						returnController = getController(ControllerFireTVRemote, 1);
////					}
////				}
////				
////				if (player == 2) {	
////					if (controllerList.getKnownControllerCounter() > 1) { 	// getOUYAcontrollerCounter
////					
////						if (tryLastUsed) {
////							returnController = getController(searchController, ControllerAmazonFire, 2); // new ControllerOUYA(game);
////						}
////						else returnController = getController(ControllerAmazonFire, 2);
////					}
////					else {
////						if (tryLastUsed) {
////							returnController = getController(searchController, ControllerNONE, 1); // new ControllerNone(game); //new ControllerOUYA(game);
////						}
////						else returnController = getController(ControllerNONE, 1);
////					}
////					if (returnController == null) returnController = new ControllerNone(game, MgrControllerConfigManager.ControllerNONE);
////				}
////				
////				if (player == 3) {	
////					if (controllerList.getKnownControllerCounter() > 2) { 	// getOUYAcontrollerCounter
////					
////						if (tryLastUsed) {
////							returnController = getController(searchController, ControllerAmazonFire, 3); // new ControllerOUYA(game);
////						}
////						else returnController = getController(ControllerAmazonFire, 3);
////					}
////					else returnController = getController(ControllerNONE, 1);
////					
////					if (returnController == null) returnController = new ControllerNone(game, MgrControllerConfigManager.ControllerNONE);
////				}
////				
////				if (player == 4) {	
////					if (controllerList.getKnownControllerCounter() > 3) { 	// getOUYAcontrollerCounter
////					
////						if (tryLastUsed) {
////							returnController = getController(searchController, ControllerAmazonFire, 4); // new ControllerOUYA(game);
////						}
////						else returnController = getController(ControllerAmazonFire, 4);
////					}
////					else returnController = getController(ControllerNONE, 1);
////					
////					if (returnController == null) returnController = new ControllerNone(game, MgrControllerConfigManager.ControllerNONE);
////				}
////			}
////        	break;
////        
////        //--------------------------------------
////        	
////		case Game.OUYA:	
////
////			if (player == 1) {
////				if (tryLastUsed) {
////					returnController = getController(searchController, OuyaCodes.ID, 1); // new ControllerOUYA(game);
////				}
////				else returnController = getController(OuyaCodes.ID, 1);
////				
////				if (returnController == null) {
////					returnController = getController(ControllerAmazonFire, 1);
////				}
////			}
////			
////			if (player == 2) {	
////				if (controllerList.getKnownControllerCounter() > 1) { 	// getOUYAcontrollerCounter
////				
////					if (tryLastUsed) {
////						returnController = getController(searchController, OuyaCodes.ID, 2); // new ControllerOUYA(game);
////					}
////					else returnController = getController(OuyaCodes.ID, 2);
////				}
////				else {
////					if (tryLastUsed) {
////						returnController = getController(searchController, ControllerNONE, 1); // new ControllerNone(game); //new ControllerOUYA(game);
////					}
////					else returnController = getController(ControllerNONE, 1);
////				}
////				if (returnController == null) returnController = new ControllerNone(game, MgrControllerConfigManager.ControllerNONE);
////			}
////			
////			if (player == 3) {	
////				if (controllerList.getKnownControllerCounter() > 2) { 	// getOUYAcontrollerCounter
////				
////					if (tryLastUsed) {
////						returnController = getController(searchController, OuyaCodes.ID, 3); // new ControllerOUYA(game);
////					}
////					else returnController = getController(OuyaCodes.ID, 3);
////				}
////				else returnController = getController(ControllerNONE, 1);
////				
////				if (returnController == null) returnController = new ControllerNone(game, MgrControllerConfigManager.ControllerNONE);
////			}
////			
////			if (player == 4) {	
////				if (controllerList.getKnownControllerCounter() > 3) { 	// getOUYAcontrollerCounter
////				
////					if (tryLastUsed) {
////						returnController = getController(searchController, OuyaCodes.ID, 4); // new ControllerOUYA(game);
////					}
////					else returnController = getController(OuyaCodes.ID, 4);
////				}
////				else returnController = getController(ControllerNONE, 1);
////				
////				if (returnController == null) returnController = new ControllerNone(game, MgrControllerConfigManager.ControllerNONE);
////			}
////        	break;
////        	
////        //--------------------------------------
////        	
////        case Game.ANDROID:	
////        	
////        	if (player == 1) {
////        		if (tryLastUsed) {
////        			returnController = getController(searchController, ControllerAndroid, 1); // new ControllerAndroid(game); // ControllerAndroidOnScreen
////        		}
////        		else returnController = getController(ControllerAndroid, 1);
////        	}
////        	if (player == 2) {
////        		if (tryLastUsed) {
////        			returnController = getController(searchController, ControllerNONE, 1); // new ControllerNone(game);
////        		}
////        		else returnController = getController(ControllerNONE, 1);
////        	}
////			break;
////		
////		//--------------------------------------
////		
////		case Game.DESKTOP:	
////			// hier muss aufgepasst werden, weil ControllerDesktop 2mal vorhanden ist, der searchController jedoch wahrscheinlich nicht
////			if (player == 1) {
////				if (tryLastUsed) {
////					if (!searchController.equals(ControllerDesktopPL1)) {
////						returnController = getController(searchController, 1); // new ControllerDesktop(game);		//--- controller for menuscreens
////					}
////					if (searchController.equals(ControllerDesktopPL1) || returnController == null) {
////						
////						returnController = getController(ControllerDesktopPL1, 1); // new ControllerDesktop(game);		//--- controller for menuscreens
////					}
////				}
////				else returnController = getController(ControllerDesktopPL1, 1);
////			}
////			if (player == 2) {
////				
////				if (tryLastUsed) {
////					if (!searchController.equals(ControllerDesktopPL2)) {
////						
////						returnController = getController(searchController, 1); // new ControllerDesktop(game);		//--- controller for menuscreens
////					}
////					if (searchController.equals(ControllerDesktopPL2) || returnController == null) {
////	
////						returnController = getController(ControllerDesktopPL2, 1); // new ControllerDesktop(game);		//--- controller for menuscreens
////					}
////				}
////				else returnController = getController(ControllerDesktopPL2, 1);
////			}
////        	break;	
////		}	
////		
////		return returnController;
////	}
//	
////	/** offers (and needs) dependency injection for controllerMgr initialization this takes the game specific controls in an array
////	 * and initializes the controllerMgr with it
////	 * @param arrayCmd */
////	public void setControllerCommandsArray (Command[] arrayCmd) {
////		this.arrayCommands = arrayCmd;
////	}
////	
////
////	/** offers (and needs) dependency injection for controllerMgr initialization this takes the game specific
////	 * defaultControllerConfigs in an array and initializes the controllerMgr with it
////	 * @param HashMap<String, ControllerConfig2> */
////	public void setDefaultConfigsMap (HashMap<String, ControllerConfig2> defaultControllerConfigs) {
////		this.arrayDefaultControllerConfigs = defaultControllerConfigs;
////	}
//	
//	//---- if inputcontroller1 != touchscreen => then show menuNavigationRectangle
//	public void setBubblrActivateMenuSwitches() {
//		
//		game.activateMenuSwitches = true;
//		
//		if (ControllerAndroid.equals(inputController_PL1.getDescription()) || ControllerAndroidOnScreen.equals(inputController_PL1.getDescription())) {
//		
//			game.activateMenuSwitches = false;
//		}
//	}
//	
//	
//	/** returns the controllerconfiguration for the type ControllerDescription based on the preferences value
//	 * <p>
//	 * The ControllerDescription parameter specifies the controllertype
//	 * <p>
//	 * @param ControllerDescription the type of controller to filter the controllerlist */
//	public ControllerConfig2 getLastUsedConfig(int player, String ControllerDescription) {
//
//		ControllerConfig2 config;
//		ControllerConfig2 returnConfig = null;
//		String searchConfig = game.getPreferences().getLastUsedConfigForController(player, ControllerDescription);
//
//		int configCount = controllerConfigList.list.size();
//		
//		if (!"".equals(searchConfig)) {		// wenn prefs abfrage kein ergebnis gebracht hat, muss die config auch nicht gesucht werden
//			
//			for (int i = 0; i < configCount; i++) {
//				
//				config = controllerConfigList.list.get(i);
//				
//				if (config.name.equals(searchConfig)) {
//					
//					if (config.controllerDescription.equals(ControllerDescription)) {
//						
//						returnConfig = config;
//						return returnConfig;
//					}
//				}
//			}
//		}
//		
//		//---- wenn in prefs keine passende config gefunden wurde, dann suche sysconfig
//		if (returnConfig == null) {
//						
//			for (int i = 0; i < configCount; i++) {
//				
//				config = controllerConfigList.list.get(i);
//				if (ControllerDescription.equals(config.controllerDescription)) {
//					
//					returnConfig = config;	// BUGFIX: 14.01.2014: da jetzt default configs unbekannter controller keine systemConfigs mehr sind, 
//											// muss hier auf jeden fall die erstbeste config gespeichert werden, sonst nullpointer
//		
//					if (config.isSystemConfig == true) {
//						
//						returnConfig = config;
//						return returnConfig;
//					}
//				}
//			}
//		}		
//		return returnConfig;
//	}
//	
//
//	
//	//-----------------
//	// checkForAutoConfigMode is needed because during init of contr.config.mgr checkForAutoConfigMode does not work
//	public void setControllerConfig(int player, ControllerConfig2 controllerConfig, boolean checkForAutoConfigMode) {
//		if (player == 1) {
//			inputController_PL1.controllerConfig = controllerConfig;
//		}
//		if (player == 2) {
//			inputController_PL2.controllerConfig = controllerConfig;
//		}
//		if (player == 3) {
//			inputController_PL3.controllerConfig = controllerConfig;
//		}
//		if (player == 4) {
//			inputController_PL4.controllerConfig = controllerConfig;
//		}
//		
//		// hier nicht möglich da sonst crash wegen konstruktor des managers
////		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements
//		
//
//		//--- securitycheck bzgl. android wird in der methode gemacht
//		// TODO: 2014-10-02: enable this!!----------------------------------------------------------------------------
////		if (checkForAutoConfigMode == true) checkForAutoConfigMode(player);	// checkForAutoConfigMode is needed because during init of contr.config.mgr checkForAutoConfigMode does not work
//	}
//	
//
//	
//	public void setCamera(int player, OrthographicCamera guiCam) {
//		
//		if (player == 1 && inputController_PL1 != null) inputController_PL1.guiCam = guiCam;
//		if (player == 2 && inputController_PL2 != null) inputController_PL2.guiCam = guiCam;
//		if (player == 3 && inputController_PL3 != null) inputController_PL3.guiCam = guiCam;
//		if (player == 4 && inputController_PL4 != null) inputController_PL4.guiCam = guiCam;
//	}
//
//	// TODO
////	public void setOwner(int player, character_goodGuy owner) {
////		if (player == 1) inputController_PL1.owner = owner;
////		if (player == 2) inputController_PL2.owner = owner;
////	}
//	
//	public void setControllingDevice(int player, Controller controller) {
//		if (player == 1) inputController_PL1.controller = controller;
//		if (player == 2) inputController_PL2.controller = controller;
//		if (player == 3) inputController_PL3.controller = controller;
//		if (player == 4) inputController_PL4.controller = controller;
//	}
//	
//	
//	public int getControllerCount() {
//		
//		int controllerCount = Controllers.getControllers().size;
//		if (game.getRunningDevice() == Game.ANDROID) controllerCount++;	// Android has Tilt/Touch by default (only add 1 controller for android not both android/androidonscreen)
//		if (game.getRunningDevice() == Game.DESKTOP) controllerCount = controllerCount + 2;	// Desktop has Keys/Mouse by default (add 2 controllers for desktop!!!)
//			
//		return controllerCount; 
//	}
//
//	
//		
//	public String[] getControllerConfigStringList(String controllerDescription) {
//		
//		String[] stringList;
//		int maxItems = 0;
//		int counter = 0;
//		
//		//----------- determine how many items we will need ------------------------------------
//		for (int i = 0; i < controllerConfigList.list.size(); i++) {
//			
//			if (controllerDescription.equals(controllerConfigList.list.get(i).controllerDescription)) maxItems++;
//		}
//		
//		stringList = new String[maxItems];
//		
//		//-----------fill the special list ------------------------------------
//		for (int i = 0; i < controllerConfigList.list.size(); i++) {
//			
//			if (controllerDescription.equals(controllerConfigList.list.get(i).controllerDescription)) {
//				
//				stringList[counter] = controllerConfigList.list.get(i).name;
//				counter++;
//			}
//		}
//		
//		return stringList;
//	}
//	
//	
//    public AbstractController getMenuController() {
//		return menuController;
//    }
//	
//    public void setMenuController(AbstractController controller) {
//		menuController = controller;
//    }
//    
//    // here setMenuController searches the correct AbstractController
//    public void setMenuController(Controller controller) {
//		menuController = getInputController(controller);
//    }
//    
//	// TODO
//    public AbstractController getInputController(int player, OrthographicCamera guiCam) {
//    	AbstractController returnController = null;
//    	
//		if (player == 1) {
////			setOwner(1, owner);
//			setCamera(1, guiCam);
//			returnController = inputController_PL1;
//		}
//		if (player == 2) {
////			setOwner(2, owner);
//			setCamera(2, guiCam);
//			returnController = inputController_PL2;
//		}
//		if (player == 3) {
////			setOwner(3, owner);
//			setCamera(3, guiCam);
//			returnController = inputController_PL3;
//		}
//		if (player == 4) {
////			setOwner(4, owner);
//			setCamera(4, guiCam);
//			returnController = inputController_PL4;
//		}
//		return returnController;
//    }
//    
//    
//    public AbstractController getInputController(int player) {
//    	AbstractController returnController = null;
//    	
// 		if (player == 1) returnController = inputController_PL1;
// 		if (player == 2) returnController = inputController_PL2;
// 		if (player == 3) returnController = inputController_PL3;
// 		if (player == 4) returnController = inputController_PL4;
// 		
// 		return returnController;
//     }
//
//    //---- get philEngineController by finding the inner controller -----
//    public AbstractController getInputController(Controller controller) {
//    	AbstractController returnController = null;
//    	
//		for (int i = 0; i < controllerList.list.size(); i++) {
//			Controller listController = controllerList.list.get(i).controller;
//			if (listController != null && listController == controller) {
//				returnController = controllerList.list.get(i);
//				break;
//			}
//		}
// 		return returnController;
//     }
//    
//    //---- set Controller as disconnected -----
//    public void setControllerConnectionStatus(Controller controller, boolean isControllerDisconnected) {
//    	    	
//		for (int i = 0; i < controllerList.list.size(); i++) {
//			AbstractController peController = controllerList.list.get(i);
//			if (peController.controller != null && peController.controller == controller) {		// disconnected controller is not null, but reports FALSE boolean
//				peController.setControllerDisconnected(isControllerDisconnected);
//			}
//		}
//     }
//    //---- set Controller as disconnected -----
//    public boolean isControllerDisconnected(Controller controller) {
//    	boolean returnbool = false;
//		for (int i = 0; i < controllerList.list.size(); i++) {
//			AbstractController peController = controllerList.list.get(i);
//			if (peController != null) {		// disconnected controller is not null, but reports FALSE boolean
//				returnbool = peController.isControllerDisconnected();
//			}
//		}
//		return returnbool;
//     }
//    
//    public void setInputController(int player, AbstractController contr) {
//    	
// 		if (player == 1) inputController_PL1 = contr;
// 		if (player == 2) inputController_PL2 = contr;
// 		if (player == 3) inputController_PL3 = contr;
// 		if (player == 4) inputController_PL4 = contr;
// 		
//// 		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements
//     }
//
//    
//    public ControllerConfig2 getControllerConfig(int player) {
//    	ControllerConfig2 returnconfig = null;
//    	
// 		if (player == 1) returnconfig = inputController_PL1.controllerConfig;
// 		if (player == 2) returnconfig = inputController_PL2.controllerConfig;
// 		if (player == 3) returnconfig = inputController_PL3.controllerConfig;
// 		if (player == 4) returnconfig = inputController_PL4.controllerConfig;
// 		
// 		return returnconfig;
//     }
//    
//    
//	public void deleteConfig(int player, ControllerConfig2 controllerConfig) {
//		
//		getControllerConfigList().deleteConfig(controllerConfig);
//
//		ControllerConfig2 newConf = null;
//		
//		if (player == 1) {
//			newConf = getLastUsedConfig(player, inputController_PL1.getDescription());
//			if (newConf == null) newConf = generateAndRegisterNewConfig(inputController_PL1.getDescription(), "new", player, false);
//			setControllerConfig(player, newConf, true);
//		}
//		if (player == 2) {
//			newConf = getLastUsedConfig(player, inputController_PL2.getDescription());
//			if (newConf == null) newConf = generateAndRegisterNewConfig(inputController_PL2.getDescription(), "new", player, false);
//			setControllerConfig(player, newConf, true);
//		}
//		if (player == 3) {
//			newConf = getLastUsedConfig(player, inputController_PL3.getDescription());
//			if (newConf == null) newConf = generateAndRegisterNewConfig(inputController_PL3.getDescription(), "new", player, false);
//			setControllerConfig(player, newConf, true);
//		}
//		if (player == 4) {
//			newConf = getLastUsedConfig(player, inputController_PL4.getDescription());
//			if (newConf == null) newConf = generateAndRegisterNewConfig(inputController_PL4.getDescription(), "new", player, false);
//			setControllerConfig(player, newConf, true);
//		}
//		
////		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements
//	}
//	
//	
//    
//	public ControllerList getControllerList() {
//		return controllerList;
//	}
//	
//	public ControllerConfigList getControllerConfigList() {	
//		return controllerConfigList;
//	}
//	
//	
//  public void switchController(int player, AbstractController contr, OrthographicCamera guiCam, Controller device, ControllerConfig2 config, boolean checkForAutoConfigMode) {
//
// 		if (player == 1) inputController_PL1 = contr;
// 		if (player == 2) inputController_PL2 = contr;
// 		if (player == 3) inputController_PL3 = contr;
// 		if (player == 4) inputController_PL4 = contr;
// 		
// 		setControllerConfig(player, config, checkForAutoConfigMode);
// 		setCamera(player, guiCam);
// 		setControllingDevice(player, device);
//// 		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements
//  }
//	
//  //--- dependency-injection for the onscreen joystick to avoid dependencies between this manager and assets of the project
//  public void setTouchpadForOnScreenJoystick(Touchpad touchpad) {
//	  
//	this.touchpad = touchpad;  
//  }
//  
//  public boolean isOnScreenJoystickAvailable() {
//	  
//	  boolean available = false;
//	  if (touchpad != null) available = true;
//	  
//	  return available;
//  }
//  
//
//  
//	/** enables / disables the on screen joystick for the given controller. Afterwards it checks if jump/fire-buttons have to be drawn and sets correct position depending on the config
//	 * <p>
//	 * <p>used by gamescreen
//	 * <p>
//	 * @param player
//	 * @param contr */
//  public void switchOnScreenStick(int player, AbstractController contr) {
//
//		if (ControllerAndroidOnScreen.equals(contr.getDescription())) {
//		
////			TouchpadStyle touchpadStyle = new TouchpadStyle(new FixedTextureRegionDrawable(Assets.joystickBackground, 140, 140), new FixedTextureRegionDrawable(Assets.joystickKnob, 34, 34)); 
////			contr.touchpad = new Touchpad(20, touchpadStyle);
//			contr.touchpad = touchpad;
//			contr.useTouchpad = true;
//		}
//		else {
//			contr.useTouchpad = false;
//			contr.touchpad = null;
//		}
//		//------------------------------------------------------------------
////		setupJumpFireButtons();	// needed to determine if gamescreen should draw drawJumpFireButtons as GUI elements and where
//  }
//  
//  
//  	//---------------------------------------------------------------------------
//  
//  	// used bei controllerchooserscreen.new(), controllerchooserscreen.checkChoosenController(), ControllerList, ...
//  	// configName = "new" => "New" + AutoID // configName = "default" => wert aus configDefaults wird übernommen // ansonsten wird der uebernommene name verwendet
//	public ControllerConfig2 generateAndRegisterNewConfig(String controllerDescription, String configName, int player, boolean isSystemConfig) {
//				
////		ControllerConfig2 newConfig = new ControllerConfig2(getNewCommandsArray());
////		newConfig.controllerDescription = controllerDescription;
////		newConfig.isSystemConfig = isSystemConfig;
////		
////		//------- Set Defaults to avoid newConfig = -1
////		if (game.getRunningDevice() == Game.DESKTOP) {
////		
////			if (controllerDescription.equals(MgrControllerConfigManager.ControllerDesktop)) {
////				if (player == 1) ConfigDefaults.setDefaultConfigKEYS_PL1(newConfig);
////				if (player == 2) ConfigDefaults.setDefaultConfigKEYS_PL2(newConfig);
////			}
////			if (controllerDescription.equals(OuyaCodes.ID)) ConfigDefaults.setDefaultConfigOUYAatPC(newConfig);
////			//--- TODO: add amazon fire controller here---------------------------------------------------------------------
////			if (controllerDescription.equals(MgrControllerConfigManager.ControllerSaitek)) ConfigDefaults.setDefaultConfigSaitek(newConfig);
////		}
////		if (game.getRunningDevice() == Game.OUYA) {
////			
////			if (controllerDescription.equals(OuyaCodes.ID)) ConfigDefaults.setDefaultConfigOUYA(newConfig);
////			if (controllerDescription.equals(MgrControllerConfigManager.ControllerAmazonFire)) ConfigDefaults.setDefaultConfigAmazonFireAtOUYA(newConfig);
////		}
//		
//		ControllerConfig2 newConfig = getClonedDefaultConfig(controllerDescription);
//		if (newConfig != null) {
//			newConfig.controllerDescription = controllerDescription;
//			newConfig.isSystemConfig = isSystemConfig;
//		
//			//---- jetzt neuen namen vergeben, da sonst ggf. der name der default config vergeben worden ist
//			if (!"default".equals(configName)) {
//	
//				if ("new".equals(configName)) {
//					id++;
//					newConfig.name = "new" + id;
//				}
//				else {
//					newConfig.name = configName;
//				}
//			}
//			//------------------------------------
//			
//			getControllerConfigList().addConfig(newConfig);		
//		}
//
//		return newConfig;
//	}
//  
//	//---------------------------------------------------------------------------
//  
////	public ControllerConfig2 getConfig(int index, String ControllerDescription) {
////				
////		ControllerConfig2 getConfig = new ControllerConfig2();		// TODO: neue config aus array kopieren
////		int controllerCount = getControllerConfigList().list.size();
////		int getCounter = 0;
////		
////		for (int i = 0; i < controllerCount; i++) {
////			
////			getConfig = getControllerConfigList().list.get(i);
////			if (ControllerDescription.equals(getConfig.controllerDescription)) {
////				
////				if (index == getCounter) {
////					
////					break;
////				}
////				else {
////					
////					getCounter ++;
////				}
////			}
////		}
////		return getConfig;
////	}
//	
//	
//
//	/** returns the last used controller or alternatively a defaultcontroller if the last used is not existent in the current controllerlist
//	 * <p>
//	 * <p>
//	 * <p>
//	 * @param searchController this is the wanted controller
//	 * @param alternativController put the name of the default controller here (should be always in the list)
//	 * @param deviceNumber if there are multiple devices with the same name/description then this is the number of the returned device  */
//	public AbstractController getController(String searchController, String alternativController, int deviceNumber) {
//		
//		AbstractController returnContr = null;
//		
//		//------ zuerst nach dem gewuenschten controller suchen
//		if (!"".equals(searchController)) returnContr = getController(searchController, deviceNumber);
//		
//		//--- falls ergebnislos, dann nach dem alternativcontroller suchen
//		if (returnContr == null) returnContr = getController(alternativController, deviceNumber);
//		
//		return returnContr;
//		
//	}
//	
//	/** returns the a controller from the current controllerlist by the given name
//	 * <p>
//	 * <p>
//	 * <p>
//	 * @param contr this is the wanted controllername
//	 * @param deviceNumber if there are multiple devices with the same name/description then this is the number of the returned device  */
//	public AbstractController getController(String contr, int deviceNumber) {
//
//		int deviceCounter = 0;
//		AbstractController returnContr = null;
//		AbstractController tempContr;
//		int size = controllerList.list.size();
//
//		for (int i = 0; i < size; i++) {
//			
//			tempContr = controllerList.list.get(i);
//			if (tempContr.getDescription().equals(contr)) {
//				
//				deviceCounter ++;
//				
//				if (deviceCounter == deviceNumber) {
//					
//					returnContr = tempContr; 
//					break; 
//				}
//			}
//		}	
//		return returnContr;
//	}
//	
//
//	
//	public int countControllersWithSameDescription(String description) {
//		
//		AbstractController returnContr = null;
//		int size = controllerList.list.size();
//		int i;
//		int counter = 0;
//		
//		for (i = 0; i < size; i++) {
//			
//			returnContr = controllerList.list.get(i);
//			if (returnContr.getDescription().equals(description)) {
//				
//				counter++; 
//			}
//		}
//		return counter;
//	}
//	
//	
//	
//	
//	public int findControllerIndexByControllerID(AbstractController gameController) {
//		
//		AbstractController returnContr = null;
//		int size = controllerList.list.size();
//		int i;
//		
//		for (i = 0; i < size; i++) {
//			
//			returnContr = controllerList.list.get(i);
//			if (returnContr.getDescription().equals(gameController.getDescription())) {
//				
//				if (returnContr.controller == gameController.controller) break; 
//			}
//		}
//		return i;
//	}
//  
//  
//  
//  public boolean isUsingOnScreenStick(int player) {
//	  boolean returnBoolean = false;
//	  
//		if (player == 1) returnBoolean = inputController_PL1.useTouchpad;
//		if (player == 2) returnBoolean = inputController_PL2.useTouchpad;
//		if (player == 3) returnBoolean = inputController_PL3.useTouchpad;
//		if (player == 4) returnBoolean = inputController_PL4.useTouchpad;
//		
//		return returnBoolean;
//  } 
//  
////--------------------------------------------------------
//  
//  
//  
//	public void saveConfigsAsDefault() {
//		
////		if (inputController_PL1.controllerConfig.lastUsedPL1 == false) {
////			
////			controllerConfigList.resetLastUsedConfig(1, inputController_PL1.getDescription());	// if player is set (last used by player) then reset all lastUsed
////			inputController_PL1.controllerConfig.lastUsedPL1 = true;
////
////		}
////		if (inputController_PL2.controllerConfig.lastUsedPL2 == false) {
////			
////			controllerConfigList.resetLastUsedConfig(2, inputController_PL2.getDescription());	// if player is set (last used by player) then reset all lastUsed
////			inputController_PL2.controllerConfig.lastUsedPL2 = true;
////		}
//		
//		game.getPreferences().setLastUsedController(1, inputController_PL1.getDescription());
//		game.getPreferences().setLastUsedController(2, inputController_PL2.getDescription());
//		game.getPreferences().setLastUsedController(3, inputController_PL3.getDescription());
//		game.getPreferences().setLastUsedController(4, inputController_PL4.getDescription());
//		
//		game.getPreferences().setLastUsedConfigForController(1, inputController_PL1.getDescription(), inputController_PL1.controllerConfig.name);
//		game.getPreferences().setLastUsedConfigForController(2, inputController_PL2.getDescription(), inputController_PL2.controllerConfig.name);
//		game.getPreferences().setLastUsedConfigForController(3, inputController_PL3.getDescription(), inputController_PL3.controllerConfig.name);
//		game.getPreferences().setLastUsedConfigForController(4, inputController_PL4.getDescription(), inputController_PL4.controllerConfig.name);
//	}
//		
//  
//  
//  //---------------------------------------------------
//  
//  
//  	private ControllerConfigList getUserconfigs() {
//		
//  		ControllerConfigList list = new ControllerConfigList();
//  		
//		for (int i = 0; i < controllerConfigList.size(); i++) {
//			
//			if (controllerConfigList.list.get(i).isSystemConfig == false) {
//				
//				list.addConfig(controllerConfigList.list.get(i));
//			}
//		}
//  		
//  		return list; 		
//  	}
//  
// 
//	public void saveConfigsToJson()  {
//    	
//    	if (FileSystem.isDirAvailable(CONFIG_DIR, game.getDebugMode())) {
//			
//    		String path = CONFIG_DIR + "/" + "configs";
//			profileDataFile = Gdx.files.local( path );
//	      
//			Json json = new Json();
////			json.setUsePrototypes(false); 			// force to save all data!
//			
//			// convert the given gameState to text
////			String profileAsText = json.toJson( controllerConfigList, ControllerConfigList.class );
//			
//			String profileAsText = json.prettyPrint( getUserconfigs() );
//	      
//			// encode the text
////	    	String profileAsCode = Base64Coder.encodeString( profileAsText );	// "verschlüsseln" damit nicht jeder dummy editieren kann
//	  
//	    	// write the profile data file
//			profileDataFile.writeString( profileAsText, false );
////			Gdx.app.log("ControllerConfigs", "Configs have been written");
//    	}
//    }
//    
//	
//    //----- load GameState from JSON or return a blank GameState ----------------------
//    private ControllerConfigList loadControllerConfigList()  {
//               	
//        if (FileSystem.isDirAvailable(CONFIG_DIR, game.getDebugMode())) {
//        	
//        	if (isConfigAvailable(false)) {
//        		
//        		return loadConfigfromJson(game.getDebugMode());
//        	}
//        	else return new ControllerConfigList(); 
//        }
//        else return new ControllerConfigList();
//    }
//    
//   
//	private ControllerConfigList loadConfigfromJson(boolean log)  {
//	        
//		ControllerConfigList list = new ControllerConfigList();
//		
//		// create the handle for the profile data file
//		String path = CONFIG_DIR + "/" + "configs";
//		profileDataFile = Gdx.files.local( path );
//	
//	    // create the JSON utility object
//	    Json json = new Json();
//	
//	    // check if the profile data file exists
//	    if( profileDataFile.exists() ) {
//	
//	        // load the profile from the data file
//	        try {
//	
//	            // read the file as text
//	            String profileAsCode = profileDataFile.readString();
//	
//	            // decode the contents
//	//            String profileAsText = Base64Coder.decodeString( profileAsCode );	// "entschlüsseln" damit nicht jeder dummy editieren kann
//	
//	            // restore the state
//	//            json.setElementType(GameStateList.class, "list", GameState.class);
//	            list = json.fromJson(ControllerConfigList.class, profileAsCode );
//	            
//	            
//	            if (log) Gdx.app.log("configs", "configs have been successfully read!");
//	            
//	        } catch( Exception e ) {
//	
//	            // log the exception
//	        	if (log) Gdx.app.log("ERROR", "Unable to parse existing profile data file", e );
//	
//	            // recover by creating a fresh new profile data file;
//	            // note that the player will lose all game progress
//	//            profile = new Profile();
//	//            persist( profile );
//	
//	        }
//	
//	    } else {
//	    	if (log) Gdx.app.log("configs", "File doesnt exist!");
//	    	// create a new profile data file
//	//        profile = new Profile();
//	//        persist( profile );
//	    }
//	
//	    return list;
//	}
//	
//	
//	
//	private boolean isConfigAvailable(boolean log) {
//		
//		boolean isConfigAvailable = false;
//		
//		// create the handle for the profile data file
//		String path = CONFIG_DIR + "/" + "configs";
//		profileDataFile = Gdx.files.local( path );
//	
//	    // check if the profile data file exists
//	    if( profileDataFile.exists() ) {
//	    
//	    	if (log) Gdx.app.log("configs", "file exists");
//	    	
//	    	if( profileDataFile.isDirectory() == false ) {
//	    		
//	    		isConfigAvailable = true;
//	    		if (log) Gdx.app.log("configs", "config exists");
//	    	}
//	    	else {
//	    		if (log) Gdx.app.log("configs", "config is a DIR!");
//	    	}
//	    }
//	    else {
//	    	if (log) Gdx.app.log("configs", "file does NOT exist!");
//	    }
//	
//		return isConfigAvailable;
//	}
//
////---------------------------------------------------------------------
//	
//	
////	//---- check if choosen config has no mappings.... if so => start config-screen in auto-mapper mode
////	public void checkForAutoConfigMode(int player) {
////		
//////		if (!controllerConfig.controllerDescription.equals(ControllerAndroidOnScreen) &&
//////				!controllerConfig.controllerDescription.equals(ControllerAndroid) &&
//////				!controllerConfig.controllerDescription.equals(ControllerNONE)) {
////		if (!ControllerAndroid.equals(getInputController(player).getDescription()) &&
////			!ControllerAndroidOnScreen.equals(getInputController(player).getDescription()) &&
////			!ControllerNONE.equals(getInputController(player).getDescription())) {
////		
////			if (getControllerConfig(player).enterMenu == UNCONFIGURED ||
////					getControllerConfig(player).quit == UNCONFIGURED || 
////					getControllerConfig(player).menuPrevious == UNCONFIGURED ||
////					getControllerConfig(player).menuNext == UNCONFIGURED) {
////				
////				//---- set all to unconfigured for stability
////				getControllerConfig(player).enterMenu = UNCONFIGURED;
////				getControllerConfig(player).quit = UNCONFIGURED;
////				getControllerConfig(player).menuPrevious = UNCONFIGURED;
////				getControllerConfig(player).menuNext = UNCONFIGURED;
////					
////			
//////				// ---- wenn config.entermenu == unconfigured && config == systemConfig => dann wird eine newConfig erzeugt (sonst wäre default alles -1 aber wegen sysConfig würde sie nicht gespeichert werden!) 
//////				if (getControllerConfig(player).isSystemConfig == true) {
//////				
//////					getControllerConfig(player).isSystemConfig = false;
////////					getControllerConfig(player).name = "userConfig";
//////					id++;
//////					getControllerConfig(player).name = "new" + id;
//////				}
////				
////				save_all();	// save weil der bildschirm verlassen wird
////				
////				
//////				game.setScene2DScreen(new ControllerConfigScreen(game, true, "ControllerConfig", player, 0));	// TODO: ggf. wieder aktivieren
////				
////				
////			}
////		}
////	}
//	//-----------------------------------------------------------------------
//
//	
////	//------ change location of jump/firebuttons and clickregions 
////    public void setupJumpFireButtons() {
////		
////    	AbstractController checkController = null;
////    	ControllerConfig checkConfig = null;
////    	
////    	checkIfSomeOneIsUsingTouchscreen();
////    	if (isSomeOneUsingTouchscreen > 0) {
////		
////    		if (isSomeOneUsingTouchscreen == 1) {
////    			checkController = inputController_PL1;
////    			checkConfig = inputController_PL1.controllerConfig;
////    		}
////       		if (isSomeOneUsingTouchscreen == 2) {
////    			checkController = inputController_PL2;
////    			checkConfig = inputController_PL2.controllerConfig;
////    		}
////       		
////       		if (checkController != null) {
////		    	
////       			if (ControllerAndroid.equals(checkController.getDescription())) {
////					
////					if (ConfigAndroidFireRight.equals(checkConfig.name)) {
////						
////						jumpButtonLocation 		= new Vector2(9, 110);
////						jumpButtonClickRegion 	= new Rectangle(0, 70, 400, 360);	// Y: 0 ist oben / 480 ist unten !!!
////			    		fireButtonLocation 		= new Vector2(696, 110);
////			    		fireButtonClickRegion 	= new Rectangle(400, 70, 400, 410);	// Y: 0 ist oben / 480 ist unten !!!
////					}
////					if (ConfigAndroidFireLeft.equals(checkConfig.name)) {
////						
////			    		fireButtonLocation 		= new Vector2(9, 110);
////			    		fireButtonClickRegion 	= new Rectangle(0, 70, 400, 360);	// Y: 0 ist oben / 480 ist unten !!!
////			    		jumpButtonLocation 		= new Vector2(696, 110);
////			    		jumpButtonClickRegion 	= new Rectangle(400, 70, 400, 410);	// Y: 0 ist oben / 480 ist unten !!!
////					}
////				}
////				if (ControllerAndroidOnScreen.equals(checkController.getDescription())) {
////					
////					if (ConfigAndroidFireRight.equals(checkConfig.name)) {
////						
////			    		jumpButtonLocation 		= new Vector2(696, 250);
////			    		jumpButtonClickRegion 	= new Rectangle(400, 70, 400, 200);
////			    		fireButtonLocation 		= new Vector2(696, 110);
////			    		fireButtonClickRegion 	= new Rectangle(400, 270, 400, 210);
////					}
////					if (ConfigAndroidFireLeft.equals(checkConfig.name)) {
////						
////			    		jumpButtonLocation 		= new Vector2(9, 250);
////			    		jumpButtonClickRegion 	= new Rectangle(0, 70, 400, 200);
////			    		fireButtonLocation 		= new Vector2(9, 110);
////			    		fireButtonClickRegion 	= new Rectangle(0, 270, 400, 160);
////					}
////				}
////       		}
////    	}
////    	else { // no touchscreen user -> set variables for desktop clickregions ( fire left)
////    		fireButtonLocation 		= new Vector2(9, 200);
////    		fireButtonClickRegion 	= new Rectangle(0, 70, 400, 360);	// Y: 0 ist oben / 480 ist unten !!!
////    		jumpButtonLocation 		= new Vector2(696, 200);
////    		jumpButtonClickRegion 	= new Rectangle(400, 70, 400, 410);	// Y: 0 ist oben / 480 ist unten !!!
////    	}
////    }
//    
//    //---------------------------------------------------
//	
//	void checkIfSomeOneIsUsingTouchscreen() {
//		
//		isSomeOneUsingTouchscreen = 0;
//		
//		if (inputController_PL1 != null && 
//				(inputController_PL1.getDescription().equals(MgrControllerConfigManager.ControllerAndroid) || 
//				inputController_PL1.getDescription().equals(MgrControllerConfigManager.ControllerAndroidOnScreen))) {
//			
//			isSomeOneUsingTouchscreen = 1;
//		}
//		if (inputController_PL2 != null && 
//				(inputController_PL2.getDescription().equals(MgrControllerConfigManager.ControllerAndroid) ||
//				inputController_PL2.getDescription().equals(MgrControllerConfigManager.ControllerAndroidOnScreen))) {
//			
//			isSomeOneUsingTouchscreen = 2;	
//		}
//	}
//
//	public int isSomeOneUsingTouchscreen(boolean update) {
//		
//		if (update == true) checkIfSomeOneIsUsingTouchscreen();
//				
//		return isSomeOneUsingTouchscreen;
//	}
//	
//	//---------------------------------------------
//	
//	public void save_all() {
//		
//		saveConfigsAsDefault();
//		saveConfigsToJson();
//	}
//	
//	/**
//	 * copie the injected arraz of game commands to use the copz for an new controller config
//	 */
//	private Command[] getNewCommandsArray() {
//		Command[] array2 = new Command[arrayCommands.length];
//		System.arraycopy(arrayCommands, 0, array2, 0, arrayCommands.length);
//		return array2;
//	}
//	
//	private ControllerConfig2 getClonedDefaultConfig(String controllerDescription) {
//		if (arrayDefaultControllerConfigs.containsKey(controllerDescription)) {
//			ControllerConfig2 newconf = arrayDefaultControllerConfigs.get(controllerDescription).copyWithoutReferences();
//			return newconf;
//		}
//		return null;
//	}
//	
//	public Command[] getArrayCommands() {
//		return arrayCommands;
//	}
//}