package de.philweb.philengine.controllers;


public class OuyaCodes {
	
	public static final String ID = "OUYA Game Controller";
	public static final int BUTTON_O = 96;
	public static final int BUTTON_U = 99;
	public static final int BUTTON_Y = 100;
	public static final int BUTTON_A = 97;
	public static final int BUTTON_MENU = 82;
	public static final int BUTTON_DPAD_UP = 19;
	public static final int BUTTON_DPAD_DOWN = 20;
	public static final int BUTTON_DPAD_RIGHT = 22;
	public static final int BUTTON_DPAD_LEFT = 21;
	public static final int BUTTON_L1 = 104;
	public static final int BUTTON_L2 = 102;
	public static final int BUTTON_L3 = 106;
	public static final int BUTTON_R1 = 105;
	public static final int BUTTON_R2 = 103;
	public static final int BUTTON_R3 = 107;
	public static final int AXIS_LEFT_X = 0;
	public static final int AXIS_LEFT_Y = 1;
	public static final int AXIS_LEFT_TRIGGER = 2;
	public static final int AXIS_RIGHT_X = 3;
	public static final int AXIS_RIGHT_Y = 4;
	public static final int AXIS_RIGHT_TRIGGER = 5;
	
	
	
	public static String getButtonToIndex(int index) {
		
		String buttonString = "";
		
		switch (index) {
		
		case -1:
			buttonString = "unconfigured";	// = -1
			break;

		case 0:
			buttonString = "left joystick left/right";	// = 0;
			break;
		case 1:
			buttonString = "left joystick up/down";	// AXIS_LEFT_Y = 1;
			break;
		case 2:
			buttonString = "left trigger (axis)";	// AXIS_LEFT_TRIGGER = 2;
			break;
		case 3:
			buttonString = "right joystick left/right";	// AXIS_RIGHT_X = 3;
			break;
		case 4:
			buttonString = "right joystick up/down";	// AXIS_RIGHT_Y = 4;
			break;
		case 5:
			buttonString = "right trigger (axis)";	// AXIS_RIGHT_TRIGGER = 5;
			break;
		case 19:
			buttonString = "DPAD up";	// BUTTON_DPAD_UP = 19;
			break;
		case 20:
			buttonString = "DPAD down";	// BUTTON_DPAD_DOWN = 20;
			break;
		case 21:
			buttonString = "DPAD left";	// BUTTON_DPAD_LEFT = 21;
			break;
		case 22:
			buttonString = "DPAD right";	// BUTTON_DPAD_RIGHT = 22;
			break;
		case 82:
			buttonString = "Menu button (u)";	
			break;		
		case 96:
			buttonString = "button O";	// BUTTON_O = 96;
			break;
		case 97:
			buttonString = "button A";	// BUTTON_A = 97;
			break;
		case 99:
			buttonString = "button U";	// BUTTON_U = 99;
			break;
		case 100:
			buttonString = "button Y";	// BUTTON_Y = 100;
			break;
		case 102:
			buttonString = "left button 2 (L2)";	// BUTTON_L2 = 102;
			break;
		case 103:
			buttonString = "right button 2 (R2)";	// BUTTON_R2 = 103;
			break;
		case 104:
			buttonString = "left button 1 (L1)";	// BUTTON_L1 = 104;
			break;
		case 105:
			buttonString = "right button 1 (R1)";	// BUTTON_R1 = 105;
			break;
		case 106:
			buttonString = "left stick button (L3)";	// BUTTON_L3 = 106;
			break;
		case 107:
			buttonString = "right stick button (R3)";	// BUTTON_R3 = 107;
			break;		
		}
		
		return buttonString;
	}
	
}