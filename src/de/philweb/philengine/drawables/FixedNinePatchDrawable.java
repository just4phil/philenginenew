package de.philweb.philengine.drawables;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;

public class FixedNinePatchDrawable extends BaseDrawable {

	boolean dropShadow = false;
	int offsetX;
	int offsetY;
	float alpha;
	FixedTextureRegionDrawable TopLeft;
	FixedTextureRegionDrawable MiddleLeft;
	FixedTextureRegionDrawable BottomLeft;
	FixedTextureRegionDrawable TopMiddle;
	FixedTextureRegionDrawable MiddleMiddle;
	FixedTextureRegionDrawable BottomMiddle;
	FixedTextureRegionDrawable TopRight;
	FixedTextureRegionDrawable MiddleRight;
	FixedTextureRegionDrawable BottomRight;
	FixedTextureRegionDrawable Nieten = null;
	boolean nietenUnten = true;
	int anzNieten = 2;
	
	public FixedNinePatchDrawable (
			FixedTextureRegionDrawable TopLeft,
			FixedTextureRegionDrawable MiddleLeft,
			FixedTextureRegionDrawable BottomLeft,
			FixedTextureRegionDrawable TopMiddle,
			FixedTextureRegionDrawable MiddleMiddle,
			FixedTextureRegionDrawable BottomMiddle,
			FixedTextureRegionDrawable TopRight,
			FixedTextureRegionDrawable MiddleRight,
			FixedTextureRegionDrawable BottomRight) {
		
		this.TopLeft = TopLeft;
		this.MiddleLeft = MiddleLeft;
		this.BottomLeft = BottomLeft;
		this.TopMiddle = TopMiddle;
		this.MiddleMiddle = MiddleMiddle;
		this.BottomMiddle = BottomMiddle;
		this.TopRight = TopRight;
		this.MiddleRight = MiddleRight;
		this.BottomRight = BottomRight;
	}
	
	public FixedNinePatchDrawable (
			FixedTextureRegionDrawable TopLeft,
			FixedTextureRegionDrawable MiddleLeft,
			FixedTextureRegionDrawable BottomLeft,
			FixedTextureRegionDrawable TopMiddle,
			FixedTextureRegionDrawable MiddleMiddle,
			FixedTextureRegionDrawable BottomMiddle,
			FixedTextureRegionDrawable TopRight,
			FixedTextureRegionDrawable MiddleRight,
			FixedTextureRegionDrawable BottomRight,
			FixedTextureRegionDrawable Nieten, int anzNieten, boolean nietenUnten) {
		
		this.TopLeft = TopLeft;
		this.MiddleLeft = MiddleLeft;
		this.BottomLeft = BottomLeft;
		this.TopMiddle = TopMiddle;
		this.MiddleMiddle = MiddleMiddle;
		this.BottomMiddle = BottomMiddle;
		this.TopRight = TopRight;
		this.MiddleRight = MiddleRight;
		this.BottomRight = BottomRight;
		this.Nieten = Nieten;
		this.nietenUnten = nietenUnten;
		this.anzNieten = anzNieten;
	}

	public void draw (Batch batch, float x, float y, float width, float height) {
		
		//---- draw shadow --------
		if (dropShadow == true) {

			batch.setColor(0f, 0f, 0f, alpha);
			
			drawInstance(batch, x + offsetX, y + offsetY, width, height);
			
			batch.setColor(1f, 1f, 1f, 1f);
		}
		
		//---- draw ninepatch --------
		drawInstance(batch, x, y, width, height);
	}
	
	public void drawInstance (Batch batch, float x, float y, float width, float height) {
			
		TopLeft.draw(batch, x, y + height - TopLeft.getH(), TopLeft.getW(), TopLeft.getH());
		MiddleLeft.draw(batch, x, y + BottomLeft.getH(), MiddleLeft.getW(), height - TopLeft.getH() - BottomLeft.getH());
		BottomLeft.draw(batch, x, y, BottomLeft.getW(), BottomLeft.getH());
		TopMiddle.draw(batch, x + TopLeft.getW(), y + height - TopMiddle.getH(), width - TopLeft.getW() - TopRight.getW(), TopMiddle.getH());
		if (MiddleMiddle != null) {	// mitte kann ggf. komplett transparent sein
			MiddleMiddle.draw(batch, x + TopLeft.getW(), y + BottomMiddle.getH(), width - TopLeft.getW() - TopRight.getW(), height - BottomMiddle.getH() - TopMiddle.getH());
		}
		BottomMiddle.draw(batch, x + BottomLeft.getW(), y, width - BottomLeft.getW() - BottomRight.getW(), BottomMiddle.getH());
		TopRight.draw(batch, x + width - TopRight.getW(), y + height - TopRight.getH(), TopRight.getW(), TopRight.getH());
		MiddleRight.draw(batch, x + width - MiddleRight.getW(), y + BottomRight.getH(), MiddleRight.getW(), height - TopRight.getH() - BottomRight.getH());
		BottomRight.draw(batch, x + width - BottomRight.getW(), y, BottomRight.getW(), BottomRight.getH());
		
		if (Nieten != null) {
			if (nietenUnten == true) {
				if (anzNieten == 1) {
					// TODO
				}
				if (anzNieten == 2) {
					Nieten.draw(batch, x + 30f, y - 10f);
					Nieten.draw(batch, x + width - Nieten.getW() - 30f, y - 10f);
				}
			}
			else {
				if (anzNieten == 1) {
					// TODO: rotate
					Nieten.draw(batch, (x + width) * 0.5f - (Nieten.getW() * 0.5f) + 5, y + height - 15f);
				}
				if (anzNieten == 2) {
					Nieten.draw(batch, (x + width) - Nieten.getW() - 30f, y + height - 15f);
					Nieten.draw(batch, x + 30f, y + height - 15f);
				}
			}
		}
	}
	
	public void enableShadow(int offsetX, int offsetY, float alpha) {
		
		this.dropShadow = true;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.alpha = alpha;
	}
	
	public void disableShadow() {
		
		this.dropShadow = false;
	}
}