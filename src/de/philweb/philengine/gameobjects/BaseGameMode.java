package de.philweb.philengine.gameobjects;

import de.philweb.philengine.common.BaseGameRenderer;
import de.philweb.philengine.common.Game;
import de.philweb.philengine.listeners.PeDialogEvent;
import de.philweb.philengine.maps.MapService;

public abstract class BaseGameMode {
	
	public Game game;   
	protected MapService map;
	protected BaseGameRenderer baseGameRenderer;
	protected BaseGameWorld baseGameWorld;
	public boolean allControllersAreAssigned;
	
	public BaseGameMode (Game game, MapService map) {
		this.game = game;
		this.map = map;
		this.baseGameWorld = map.getGameWorld();
		this.baseGameRenderer = map.getGameRenderer();
	}
	 
	public abstract void setupGameMode();
	public abstract void configurePlayerObjects();
	public abstract void configureEnemyObjects();
	public abstract boolean checkGameOver();
	public abstract String getGameModeName();
	
	public void checkAssignedControllers() {
		// check menucontroller, too
		if (game.getControllerConfigManager().getMenuController() == null) allControllersAreAssigned = false;
		if (allControllersAreAssigned == false) {

			// game.getPlatformResolver().showToast("PeDialogEvent unassignedControllersEvent");
			PeDialogEvent unassignedControllersEvent = new PeDialogEvent();
			unassignedControllersEvent.setType(PeDialogEvent.Type.unassignedControllers);
			game.notify(unassignedControllersEvent);
		}
	}
    
	public BaseGameWorld getGameWorld() {
    	return baseGameWorld;
    }
    
	public BaseGameRenderer getGameRenderer() {
		return baseGameRenderer;
	}
}




	
