package de.philweb.philengine.gameobjects;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.maps.MapService;

public class BaseGameModeSTUB extends BaseGameMode {

	public BaseGameModeSTUB(Game game, MapService map) {
		super(game, map);
	}

	@Override
	public void setupGameMode() {}

	@Override
	public void configurePlayerObjects() {}

	@Override
	public void configureEnemyObjects() {}

	@Override
	public boolean checkGameOver() { return false;	}

	@Override
	public String getGameModeName() {
		return "stub";
	}
}
