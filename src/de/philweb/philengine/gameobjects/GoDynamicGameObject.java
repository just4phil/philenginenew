package de.philweb.philengine.gameobjects;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.MassData;

import de.philweb.philengine.controllers.AbstractController;
import de.philweb.philengine.controllers.ControllerSTUB;
import de.philweb.philengine.maps.MapService;
import de.philweb.philengine.particles2d.IparticlePosition;


public class GoDynamicGameObject extends GoGameObject implements IparticlePosition {
	
	protected AbstractController controller = null;
	protected Vector2 position_previous;
	public float angle;
	protected float angle_previous;
	protected MassData masse = new MassData();
	protected float vel_y;
	protected float vel_x;	
	public boolean isInInterpolationArray = false;
	protected boolean check_MAX_VELOCITY_X = false;
	protected boolean check_MAX_VELOCITY_Y = false;
	protected float maxVELOCITY_X = 0f;
	protected float maxVELOCITY_Y = 0f;
	
	public GoDynamicGameObject(MapService map, int objectID, float width_m, float height_m, float x, float y, float angleInDegrees) {
		
		super(map, objectID, width_m, height_m, x, y);
	
		controller = new ControllerSTUB(game);	// just to avoid nullpointer
		position_previous = new Vector2();
		angle = MathUtils.degreesToRadians * angleInDegrees;		
		angle_previous = angle;
	}
	
	public void update (float deltaTime) {
		if (body != null) {
			angle = body.getAngle();		// not neccessary due to interpolation ... only testing difference to fixed timestep
			
			//--- angle correction to fix angle to max 0 - 2*PI --------
			if (angle > MathUtils.PI2) {
				angle = angle - MathUtils.PI2;
				angle_previous = angle;
				body.setTransform(body.getPosition().x, body.getPosition().y, angle);	// update box2d
			}
			else if (angle < 0) {
				angle = MathUtils.PI2 + angle;
				angle_previous = angle;
				body.setTransform(body.getPosition().x, body.getPosition().y, angle);	// update box2d
			}
			
			//--- update actual position with box2d-data -----
			if (!isInInterpolationArray) {	// if not interpolated then copy data here, else it will be done in interpolation
				position_m.x 	= body.getPosition().x;	// not neccessary due to interpolation ... only testing difference to fixed timestep 
				position_m.y 	= body.getPosition().y;	// not neccessary due to interpolation ... only testing difference to fixed timestep  
			}
			vel_x 			= body.getLinearVelocity().x;
			vel_y 			= body.getLinearVelocity().y;
			
	        // cap max velocity
			if (check_MAX_VELOCITY_X) {
				if (vel_x < maxVELOCITY_X) body.setLinearVelocity(maxVELOCITY_X, body.getLinearVelocity().y);
				vel_x = maxVELOCITY_X;
			}
			if (check_MAX_VELOCITY_Y) {
				if (vel_y < maxVELOCITY_Y) body.setLinearVelocity(body.getLinearVelocity().x, maxVELOCITY_Y);
				vel_y = maxVELOCITY_Y;
			}
			
			//-- verhindern dass sich das object zu schnell dreht --------- 
//			if (maxAngularVelocity > -1) {
//				if (body.getAngularVelocity() < -maxAngularVelocity) body.setAngularVelocity(-maxAngularVelocity);
//				if (body.getAngularVelocity() > maxAngularVelocity) body.setAngularVelocity(maxAngularVelocity);
//			}
		}
	}
	
	//--- wenn z.b. ein body durch eine decke geht und einen "sprung" macht dann updaten damit keine positions-artefakte aufgrund der interpolation entstehen
	public void updatePositions() {
		if (body != null) {
			//--- position des objekts inkl. interpolationsposition ebenfalls angleichen, da sonst artefakte der interpolation auftreten
			position_previous.x = body.getPosition().x;
			position_previous.y = body.getPosition().y;
			position_m.x 		= position_previous.x;
			position_m.y 		= position_previous.y;
		}
	}
	
	public void updateAngle() {
		if (body != null) {
			angle 				= body.getAngle();
			angle_previous		= angle;
		}
	}
	
	public void setMaxVelocityX(float maxVelX) {
		this.maxVELOCITY_X = maxVelX;
		this.check_MAX_VELOCITY_X = true;
	}
	public void setMaxVelocityY(float maxVelY) {
		this.maxVELOCITY_Y = maxVelY;
		this.check_MAX_VELOCITY_Y = true;
	}
	public void clearMaxVelocity_X() {
		this.check_MAX_VELOCITY_X = false;
	}
	public void clearMaxVelocity_Y() {
		this.check_MAX_VELOCITY_Y = false;
	}
	
	public float getAngleInDegress() {
		return (MathUtils.radiansToDegrees * angle);
	}
	
	public float getAnglePreviousInDegress() {
		return (MathUtils.radiansToDegrees * angle_previous);
	}
	
	//-------------------------------------------
	
	public Vector2 getLocalVelocity () {
	    /*returns get velocity vector relative to car
	    */
	    return body.getLocalVector(body.getLinearVelocityFromLocalPoint(body.getPosition()));
	};

	public Vector2 getDirectionVector () {
	    /*
	    returns a world unit vector pointing in the direction this wheel is moving
	    */
		Vector2 directionVector;
		if (getLocalVelocity().y > 0)
			directionVector = new Vector2(0,1);
		else
			directionVector = new Vector2(0,-1);
			
		return directionVector.rotate((float) MathUtils.radiansToDegrees * body.getAngle());	    
	};

	public Vector2 getKillVelocityVector (){
	    /*
	    substracts sideways velocity from this wheel's velocity vector and returns the remaining front-facing velocity vector
	    */
	    Vector2 velocity = body.getLinearVelocity();
	    Vector2 sidewaysAxis = getDirectionVector();
	    float dotprod = velocity.dot(sidewaysAxis);
	    return new Vector2(sidewaysAxis.x*dotprod, sidewaysAxis.y*dotprod);
	};

	public void killSidewaysVelocity (){
	    /*
	    removes all sideways velocity from this wheels velocity
	    */
	    body.setLinearVelocity(getKillVelocityVector());
	};
	
	public void setController(AbstractController controller) {
		this.controller = controller;
//		this.controller.setGameObject(this); 		// sets the owner gameobject in the controller instance
	}
	public AbstractController getController() {
		return controller;
	}

//	@Override
//	public boolean stopParticleEffect() {
//		return false;
//	}
}
