package de.philweb.philengine.gameobjects;


public interface IpowerUp {

	public void apply(GoGameObject go);
}
