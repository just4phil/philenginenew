package de.philweb.philengine.maps;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;

import de.philweb.philengine.common.BaseGameRenderer;
import de.philweb.philengine.common.Game;
import de.philweb.philengine.gameobjects.BaseGameWorld;
import de.philweb.philengine.gameobjects.MgrGameObjectManager;
import de.philweb.philengine.particles2d.MgrParticleFxManager;
import de.philweb.philengine.screensmenus.AbstractScreen;

public class MapService implements Disposable {
				
	public Game game;
	protected AbstractScreen gameScreen;
	private boolean calculateFPS = false; // TODO // FIXME -> make this available as service!
	
	//======= GameWorld and GameRenderer =====
	protected BaseGameWorld gameWorld;
	protected BaseGameRenderer gameRenderer;
	
	//======= Particle Manager ============
	protected MgrParticleFxManager _particleFxManager;
	protected boolean isParticlesEnabled;	//----- buffer preferences ------
	
	//======= GameObjectManager ============
	protected MgrGameObjectManager _gameObjectManager;
	
	//======= box2d ========================
	protected World box2dworld = null;
	private int VELOCITY_ITERATIONS = 10;
	private int POSITION_ITERATIONS = 8;
//	protected ContactListener box2dContactListener = null; // nicht noetig
	
	//======= TiledMapManager ==============
	MgrTiledMapManager _tiledMapManager;
	
	//======= Sound FX =====================
//	protected SoundListener soundListener;
	
	//====== for fixed-timestep box2d simulation =========
	private float fixedTimeStep = 0.0133f;
	private float accumulator;
	private final static int logic_FPSupdateIntervall = 1;  //--- display FPS alle x sekunden
	private long logic_lastRender;
	private long logic_now;
	public int logic_frameCount = 0;
	public int logic_lastFPS = 0;
	
	public MapService(Game game, AbstractScreen gameScreen) {
		this.game = game;
		this.gameScreen = gameScreen;
	} 

	// actually optional 
    public MapService initialize(float fixedTimeStep, boolean calculateFPS) {
		this.fixedTimeStep = fixedTimeStep;
		this.calculateFPS = calculateFPS;
    	return this;
    }
    
	public MgrParticleFxManager getParticleFxManager() {
		if (_particleFxManager == null) {
			_particleFxManager = game.getParticleFxManager();
			isParticlesEnabled = game.getPreferences().isParticlesEnabled();
			_particleFxManager.setParticlesEnabled(isParticlesEnabled);
		}
		return _particleFxManager;
	}
	
	public MgrGameObjectManager getGameObjectManager() {
		if (_gameObjectManager == null) _gameObjectManager = new MgrGameObjectManager();
		return _gameObjectManager;
	}
	
    public MapService addBox2D(float gravityX, float gravityY, boolean doSleep, ContactListener box2dContactListener) {
    	return addBox2D(gravityX, gravityY, doSleep, VELOCITY_ITERATIONS, POSITION_ITERATIONS, box2dContactListener);
    }
    public MapService addBox2D(float gravityX, float gravityY, boolean doSleep, int VELOCITY_ITERATIONS, int POSITION_ITERATIONS, ContactListener box2dContactListener) {
    	if (box2dworld == null) {
    		box2dworld = new World(new Vector2(gravityX, gravityY), doSleep);
    		box2dworld.setContactListener(box2dContactListener);
    	}
    	return this;
    }
    public World getBox2DWorld() {
    	return box2dworld;
    }
    public MapService setBox2DContactListener(ContactListener box2dContactListener) {
    	if (box2dworld != null) box2dworld.setContactListener(box2dContactListener);
    	return this;
    }	
        
	//====== for fixed-timestep box2d simulation =========
//	dt = 0.0133f;	// = 75 FPS logic
//	dt = 0.01f;	// = 100 FPS logic
//	dt = 0.03f;	// = 35 FPS logic
	public void simulate(float deltaTime) {
		
		if ( deltaTime > 0.25f ) deltaTime = 0.25f;	  // note: max frame time to avoid spiral of death
        accumulator += deltaTime;
        
        while (accumulator >= fixedTimeStep) {
        	
        	if (_gameObjectManager != null) _gameObjectManager.copyCurrentPosition();
        	
        	//--- important: 1. update box2d, 2. update world => else: render-difference between fixture and objects
        	if (box2dworld != null) box2dworld.step(fixedTimeStep, VELOCITY_ITERATIONS, POSITION_ITERATIONS);	
        	
        	if (gameWorld != null) gameWorld.update(fixedTimeStep);
  
        	accumulator -= fixedTimeStep;
        	if (_gameObjectManager != null) _gameObjectManager.interpolateCurrentPosition(accumulator / fixedTimeStep);
    		    		
        	//---------- FPS check -----------------------------
    		if (calculateFPS) {
	        	logic_frameCount ++;
	        	logic_now = System.nanoTime();	// zeit loggen
	    		
	    		if ((logic_now - logic_lastRender) >= logic_FPSupdateIntervall * 1000000000)  {
	
	    			logic_lastFPS = logic_frameCount / logic_FPSupdateIntervall;
	    					
	    			logic_frameCount = 0;
	    			logic_lastRender = System.nanoTime();
	    			Gdx.app.log("MapService", "simulate - logic-FPS: " + logic_lastFPS);
	    		}
    		}
    		//--------------------------------------------------------------
        }
	}
	
	public void render() {
//		if (_backgroundRenderer != null) _backgroundRenderer.render();	// TODO // FIXME
		if (_tiledMapManager != null && _tiledMapManager.tiledMap != null) _tiledMapManager.render();
		if (gameRenderer != null) gameRenderer.render();
	}
	
    public MapService setGameWorld(BaseGameWorld gameWorld) {
    	this.gameWorld = gameWorld;
    	return this;
    }
    public BaseGameWorld getGameWorld() {
    	return gameWorld;
    }
    
	public BaseGameRenderer getGameRenderer() {
		return gameRenderer;
	}
		
	public MapService setGameRenderer(BaseGameRenderer gameRenderer) {
		this.gameRenderer = gameRenderer;
		return this;
	}
		
	public AbstractScreen getScreen() {
		return gameScreen;
	}
	
	public MgrTiledMapManager getTiledMapManager() {
		if (_tiledMapManager == null) {
			_tiledMapManager = new MgrTiledMapManager(this);
		}
		return _tiledMapManager;
	}	
	public void disposeTiledMapManager() {
		_tiledMapManager.dispose();
		_tiledMapManager = null;
	}
	
	@Override
	public void dispose() {
//		if (_particleFxManager != null) _particleFxManager.clear();	// this is a bug! -> clear the mgr after it has been setup by the gamescreen
		if (box2dworld != null) box2dworld.dispose();
		if (gameWorld != null) gameWorld.dispose();		// VERY IMPORTANT => !! let the background thread stop!!!
		if (gameRenderer != null) gameRenderer.dispose();	
		if (_tiledMapManager != null) disposeTiledMapManager();
	}
}
