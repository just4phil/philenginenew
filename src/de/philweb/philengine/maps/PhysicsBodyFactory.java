package de.philweb.philengine.maps;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.philweb.philengine.gameobjects.GoGameObject;


public class PhysicsBodyFactory {

	
	public static Fixture addCircleFixture(int fixtureID, GoGameObject gameObject, float radiusInMeter, boolean isSensor) {
		Fixture fix;
		FixtureDef characterFixtureDef = new FixtureDef();
		CircleShape characterShape = new CircleShape();
		characterShape.setRadius(radiusInMeter - (radiusInMeter * 0.3f)); 				// minus fix-verkleinerung 30%
		
		characterFixtureDef.shape = characterShape;
		characterFixtureDef.density = 1.0f;
		characterFixtureDef.isSensor = isSensor;
		
		fix = gameObject.getBody().createFixture(characterFixtureDef); 
		fix.setUserData(fixtureID);
		characterShape.dispose();
		return fix;
	}
	
	public static Fixture addPolygonFixture(int fixtureID, GoGameObject gameObject, Vector2[] vertices, float mass) {
		Fixture fix;	
		PolygonShape characterShape = new PolygonShape();
		characterShape.set(vertices);
		
		FixtureDef characterFixtureDef = new FixtureDef();
		characterFixtureDef.shape = characterShape;
		characterFixtureDef.density = 1.0f;
		 
		fix = gameObject.getBody().createFixture(characterFixtureDef);
		fix.setUserData(fixtureID);
		characterShape.dispose(); 
		return fix;
	}
	
	public static Fixture addRectangleFixture(int fixtureID, GoGameObject gameObject, float w, float h) {
		return addRectangleFixture(fixtureID, gameObject, w, h, 0f, 0f, 0f, 0f, 0f, 0f);
	}
	
	public static Fixture addRectangleFixture(int fixtureID, GoGameObject gameObject, float w, float h, float offsetX, float offsetY, float rotationInDegrees, float mass) {
		return addRectangleFixture(fixtureID, gameObject, w, h, offsetX, offsetY, rotationInDegrees, mass, 0f, 0f);
	}
	
	public static Fixture addRectangleFixture(int fixtureID, GoGameObject gameObject, float w, float h, float offsetX, float offsetY, float rotationInDegrees, float mass, float smallerX, float smallerY) {
		Fixture fix;
		Vector2 offset = new Vector2();
		offset.set(offsetX, offsetY); 		//---offset for fixture
//		offset.set(w * 0.5f, h * 0.5f); 		//---offset for fixture
		
		FixtureDef characterFixtureDef = new FixtureDef();
		PolygonShape characterShape = new PolygonShape();
//		characterShape.setAsBox(w * 0.5f, h * 0.5f, offset, 0.0f);
		characterShape.setAsBox((w * 0.5f) -smallerX, (h * 0.5f) - smallerY, offset, MathUtils.degreesToRadians * rotationInDegrees);
		characterFixtureDef.shape = characterShape;
		characterFixtureDef.density = 1.0f;
		
		fix = gameObject.getBody().createFixture(characterFixtureDef);
		fix.setUserData(fixtureID);
		characterShape.dispose(); 
		return fix;
	}
	
	public static Fixture addRectangleFixture(int fixtureID, GoGameObject gameObject, float w, float h, float offsetX, float offsetY, float rotationInDegrees) {
		return addRectangleFixture(fixtureID, gameObject, w, h, offsetX, offsetY, rotationInDegrees, 0f);
	}

	public static Fixture addStaticTileBodyAndFixture(String name, World box2dWorld, float x, float y, float w, float h, float offsetX, float offsetY) {
		Fixture fix;
		BodyDef characterBodyDef = new BodyDef();
		characterBodyDef.type = BodyDef.BodyType.StaticBody;

		Body body = box2dWorld.createBody(characterBodyDef);
//		body.setUserData(gameObject);
		body.setTransform(x - w * 0.5f, y - h * 0.5f, 0f);

		Vector2 offset = new Vector2();
		offset.set(0f, 0f); 		//---offset for fixture
		
		FixtureDef characterFixtureDef = new FixtureDef();
		PolygonShape characterShape = new PolygonShape();
		characterShape.setAsBox(w * 0.5f, h * 0.5f, offset, 0.0f);
		
		characterFixtureDef.shape = characterShape;
		 
		fix = body.createFixture(characterFixtureDef);
		fix.setUserData(name);
		characterShape.dispose(); 
		return fix;
	}
}
