//package de.philweb.philengine.maps;
//
//import java.util.ArrayList;
//
//import box2dLight.RayHandler;
//
//import com.badlogic.gdx.maps.MapObjects;
//import com.badlogic.gdx.maps.objects.RectangleMapObject;
//import com.badlogic.gdx.maps.tiled.TiledMap;
//import com.badlogic.gdx.physics.box2d.World;
//
//import de.philweb.philengine.controllers.ControllerNone;
//import de.philweb.philengine.gameobjects.GoGameObject;
//
//public class TiledMapLevelLoader {
//
//
//	
//	//=== returns properties that can be in maps, in layers or in objects =======================================================
//	
//	public static float getHierarchyPropertyFloat(TiledMap map, String search, int layerCounter, int objectCounter, float defaultValue) {
//		
//		float value = defaultValue;
//		
//		if(map.getProperties().containsKey(search)) 
//			value = Float.parseFloat(map.getProperties().get(search, String.class));
//		
//		if(map.getLayers().get(layerCounter).getProperties().containsKey(search)) 
//			value = Float.parseFloat(map.getLayers().get(layerCounter).getProperties().get(search, String.class));
//
//		if(map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().containsKey(search)) 
//			value = Float.parseFloat(map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().get(search, String.class));
//		
//		return value;
//	}
//	
//	//============================================================================================
//	
//	public static String getHierarchyPropertyString(TiledMap map, String search, int layerCounter, int objectCounter, String defaultValue) {
//		
//		String value = defaultValue;
//		
//		if(map.getProperties().containsKey(search)) 
//			value = map.getProperties().get(search, String.class);
//		
//		if(map.getLayers().get(layerCounter).getProperties().containsKey(search)) 
//			value = map.getLayers().get(layerCounter).getProperties().get(search, String.class);
//
//		if(map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().containsKey(search)) 
//			value = map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().get(search, String.class);
//		
//		return value;
//	}
//	
//	//============================================================================================
//	
//	public static int getHierarchyPropertyInteger(TiledMap map, String search, int layerCounter, int objectCounter, int defaultValue) {
//		
//		String string = "";
//		int value = defaultValue;
//		
//		if(map.getProperties().containsKey(search)) 
//			string = map.getProperties().get(search, String.class);
//		
//		if(map.getLayers().get(layerCounter).getProperties().containsKey(search)) 
//			string = map.getLayers().get(layerCounter).getProperties().get(search, String.class);
//
//		if(map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().containsKey(search)) 
//			string = map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().get(search, String.class);
//		
//		if (!string.equals("")) {
//			
//			value = Integer.parseInt(string);
//		}
//		
//		return value;
//	}
//	
//	//============================================================================================
//	
//	public static boolean getHierarchyPropertyBoolean(TiledMap map, String search, int layerCounter, int objectCounter, boolean defaultValue) {
//		
//		boolean value = defaultValue;
//		
//		if(map.getProperties().containsKey(search)) 
//			value = Boolean.parseBoolean(map.getProperties().get(search, String.class));
//		
//		if(map.getLayers().get(layerCounter).getProperties().containsKey(search)) 
//			value = Boolean.parseBoolean(map.getLayers().get(layerCounter).getProperties().get(search, String.class));
//
//		if(map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().containsKey(search)) 
//			value = Boolean.parseBoolean(map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().get(search, String.class));
//		
//		return value;
//	}
//	
//	//============================================================================================
//	
//	public static long getHierarchyPropertyLong(TiledMap map, String search, int layerCounter, int objectCounter, long defaultValue) {
//		
//		long value = defaultValue;
//		String longString = "";
//		
//		if(map.getProperties().containsKey(search)) 
//			longString = map.getProperties().get(search, String.class);
//		
//		if(map.getLayers().get(layerCounter).getProperties().containsKey(search)) 
//			longString = map.getLayers().get(layerCounter).getProperties().get(search, String.class);
//
//		if(map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().containsKey(search)) 
//			longString = map.getLayers().get(layerCounter).getObjects().get(objectCounter).getProperties().get(search, String.class);
//		
//		if (!longString.equals("")) {
//			
//			value = Long.parseLong(longString);
//		}
//		
//		return value;
//	}
//	
//	//================================================================================================
//	
//	
//////------------ liest die objekte (monster + player) aus der map --------------------------------
//
//
//	
//	public static ArrayList<GoGameObject> getObjectList(TiledMap map, String ID, Welt world, float originalTildemapPPM) {
//		RectangleMapObject rectangleMapObject;
//		float x;
//		float y;
//		float w;
//		float h;
//		ArrayList<GoGameObject> objectList = new ArrayList<GoGameObject>();
//		
//		int anz_objectGroups = map.getLayers().getCount();	
//		
//		for (int counter_groups = 0; counter_groups < anz_objectGroups; counter_groups++) {
//		
//			if (map.getLayers().get(counter_groups).getName().equals(ID)) {
//				
//				int anz_objekte = map.getLayers().get(counter_groups).getObjects().getCount();	
//				
//				for (int counter = 0; counter < anz_objekte; counter++) {
//
//					rectangleMapObject = (RectangleMapObject)map.getLayers().get(counter_groups).getObjects().get(counter);
//					w = (rectangleMapObject.getRectangle().width) / originalTildemapPPM;
//					h = (rectangleMapObject.getRectangle().height) / originalTildemapPPM;
//					x = (rectangleMapObject.getRectangle().x / originalTildemapPPM) + (w * 0.5f);
//					y = (rectangleMapObject.getRectangle().y / originalTildemapPPM) + (h * 0.5f);
//					
//					GoGameObject objekt  = new GoGameObject();
//					objekt.position_m.x = Welt.PLAYFIELDOFFSETX + x;
//					objekt.position_m.y = Welt.PLAYFIELDOFFSETY + y;
//					objekt.assetName = map.getLayers().get(counter_groups).getObjects().get(counter).getName();
//					objectList.add(objekt);
//				}
//			}
//		}
//
//		return objectList;
//	}
//	
//	//-------------------------------------------------------
//	
//	
//	
//	public static ArrayList<Exit> getExitList(TiledMap map, Welt world, float originalTildemapPPM) {
//		RectangleMapObject rectangleMapObject;
//		float x;
//		float y;
//		float w;
//		float h;
//		ArrayList<Exit> exitList = new ArrayList<Exit>();
//
//		if (map.getLayers().get("exit") != null) {
//
//			MapObjects mapObjects = map.getLayers().get("exit").getObjects();	
//			int anz_objekte = mapObjects.getCount();	
//			
//			for (int counter = 0; counter < anz_objekte; counter++) {
//				
//				rectangleMapObject = (RectangleMapObject)mapObjects.get(counter);
//				x = rectangleMapObject.getRectangle().x / originalTildemapPPM;
//				y = (rectangleMapObject.getRectangle().y) / originalTildemapPPM;
//				w = (rectangleMapObject.getRectangle().width) / originalTildemapPPM;
//				h = (rectangleMapObject.getRectangle().height) / originalTildemapPPM;
//
//				Exit objekt  = new Exit(world);
//				objekt.position_m.x = Welt.PLAYFIELDOFFSETX + x + (w * 0.5f);
//				objekt.position_m.y = Welt.PLAYFIELDOFFSETY + y + (h * 0.5f);
//				
//				exitList.add(objekt);
//				
////				Gdx.app.log("exit pos x: " + x, "exit pos y: " + y); 
//			}
//		}
//		return exitList;
//	}
//	
//	
//	//------------------------------------------------------
//	
//	public static ArrayList<Collectable> getCollectablesList(TiledMap map, World box2dwelt, RayHandler rayHandler, boolean isLightsEnabled, boolean isParticlesEnabled, Welt world, float originalTildemapPPM) {
//		
//		RectangleMapObject rectangleMapObject;
//		ArrayList<Collectable> collectablesList = new ArrayList<Collectable>();
//		float x;
//		float y;
//		float w;
//		float h;
//		int anz_objectGroups = map.getLayers().getCount();	
//		
//		for (int counter_groups = 0; counter_groups < anz_objectGroups; counter_groups++) {
//		
//			if (map.getLayers().get(counter_groups).getName().equals("collectables")) {
//				
//				MapObjects mapObjects = map.getLayers().get("collectables").getObjects();
//				int anz_objekte = map.getLayers().get(counter_groups).getObjects().getCount();	
//				
//				for (int counter = 0; counter < anz_objekte; counter++) {
//					
////					float x = Welt.PLAYFIELDOFFSETX + ((mapObjects.get(counter).getProperties().get("x", float.class) / originalTildemapPPM) + Collectable.TILEDMAPVERSATZ_X);
////					float y = Welt.PLAYFIELDOFFSETY + (((mapHeight - mapObjects.get(counter).getProperties().get("y", float.class)) / originalTildemapPPM) + Collectable.TILEDMAPVERSATZ_Y);
//					
//					rectangleMapObject = (RectangleMapObject)map.getLayers().get(counter_groups).getObjects().get(counter);
//					w = (rectangleMapObject.getRectangle().width) / originalTildemapPPM;
//					h = (rectangleMapObject.getRectangle().height) / originalTildemapPPM;
//					x = (rectangleMapObject.getRectangle().x / originalTildemapPPM) + (w * 0.5f);
//					y = (rectangleMapObject.getRectangle().y / originalTildemapPPM) + (h * 0.5f);
//					
//					Collectable objekt  = new Collectable(world, box2dwelt, rayHandler, isLightsEnabled, isParticlesEnabled);
//					
//					objekt.position_m.x = Welt.PLAYFIELDOFFSETX + x;
//					objekt.position_m.y = Welt.PLAYFIELDOFFSETY + y;
//					//----------------------
//					
//					String search;
//					String parse;
//			
//					
//					//----------- collectableAsset -----------
//					objekt.collectableID = Collectable.COLLECTABLEID_EMPTY;
//					
//					search = "collectableAsset";
//					parse = "";					
//					String collectableAsset = getHierarchyPropertyString(map, search, counter_groups, counter, Welt.COLLECTABLEASSET);
//					
//					objekt.collectableID = Collectable.getCollectableID(collectableAsset);
//									
//				
//					//----- collectablePoints -------------------------------------
//					
////					int collectablePoints = Welt.COLLECTABLEPOINTS;
//					int collectablePoints;
//					
//					search = "collectablePoints";
//					parse = "";
//					
//					collectablePoints = getHierarchyPropertyInteger(map, search, counter_groups, counter, Collectable.getPoints(objekt.collectableID, false));
//										
//					objekt.score = collectablePoints;
//					
//					////------------------------------------------
//
//
//					String collectableType;
//					search = "collectableType";
//					parse = "";
//					
//					collectableType = getHierarchyPropertyString(map, search, counter_groups, counter, Welt.COLLECTABLETYPE);
//					
//					objekt.collectableTypeID = Collectable.getCollectableTypeID(collectableType);
//					
//					
//					
//					//----- collectable functionDuration -------------------------------------
//					
//					int functionDuration;
//					search = "functionDuration";
//					parse = "";
//					
//					functionDuration = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.functionDuration);
//										
//					objekt.functionDuration = functionDuration;
//					
//					
//					
//			//----- functionValue -------------------------------------
//					
//					float functionValue;
//					search = "functionValue";
//					
//					functionValue = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.functionValue);
//
//					objekt.functionValue = functionValue;
//					
//					//----------------------------------------------------
//					
//					collectablesList.add(objekt);
//				}
//			}
//		}
//
//		return collectablesList;
//	}
//	
//	
//
//	
//	
//	
//	
//	
//	//------------------------------------------------------
//	
//	public static ArrayList<Cannon> getCannonList(TiledMap map, World box2dwelt, boolean particlesEnabled, ArrayList<BubblrParticleEmitter> fxEmitters, Welt welt, float originalTildemapPPM) {
//		
//		RectangleMapObject rectangleMapObject;
//		ArrayList<Cannon> cannonList = new ArrayList<Cannon>();
//		float x;
//		float y;
//		float w;
//		float h;
//		
//		int anz_objectGroups = map.getLayers().getCount();	
//		
//		for (int counter_groups = 0; counter_groups < anz_objectGroups; counter_groups++) {
//		
//			if (map.getLayers().get(counter_groups).getName().equals("cannons")) {
//				
//				MapObjects mapObjects = map.getLayers().get("cannons").getObjects();	
//				int anz_objekte = map.getLayers().get(counter_groups).getObjects().getCount();	
//				
//				for (int counter = 0; counter < anz_objekte; counter++) {
//					
//					rectangleMapObject = (RectangleMapObject)map.getLayers().get("cannons").getObjects().get(counter);
//					w = (rectangleMapObject.getRectangle().width) / originalTildemapPPM;
//					h = (rectangleMapObject.getRectangle().height) / originalTildemapPPM;
//					x = Welt.PLAYFIELDOFFSETX + (rectangleMapObject.getRectangle().x / originalTildemapPPM) + (w * 0.5f) + Cannon.CANNONFIXTUREOFFSET_x;
//					y = Welt.PLAYFIELDOFFSETY + ((rectangleMapObject.getRectangle().y) / originalTildemapPPM) + (h * 0.5f) + Cannon.CANNONFIXTUREOFFSET_y;
//
//					Cannon objekt  = new Cannon(x, y, box2dwelt, particlesEnabled, fxEmitters, welt);
//
//					//----------------------
//					
//					String search;
//					String parse;
//					
//					//----- cannonFacingRight -------------------------------------
//					
//					int cannonFacingRight;
//					search = "cannonFacingRight";
//					parse = "";
//					
//					cannonFacingRight = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.CANNONFACINGRIGHT);
//					
//					if (cannonFacingRight == 0) {
//						objekt.facingRight = false;
//					}
//					else {
//						objekt.facingRight = true;
//					}
//
//					//----- cannonAutoFire -------------------------------------
//					
//					int cannonAutoFire;
//					search = "cannonAutoFire";
//					parse = "";
//					
//					cannonAutoFire = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.CANNONAUTOFIRE);
//					
//					if (cannonAutoFire == 0) {
//						objekt.autoFire = false;
//					}
//					else {
//						objekt.autoFire = true;
//					}
//					
//			
//					//----- cannonMayBeUsed -------------------------------------
//					
//					int cannonMayBeUsed;
//					search = "cannonMayBeUsed";
//					parse = "";
//					
//					cannonMayBeUsed = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.CANNONMAYBEUSED);
//					
//					if (cannonMayBeUsed == 0) {
//						objekt.mayBeUsed = false;
//					}
//					else {
//						objekt.mayBeUsed = true;
//					}
//					
//					
//					//----- cannonAngleDegrees -------------------------------------
//					
//					float cannonAngleDegrees;
//					search = "cannonAngleDegrees";
//					parse = "";
//					
//					cannonAngleDegrees = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.CANNONANGLEDEGREES);
//					
//					objekt.angleDegrees = cannonAngleDegrees;
//					
//					
//			//----- cannonAngleDegrees -------------------------------------
//					
//					float cannonAngularVelocity;
//					search = "cannonAngularVelocity";
//					parse = "";
//					
//					cannonAngularVelocity = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.CANNONANGULARVELOCITY);
//					
//					objekt.angularVelocity = cannonAngularVelocity;
//					
//					
//			//----- cannonFireRate -------------------------------------
//					
//					float cannonFireRate;
//					search = "cannonFireRate";
//					parse = "";
//					
//					cannonFireRate = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.CANNONFIRERATE);
//					
//					objekt.fireRate = cannonFireRate;
//					
//					
//			//----- cannonFirePower -------------------------------------
//					
//					float cannonFirePower;
//					search = "cannonFirePower";
//					parse = "";
//					
//					cannonFirePower = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.CANNONFIREPOWER);
//					
//					objekt.firePower = cannonFirePower;
//					//------------------------------------------
//					
//					
//					//----- cannonAutoRotate -------------------------------------
//					
//					int cannonAutoRotate;
//					search = "cannonAutoRotate";
//					parse = "";
//					
//					cannonAutoRotate = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.CANNONAUTOROTATE);
//					
//					if (cannonAutoRotate == 0) {
//						objekt.autoRotate = false;
//					}
//					else {
//						objekt.autoRotate = true;
//					}
//					
//					
//					//----- cannonMaxDegrees -------------------------------------
//					
//					int cannonMaxDegrees;
//					search = "cannonMaxDegrees";
//					parse = "";
//					
//					cannonMaxDegrees = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.CANNONMAXDEGRESS);
//					
//					objekt.maxDegrees = cannonMaxDegrees;
//					
//					
//					//----- cannonMinDegrees -------------------------------------
//					
//					int cannonMinDegrees;
//					search = "cannonMinDegrees";
//					parse = "";
//					
//					cannonMinDegrees = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.CANNONMINDEGRESS);
//					
//					objekt.minDegrees = cannonMinDegrees;
//					
//					
//					//----- cannonDestroyable -------------------------------------
//					
//					int cannonDestroyable;
//					search = "cannonDestroyable";
//					parse = "";
//					
//					cannonDestroyable = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.CANNONDESTROYABLE);
//					
//					if (cannonDestroyable == 0) {
//						objekt.destroyable = false;
//					}
//					else {
//						objekt.destroyable = true;
//					}
//					
//					
//					
//					//----- cannonCanDestroyBarrier -------------------------------------
//					
//					int cannonCanDestroyBarrier;
//					search = "cannonCanDestroyBarrier";
//					parse = "";
//					
//					cannonCanDestroyBarrier = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.CANNONCANDESTROYBARRIER);
//					
//					if (cannonCanDestroyBarrier == 0) {
//						objekt.canDestroyBarrier = false;
//					}
//					else {
//						objekt.canDestroyBarrier = true;
//					}
//					
//					
//					//----------------------------------------------------
//					
//					cannonList.add(objekt);
//				}
//			}
//		}
//
//		return cannonList;
//	}
//	
//	 
//	
//	 
//	
//	
//	//------------------------------------------------------
//	
//	public static ArrayList<FloatingPlattform> getPlattformList(TiledMap map, Welt world, float originalTildemapPPM) {
//		
//		RectangleMapObject rectangleMapObject;
//		ArrayList<FloatingPlattform> plattformList = new ArrayList<FloatingPlattform>();	
//		float x;
//		float y;
//		float w;
//		float h;
//		
//		int anz_objectGroups = map.getLayers().getCount();	
//		
//		for (int counter_groups = 0; counter_groups < anz_objectGroups; counter_groups++) {
//		
//			if (map.getLayers().get(counter_groups).getName().equals("floatingPlattforms")) {
//				
//				MapObjects mapObjects = map.getLayers().get("floatingPlattforms").getObjects();
//				int anz_objekte = map.getLayers().get(counter_groups).getObjects().getCount();	
//				
//				for (int counter = 0; counter < anz_objekte; counter++) {
//					
////					float x = Welt.PLAYFIELDOFFSETX + ((mapObjects.get(counter).getProperties().get("x", float.class) / originalTildemapPPM) + FloatingPlattform.TILEDMAPVERSATZ_X);
////					float y = Welt.PLAYFIELDOFFSETY + (((mapHeight - mapObjects.get(counter).getProperties().get("y", float.class)) / originalTildemapPPM) + FloatingPlattform.TILEDMAPVERSATZ_Y);
//
//					rectangleMapObject = (RectangleMapObject)map.getLayers().get("floatingPlattforms").getObjects().get(counter);
//					w = (rectangleMapObject.getRectangle().width) / originalTildemapPPM;
//					h = (rectangleMapObject.getRectangle().height) / originalTildemapPPM;
//					x = Welt.PLAYFIELDOFFSETX + (rectangleMapObject.getRectangle().x / originalTildemapPPM) + (w * 0.5f);
//					y = Welt.PLAYFIELDOFFSETY + ((rectangleMapObject.getRectangle().y) / originalTildemapPPM) + (h * 0.5f);
//				
//					FloatingPlattform objekt  = new FloatingPlattform(x, y, w, h);
//					//----------------------
//					
//					String search;
//					String parse;
//
//					
////					//----- plattformMoveLeftRight -------------------------------------
//					
//					int plattformWidthInTiles;
//					search = "plattformWidthInTiles";
//					parse = "";
//					
//					plattformWidthInTiles = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.plattformWidthInTiles);
//					
//					objekt.plattformWidthInTiles = plattformWidthInTiles;
//			
//					
//					
////					//----- plattformMoveLeftRight -------------------------------------
//					
//					int plattformMoveLeftRight;
//					search = "plattformMoveLeftRight";
//					parse = "";
//					
//					plattformMoveLeftRight = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.plattformMoveLeftRight);
//					
//					if (plattformMoveLeftRight == 0) {
//						objekt.plattformMoveLeftRight = false;
//					}
//					else {
//						objekt.plattformMoveLeftRight = true;
//					}	
//					
//					
////					//----- plattformMoveUpDown -------------------------------------
//					
//					int plattformMoveUpDown;
//					search = "plattformMoveUpDown";
//					parse = "";
//					
//					plattformMoveUpDown = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.plattformMoveUpDown);
//					
//					if (plattformMoveUpDown == 0) {
//						objekt.plattformMoveUpDown = false;
//					}
//					else {
//						objekt.plattformMoveUpDown = true;
//					}
//					
//					
////					//----- plattformStartRight -------------------------------------
//					
//					int plattformStartRight;
//					search = "plattformStartRight";
//					parse = "";
//					
//					plattformStartRight = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.plattformStartRight);
//					
//					if (plattformStartRight == 0) {
//						objekt.plattformStartRight = false;
//					}
//					else {
//						objekt.plattformStartRight = true;
//					}
////
////					//----- plattformStartUp -------------------------------------
//					
//					int plattformStartUp;
//					search = "plattformStartUp";
//					parse = "";
//					
//					plattformStartUp = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.plattformStartUp);
//					
//					if (plattformStartUp == 0) {
//						objekt.plattformStartUp = false;
//					}
//					else {
//						objekt.plattformStartUp = true;
//					}
//					
////			
////					//----- cannonMayBeUsed -------------------------------------
//					
//					int plattformAutoMove;
//					search = "plattformAutoMove";
//					parse = "";
//					
//					plattformAutoMove = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.plattformAutoMove);
//					
//					if (plattformAutoMove == 0) {
//						objekt.plattformAutoMove = false;
//					}
//					else {
//						objekt.plattformAutoMove = true;
//					}
//					
//					
////			//----- plattformMinPosX -------------------------------------
//					
//					float plattformMinPosX;
//					search = "plattformMinPosX";
//					parse = "";
//					
//					plattformMinPosX = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.plattformMinPosX);
//					plattformMinPosX = Welt.PLAYFIELDOFFSETX + (plattformMinPosX * Welt.TILESVIRTUALRESOLUTION  / originalTildemapPPM);
//
//					objekt.plattformMinPosX = plattformMinPosX;
//					
//					
////			//----- plattformMaxPosX -------------------------------------
//					
//					float plattformMaxPosX;
//					search = "plattformMaxPosX";
//					parse = "";
//					
//					plattformMaxPosX = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.plattformMaxPosX);
//					plattformMaxPosX = Welt.PLAYFIELDOFFSETX + (plattformMaxPosX * Welt.TILESVIRTUALRESOLUTION  / originalTildemapPPM);
//
//					objekt.plattformMaxPosX = plattformMaxPosX;
//					
//					
////			//----- plattformMaxPosY -------------------------------------
//					
//					float plattformMaxPosY;
//					search = "plattformMaxPosY";
//					parse = "";
//					 
//					plattformMaxPosY = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.plattformMaxPosY);
//					if (plattformMaxPosY != 0) plattformMaxPosY = (Welt.TILESVERICALLY - plattformMaxPosY) * Welt.TILESVIRTUALRESOLUTION  / originalTildemapPPM; 	
//					// hier ist playfieldoffset falsch, da plattformen bei wert = 0 durch die decke/den boden gehen sollen. mit offset ist der wert immer != 0
//					// die abfrage if (plattformMinPosY != 0)  ist nötig, da wert = 0 dazu führen soll dass plattformen durch decken/böden gehen.... ohne die abfrage tritt wert = 0 durch konvertierung nicht ein
//					
//					objekt.plattformMaxPosY = plattformMaxPosY;
//					
//					
////			//----- plattformMinPosY -------------------------------------
//					
//					float plattformMinPosY;
//					search = "plattformMinPosY";
//					parse = "";
//					
//					plattformMinPosY = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.plattformMinPosY);
//					if (plattformMinPosY != 0) plattformMinPosY = (Welt.TILESVERICALLY - plattformMinPosY) * Welt.TILESVIRTUALRESOLUTION  / originalTildemapPPM; 	
//					// hier ist playfieldoffset falsch, da plattformen bei wert = 0 durch die decke/den boden gehen sollen. mit offset ist der wert immer != 0
//					// die abfrage if (plattformMinPosY != 0)  ist nötig, da wert = 0 dazu führen soll dass plattformen durch decken/böden gehen.... ohne die abfrage tritt wert = 0 durch konvertierung nicht ein
//					
//					objekt.plattformMinPosY = plattformMinPosY;
////					//------------------------------------------
////					
////					
//					
//					
////			//----- plattformSpeed -------------------------------------
//					
//					float plattformSpeed;
//					search = "plattformSpeed";
//					parse = "";
//					
//					plattformSpeed = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.plattformSpeed);
//					
//					objekt.plattformSpeed = plattformSpeed;
////					//------------------------------------------
//					
//					
////					//----- plattformDestroyable -------------------------------------
////					
////					int plattformDestroyable;
////					search = "plattformDestroyable";
////					parse = "";
////					
////					plattformDestroyable = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.plattformDestroyable);
////					
////					if (plattformDestroyable == 0) {
////						objekt.plattformDestroyable = false;
////					}
////					else {
////						objekt.plattformDestroyable = true;
////					}
//					
//					
////					//----- plattformSelfDestruct -------------------------------------
////					int plattformSelfDestruct;
////					search = "plattformSelfDestruct";
////					parse = "";
////					
////					plattformSelfDestruct = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.plattformSelfDestruct);
////					
////					if (plattformSelfDestruct == 0) {
////						objekt.plattformSelfDestruct = false;
////					}
////					else {
////						objekt.plattformSelfDestruct = true;
////					}
//					
//					
////					//----- plattformSelfDestructTime -------------------------------------
////					long plattformSelfDestructTime;
////					search = "plattformSelfDestructTime";
////					parse = "";
////					
////					plattformSelfDestructTime = getHierarchyPropertyLong(map, search, counter_groups, counter, Welt.plattformSelfDestructTime);
////					
////					objekt.plattformSelfDestructTime = plattformSelfDestructTime;
////							//------------------------------------------
//					
//					
////							//----- plattformSelfDestructDelayAfterTouch -------------------------------------
////					long plattformSelfDestructDelayAfterTouch;
////					search = "plattformSelfDestructDelayAfterTouch";
////					parse = "";
////					
////					plattformSelfDestructDelayAfterTouch = getHierarchyPropertyLong(map, search, counter_groups, counter, Welt.plattformSelfDestructDelayAfterTouch);
////					
////					objekt.plattformSelfDestructDelayAfterTouch = plattformSelfDestructDelayAfterTouch;
////							//------------------------------------------	
//
//					
//					
////					String plattformAsset;
////					search = "plattformAsset";
////					parse = "";
////					
////					plattformAsset = getHierarchyPropertyString(map, search, counter_groups, counter, Welt.plattformAsset);
////					
////					objekt.plattformAsset = plattformAsset;
//							
//					
//					//----------------------------------------------------
//					
//					plattformList.add(objekt);
//				}
//			}
//		}
//
//		return plattformList;
//	}
//	
//	
//	
//	
//	
//	
//////------------ liest die monster aus der map --------------------------------
//
//	public static ArrayList<character_badGuy> getMonsterList(TiledMap map, String ID, World box2dwelt, Welt world, float originalTildemapPPM) {
//		
//		RectangleMapObject rectangleMapObject;
//		float monsterSpeed;
//		String monsterSpeed_parse;
//		int points4kill;
//		int collectablePoints;
////		String collectableType;
//		int collectableTypeID;
//		String AI;
//		String AI_parse;
//		int weapon_usage;
//		String weapon_usage_parse;
//		String weapon;
//		String weapon_parse;
//		
//		String search;
//		String parse;
//		float x;
//		float y;
//		float w;
//		float h;
//		
//		ArrayList<character_badGuy> objectList = new ArrayList<character_badGuy>();
//		
//		
//		int anz_objectGroups = map.getLayers().getCount();	
//		
//		for (int counter_groups = 0; counter_groups < anz_objectGroups; counter_groups++) {
//		
//			if (map.getLayers().get(counter_groups).getName().equals(ID)) {
//				
//				MapObjects mapObjects = map.getLayers().get(ID).getObjects();
//				int anz_objekte = map.getLayers().get(counter_groups).getObjects().getCount();	
//				
//				for (int counter = 0; counter < anz_objekte; counter++) {
//					
////					float x = Welt.PLAYFIELDOFFSETX + (mapObjects.get(counter).getProperties().get("x", float.class) / originalTildemapPPM);
////					float y = Welt.PLAYFIELDOFFSETY + ((mapHeight - mapObjects.get(counter).getProperties().get("y", float.class)) / originalTildemapPPM);
//					
//					rectangleMapObject = (RectangleMapObject)map.getLayers().get(counter_groups).getObjects().get(counter);
//					w = (rectangleMapObject.getRectangle().width) / originalTildemapPPM;
//					h = (rectangleMapObject.getRectangle().height) / originalTildemapPPM;
//					x = Welt.PLAYFIELDOFFSETX + (rectangleMapObject.getRectangle().x / originalTildemapPPM) + (w * 0.5f);
//					y = Welt.PLAYFIELDOFFSETY + ((rectangleMapObject.getRectangle().y) / originalTildemapPPM) + (h * 0.5f);
//					
//			//----- MONSTERSPEED -------------------------------------
//					
//					monsterSpeed_parse = "";
//					search = "monsterSpeed";
//					monsterSpeed = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.MONSTERSPEED);
//
//					
//			//----- MONSTERCANPASSFLOOR -------------------------------------
//					
//					int monsterCanPassFloor;
////					String monsterCanPassFloor_parse = "";
//					search = "monsterCanPassFloor";
//					monsterCanPassFloor = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.MONSTERCANPASSFLOOR);
//					
//					
//			//----- MONSTERCANPASSCEILINGS -------------------------------------
//					
//					int monsterCanPassCeilings;
////					String monsterCanPassCeilings_parse = "";
//					search = "monsterCanPassCeilings";
//					monsterCanPassCeilings = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.MONSTERCANPASSCEILINGS);					
//				
//				
//					
//			//----- MONSTERWEAPONUSAGE -------------------------------------
//					
//					int monsterWeaponUsage;
////					String monsterWeaponUsage_parse = "";
//					search = "monsterWeaponUsage";
//					monsterWeaponUsage = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.MONSTERWEAPONUSAGE);	
//					
//					
//			//----- monsterPoints4Kill -------------------------------------
//					
//					int monsterPoints4Kill;
////					String monsterPoints4Kill_parse = "";
//					search = "monsterPoints4Kill";
//					monsterPoints4Kill = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.MONSTERPOINTS4KILL);	
//					
//					
//					
//			//----- MONSTERASSET -------------------------------------					
//					
//					String monsterAsset;
////					String monsterAsset_parse;
//					search = "monsterAsset";
//					monsterAsset = getHierarchyPropertyString(map, search, counter_groups, counter, Welt.MONSTERASSET);	
//					
//					
//				//----- MONSTERAI ------------------------------------
//					
//					AI_parse = "";
//					search = "monsterAI";
//					AI = getHierarchyPropertyString(map, search, counter_groups, counter, Welt.MONSTERAI);	
//					
//					
//					
//			//----- monsterFacingRight -------------------------------------
//					
//					int monsterFacingRight;
////					String monsterFacingRight_parse = "";
//					search = "monsterFacingRight";
//					monsterFacingRight = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.MONSTERDIRECTIONRIGHT);
//					
//					
//					
//			//----- COLLECTABLEASSET -------------------------------------
//					
//					String collectableAsset;
////					String collectableAsset_parse = "";
//					search = "collectableAsset";
//					collectableAsset = getHierarchyPropertyString(map, search, counter_groups, counter, Welt.COLLECTABLEASSET);
//					
//					int collectableID = Collectable.getCollectableID(collectableAsset);
//					
//					
//			//----- collectablePoints -------------------------------------
//
//					search = "collectablePoints";
////					String collectablePoints_parse = "";
//					
//					collectablePoints = getHierarchyPropertyInteger(map, search, counter_groups, counter, Collectable.getPoints(collectableID, false));
//
//					
//			//----- COLLECTABLETYPE -------------------------------------
//
////					String collectableType_parse = "";
//					search = "collectableType";
//					collectableTypeID = Collectable.getCollectableTypeID(getHierarchyPropertyString(map, search, counter_groups, counter, Welt.COLLECTABLETYPE));	
//					
//					
//					//----- collectable functionDuration -------------------------------------
//					
//					int functionDuration;
//					search = "functionDuration";
//					parse = "";
//					
//					functionDuration = getHierarchyPropertyInteger(map, search, counter_groups, counter, Welt.functionDuration);
//														
//
//					
//					//----- functionValue -------------------------------------
//					
//					float functionValue;
//					search = "functionValue";
//					parse = "";
//					
//					functionValue = getHierarchyPropertyFloat(map, search, counter_groups, counter, Welt.functionValue);
//					
//					
//					
//				//----- pr�fen auf waffe ---------------------------
//				
//					weapon_parse = "";
//					search = "monsterWeapon";
//					weapon = getHierarchyPropertyString(map, search, counter_groups, counter, Welt.MONSTERWEAPON);	
//					
//
//					
//					//----- monsterRTDnoCount --------------- flag signals if the monster is a "release the dogs" monster. those monster must not be killed to finish the level (kill of normal monsters is ok)
//					
//					int monsterRTDnoCount;
//					search = "monsterRTDnoCount";
//					parse = "";
//					
//					monsterRTDnoCount = getHierarchyPropertyInteger(map, search, counter_groups, counter, 0);
//					
//					
//					
//					//------ re-size monster -------------------------------------
//				
//					float width = 0;
//					float height = 0;
//					float fix_verkleinerung_x = 0;
//					float fix_verkleinerung_y = 0;
//					
//				
//
//					//-------------- benzo / benzoNoJump----------------
//					if (AI.equals("benzo") || AI.equals("benzoNoJump")) {
//						width = character_badGuy.WIDTH_BENZO_M;
//						height = character_badGuy.HEIGHT_BENZO_M;
//						fix_verkleinerung_x = character_badGuy.FIXVERKLEINERUNG_X;
//						fix_verkleinerung_y = character_badGuy.FIXVERKLEINERUNG_Y;
//					}
//										
//					//-------------- GHOSTS ----------------
//					if (AI.equals("flyer")) {
//						width = character_badGuy.WIDTH_GHOST_M;
//						height= character_badGuy.HEIGHT_GHOST_M;
//						fix_verkleinerung_x = character_badGuy.FIXVERKLEINERUNG_X;
//						fix_verkleinerung_y = character_badGuy.FIXVERKLEINERUNG_Y;
//					}
//					
//					//------------- DRAGONS -----------------
////					if (AI.equals("krabbler")) {
////						width= Assets.dragon.getKeyFrame(0f, 0).getRegionWidth();
////						height= Assets.dragon.getKeyFrame(0f, 0).getRegionHeight();
////						fix_verkleinerung_x = 27;
////						fix_verkleinerung_y = 18;
////					}
//
//					
//					
//					character_badGuy objekt = new character_badGuy("monster", "monster", AI, x, y, width, height, fix_verkleinerung_x, fix_verkleinerung_y, true, box2dwelt, monsterSpeed, world);
//
//					
//				//------ DATEN SETZEN --------------------------------	
//					
//					objekt.assetName = monsterAsset;
////					objekt.position_m.x = Welt.PLAYFIELDOFFSETX + x;
////					objekt.position_m.y = Welt.PLAYFIELDOFFSETY + y;
//					
//					if (monsterFacingRight == 1) {
//						objekt.facingRight = true;
//					}
//					else {
//						objekt.facingRight = false;
//					}
//					
//					if (monsterCanPassCeilings == 1) {
//						objekt.monsterCanPassCeilings = true;
//					}
//					else {
//						objekt.monsterCanPassCeilings = false;
//					}
//					if (monsterCanPassFloor == 1) {
//						objekt.monsterCanPassFloor = true;
//					}
//					else {
//						objekt.monsterCanPassFloor = false;
//					}
//
//					
//					if (monsterRTDnoCount == 0) {
//						objekt.monsterRTDnoCount = false;
//					}
//					else {
//						objekt.monsterRTDnoCount = true;
//					}
//					
//					
//					objekt.points4kill = monsterPoints4Kill;
////					objekt.collectableType = collectableType;
//					objekt.collectableTypeID = collectableTypeID;
//					objekt.collectableID = collectableID;
//					objekt.collectablePoints = Collectable.getPoints(collectableID, false);
//					objekt.functionDuration = functionDuration;
//					objekt.functionValue = functionValue;
//					
//					if (AI.equals("none")) objekt.inputSource = new ControllerNone(world.bubblr, ControllerConfigManager.ControllerNONE);
////					if (AI.equals("krabbler")) objekt.inputSource = new Controller_AI_krabbler(world.bubblr, "krabbler");
//					if (AI.equals("benzoNoJump")) objekt.inputSource = new Controller_AI_krabbler(world.bubblr, "benzoNoJump");
//					if (AI.equals("benzo")) objekt.inputSource = new Controller_AI_benzo(world.bubblr, "benzo");
//					if (AI.equals("flyer")) objekt.inputSource = new Controller_AI_flyer(world.bubblr, "flyer");
//					
//					if (weapon.equals("none")) objekt.setWeapon(null);
//					if (weapon.equals("bubble")) objekt.setWeapon(new weapon_Bubble(world, Welt.WEAPON_BUBBLE_IMPULS, Welt.WEAPON_BUBBLE_DISTANCE, Welt.WEAPON_BUBBLE_LIFETIME, Welt.WEAPON_BUBBLE_CANCATCHTIME));
//					if (weapon.equals("fireball")) objekt.setWeapon(new WeaponFireball(Welt.WEAPON_FIREBALL_IMPULS , Welt.WEAPON_FIREBALL_DISTANCE , Welt.WEAPON_FIREBALL_LIFETIME , Welt.WEAPON_FIREBALL_CANCATCHTIME));
//
////					if (weapon.equals("bubble")) objekt.weapon = new weapon_Bubble(8.0f, 1.5f, 10000000000L, 500000000L);
////					if (weapon.equals("fireball")) objekt.weapon = new WeaponFireball(15.0f, 5f, 10000000000L, 500000000L);
//					
//					if (monsterWeaponUsage == 0) objekt.weapon_usage = false;
//					if (monsterWeaponUsage == 1) objekt.weapon_usage = true;
//				    //--------------------------------------	
//					
//					
//					//------- special settings for monstertypes
//					
//					// monstertype FLYER: ------------------------------
////					if (objekt.inputSource.getDescription().equals("AI_flyer")) objekt.body.setGravityScale(0f);	// wird in dyn.gameobjekt zentral erledigt
//
//					objectList.add(objekt);
//				}
//			}
//		}
//
//		return objectList;
//	}
////=================================================================================================================
//	
//	
//	
//	
//////------------ liest einen player aus der map --------------------------------
//
//	public static character_goodGuy getPlayer(TiledMap map, int ID, World box2dwelt, Welt world, float originalTildemapPPM) {
//		 
//		RectangleMapObject rectangleMapObject;
//		String layerName = "player";
//		character_goodGuy objekt = null;
//		String playerString = new String();
//		boolean facingRight;
//		
//		String search;
//		String parse;
//		float x;
//		float y;
//		float w;
//		float h;
//		
//		int anz_objectGroups = map.getLayers().getCount();	
//		
//		for (int counter_groups = 0; counter_groups < anz_objectGroups; counter_groups++) {
//		
//			if (map.getLayers().get(counter_groups).getName().equals(layerName)) {
//				
//				MapObjects mapObjects = map.getLayers().get(layerName).getObjects();	
//				int anz_objekte = map.getLayers().get(counter_groups).getObjects().getCount();	
//				
//				for (int counter = 0; counter < anz_objekte; counter++) {
//															
//					//----- player ID -------------------------------------
//					int playerID = 0;
//					search = "playerID";
//					parse = "";
//					
//					if(map.getLayers().get(counter_groups).getObjects().get(counter).getProperties().containsKey(search)) {
//						playerID = Integer.parseInt(map.getLayers().get(counter_groups).getObjects().get(counter).getProperties().get(search, String.class));
//					}
//					
//					
//					if (playerID == ID) {
//					
//					
//						if (playerID == 1) playerString = "player1";
//						if (playerID == 2) playerString = "player2";
//						
//						rectangleMapObject = (RectangleMapObject)map.getLayers().get(layerName).getObjects().get(counter);
//						w = (rectangleMapObject.getRectangle().width) / originalTildemapPPM;
//						h = (rectangleMapObject.getRectangle().height) / originalTildemapPPM;
//						x = Welt.PLAYFIELDOFFSETX + (rectangleMapObject.getRectangle().x / originalTildemapPPM) + (w * 0.5f);
//						y = Welt.PLAYFIELDOFFSETY + ((rectangleMapObject.getRectangle().y) / originalTildemapPPM) + (h * 0.5f);
//
//						
//						
//						//----- FacingRight -------------------------------------
//						
//						int playerFacingRight = Welt.PLAYERDIRECTIONRIGHT;
//						String playerFacingRight_parse = "";
//						
//						if(map.getLayers().get(layerName).getObjects().get(counter).getProperties().containsKey("playerFacingRight")) {
//							playerFacingRight = Integer.parseInt(map.getLayers().get(layerName).getObjects().get(counter).getProperties().get("playerFacingRight", String.class));
//						}
//						
//	
//						if (playerFacingRight == 1) {
//							facingRight = true;
//						}
//						else {
//							facingRight = false;
//						}
//	
//					    //--------------------------------------	
//
//						objekt = new character_goodGuy(playerString, "character", x, y, Welt.PLAYER_MOVE_VELOCITY, facingRight, world.box2dwelt, world, world.bubblr);
//					}
//				}
//			}
//		}
//
//		return objekt;
//	}
//	
//
//	 
//////------------------------------------------------------------------
//	
//	
//	
//	public static String getMapPropertyString(TiledMap map, String propertyName) {
//		
//		String ret = "";									
//		
//		if(map.getProperties().containsKey(propertyName)) ret = map.getProperties().get(propertyName, String.class);
//
//		return ret;
//	}
//	
//	
//	
//	public static Integer getMapPropertyInt(TiledMap map, String propertyName) {
//				
//		return getMapPropertyInt(map, propertyName, 0);
//	}
//	
//	public static Integer getMapPropertyInt(TiledMap map, String propertyName, int defaultValue) {
//		
//		String parse = "";
//		int ret = defaultValue;											// TODO: liefert im zweifel immer null zur�ck... also aufpassen!
//		
//		if (map.getProperties().containsKey(propertyName)) parse = map.getProperties().get(propertyName, String.class);
//		
//		if (!parse.equals("")) ret = Integer.parseInt(parse);
//		
//		return ret;
//	}
//	
//	 
//	
//	public static Float getMapPropertyFloat(TiledMap map, String propertyName) {
//		
//		float ret = 0f;											// TODO: liefert im zweifel immer null zur�ck... also aufpassen!
//		
//		if(map.getProperties().containsKey(propertyName)) ret = Float.parseFloat(map.getProperties().get(propertyName, String.class));
//
//		return ret;
//	}
//	
//	
//	public static Boolean getMapPropertyBoolean(TiledMap map, String propertyName) {
//		
//		boolean ret = false;											// TODO: liefert im zweifel immer null zur�ck... also aufpassen!
//		
//		if(map.getProperties().containsKey(propertyName)) ret = Boolean.parseBoolean(map.getProperties().get(propertyName, String.class));
//
//		return ret;
//	}
//	
//////------------------------------------------------------------------
//
//}
//
//
