package de.philweb.philengine.particles2d;

import com.badlogic.gdx.math.Vector2;

public interface IparticlePosition {

	public Vector2 getPosition();
}
