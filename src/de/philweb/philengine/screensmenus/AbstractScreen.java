
package de.philweb.philengine.screensmenus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.philweb.philengine.assetservice.AssetService;
import de.philweb.philengine.common.Game;
import de.philweb.philengine.common.GamePreferences;
import de.philweb.philengine.common.PeSound;
import de.philweb.philengine.controllers.AbstractController;
import de.philweb.philengine.controllers.MgrControllerConfigManager;
import de.philweb.philengine.drawables.FixedTextureRegionDrawable;
import de.philweb.philengine.listeners.IeventCallback;
import de.philweb.philengine.listeners.PeDialogEvent;
import de.philweb.philengine.maps.MapService;

/**
 * @author andre
 *
 */
public abstract class AbstractScreen {

	protected Game game;
	private GamePreferences prefs;
	public Stage stage;
	public AbstractController inputSource;
	protected MapService _mapService;
	protected MgrControllerConfigManager controllerConfigManager;
	protected MgrMenuNavigationManager menuNavigationManager;
	private AssetService _assetService;	// shall give a shortcut to the assetservice
	protected SpriteBatch batch;
	protected BitmapFont font;
	public boolean soundOn = true;
	float soundVolume;
	
	FixedTextureRegionDrawable fixedTextureRegionDrawableBackground = null;;
	int backgroundPosX, backgroundPosY;
	
	//----- font scale ---------
	public float oldScaleX_big;
	public float oldScaleY_big;
	public float oldScaleX_small;
	public float oldScaleY_small;
	public float oldScaleX_tiny;
	public float oldScaleY_tiny;
	
	//----- font color ---------
	public Color color;
	
// private long lastRender;
// private long nowFPS;
// public int frameCount = 0;
// public int lastFPS = 0;
// public int rendercalls = 0;
// public int rendercallsPerFrame = 0;
// public int lastRenderCalls = 0;

	public AbstractScreen (Game game) {
		this.game = game;
		initialize();
	}

	void initialize () {
		controllerConfigManager = game.getControllerConfigManager();
		inputSource = controllerConfigManager.getMenuController();
		batch = game.getSpritebatch();
		stage = game.getStage();
		stage.clear(); // clear stage completely because its a member of the game.class		
		prefs = game.getPreferences();
		_assetService = game.getAssetService();
		menuNavigationManager = new MgrMenuNavigationManager(game);
		

		color = new Color();
		
		// Receive inputs from stage
		Gdx.input.setInputProcessor(stage);
		
		Gdx.input.setCatchBackKey(true);	// back key abfangen
		
		updateSoundPrefs();
	}
	
	public void backupFontScale() {
		
		//--- save normal scale-values
//		oldScaleX_big = Assets.font_big.getScaleX();
//		oldScaleY_big = Assets.font_big.getScaleY();
//		
//		oldScaleX_small = Assets.font_small.getScaleX();
//		oldScaleY_small = Assets.font_small.getScaleY();
//		
//		oldScaleX_tiny = Assets.font_tiny.getScaleX();
//		oldScaleY_tiny = Assets.font_tiny.getScaleY();
	}
	
	
	public void restoreFontScale() {
		
		//--- reset font scale 
//		Assets.font_big.setScale(oldScaleX_big, oldScaleY_big);
//		Assets.font_small.setScale(oldScaleX_small, oldScaleY_small);
//		Assets.font_tiny.setScale(oldScaleX_tiny, oldScaleY_tiny);
//		Assets.font_tiny.setColor(1f, 1f, 1f, 1f);
//		Assets.font_small.setColor(1f, 1f, 1f, 1f);
//		Assets.font_big.setColor(1f, 1f, 1f, 1f);
//		color.set(1f, 1f, 1f, 1f);
	}
	
	// this is helpfull because game.setScreen is often called with new Screens() that have to be constructed. 
	// this will be called by game-class after setScreen and makes sure that the screen-construction has finished
	public abstract void doAfterSetScreen();
	
	
	/** checks for exiting menuController.
	 * if controller == null -> sends event to game-class => game-class shows controllerAssignScreen -> returns false<br>
	 * returns true if there is a controller  */	
	public boolean checkAssignedMenuController() {
		return checkAssignedMenuController(null);
	}
	
	/** checks for exiting menuController.
	 * if controller == null -> sends event to game-class => game-class shows controllerAssignScreen -> returns false<br>
	 * returns true if there is a controller  <br>
	 * callback: set this if you want to execute code after a new controller has been set */	
	public boolean checkAssignedMenuController(IeventCallback cb) {
		inputSource = game.getControllerConfigManager().getMenuController();
		if (inputSource == null) {
			Gdx.app.log("AbstractScreen", "inputSource -> getMenuController() -> == NULL !!");
//			// game.getPlatformResolver().showToast("PeDialogEvent unassignedControllersEvent");
			PeDialogEvent unassignedControllersEvent = new PeDialogEvent();
			unassignedControllersEvent.setType(PeDialogEvent.Type.unassignedMenuController);
			unassignedControllersEvent.setCallback(cb);
			game.notify(unassignedControllersEvent);
			
			// game soll einen assign screen einschieben
			return false;
		}
		else return true;
	}

	public void updateSoundPrefs () {
		soundOn = game.getPreferences().isSoundEffectsEnabled();
		soundVolume = game.getPreferences().getSoundVolume(); // soundvolume zwischenspeichern
		// soundVolume = game.getMusicManager().getSoundVolume(); // soundvolume zwischenspeichern
	}

	public void update(float delta) {

		if (Gdx.input.isKeyPressed(Keys.PAGE_UP)) game.getCameraManager().setCameraZoomIncrement(0.01f); // ====== TODO : deaktivieren ========================
		if (Gdx.input.isKeyPressed(Keys.PAGE_DOWN)) game.getCameraManager().setCameraZoomIncrement(-0.01f); // ===== TODO : deaktivieren ======================
		

		//---- check standard menu controls -----------
		menuNavigationManager.updateOpenedMenu(delta);	// includes nullpointer check
		
		
		
		// -------- update music ---------------
		// game.musicManager.update(delta);

		// ---- update submenu (z.b. scollpane scrollto -----------
		// if (menuNavigationManager.openedsubMenuTable != null) menuNavigationManager.openedsubMenuTable.update(delta);
		
		// //---------- draw calls check ----------------------------
		// int totalRC = batch.totalRenderCalls;
		// rendercalls = totalRC - lastRenderCalls;
		//
		// //---------- FPS check ----------------------------
		// frameCount ++;
		// nowFPS = System.nanoTime(); // zeit loggen
		//
		// if ((nowFPS - lastRender) >= 1000000000) {
		//
		// lastFPS = frameCount;
		// frameCount = 0;
		//
		// rendercallsPerFrame = rendercalls / lastFPS;
		// lastRenderCalls = batch.totalRenderCalls;
		//
		// // Gdx.app.log("rendercalls", "" + rendercallsPerFrame);
		// Gdx.app.log("lastFPS", "" + lastFPS);
		//
		// lastRender = System.nanoTime();
	// }
		// --------------------------------------------------------------
		
		//---- Simulate the gameWorld:
		if (_mapService != null) _mapService.simulate(delta);
	}
	
	public void present () {	
		
		//----- draw background -------------------------------
		if (fixedTextureRegionDrawableBackground != null) {
			if (!batch.isDrawing()) {
				batch.begin();
					fixedTextureRegionDrawableBackground.draw(batch, backgroundPosX, backgroundPosY);
				batch.end();
			}
			else fixedTextureRegionDrawableBackground.draw(batch, backgroundPosX, backgroundPosY);
		}
		
		//---- render the gameWorld => only if the child-screen calls super.present() !!
		if (_mapService != null) _mapService.render();
	}
		
	
	public void setBackground(FixedTextureRegionDrawable fixedTextureRegionDrawableBackground) {
		this.fixedTextureRegionDrawableBackground = fixedTextureRegionDrawableBackground;
		this.backgroundPosX = (int)(game.getGameConfig().getVIRTUAL_WIDTH_HUD() / 2f) - (int)(fixedTextureRegionDrawableBackground.getW() / 2f);
		this.backgroundPosY = (int)(game.getGameConfig().getVIRTUAL_HEIGHT_HUD() / 2f) - (int)(fixedTextureRegionDrawableBackground.getH() / 2f);
	}
		
//	/** Set stage background. Sets the image (Adds to stage as image)
//	 * 
//	 * @param backgroundTextureRegion */
//	public void setBackgroundTexture (TextureRegion textureBackground, float alpha) {
//		Drawable tBg = new TextureRegionDrawable(textureBackground);
//		Image imgbg = new Image(tBg, Scaling.stretch);
//		imgbg.setFillParent(true);
//		imgbg.setColor(1f, 1f, 1f, alpha);
//		stage.addActor(imgbg);
//
//		// Gdx.app.log("ScreenLog", "SCREEN BACKGROUND SETTED: " + screenName);
//	}

	//does not draw centered !!!
//	public void setBackgroundTexture (Drawable tBg, float alpha) {
//		Image imgbg = new Image(tBg, Scaling.fit);
//		imgbg.setFillParent(true);
//		imgbg.setColor(1f, 1f, 1f, alpha);
//		stage.addActor(imgbg);
//		// Gdx.app.log("ScreenLog", "SCREEN BACKGROUND SETTED: " + screenName);
//	}

	/** Set the back button active for the screen. Sets "Gdx.input.setCatchBackKey(true)" and override the method "keyBackPressed"
	 * to add desired functionality to back button
	 * 
	 * @param isBackButtonActive to use or not to use the back button
	 * @see keyBackPressed */
	public void setBackButtonActive (boolean isBackButtonActive) {
		Gdx.input.setCatchBackKey(true);
		// this.isBackButtonActive = isBackButtonActive;
		// Gdx.app.log("ScreenLog", "CUSTOM BACK BUTTON SETTED");
	}

	/** Override this method to do some function when back button pressed */
	public void keyBackPressed () {
	}

	/** Get the game class */
	public Game getGame () {
		return game;
	}

	/** Set the game class */
	public void setGame (Game game) {
		this.game = game;
	}

// /**
// * Get if back button active
// * */
// public boolean isBackButtonActive() {
// return isBackButtonActive;
// }

	/** Get stage of the screen */
	public Stage getStage () {
		return stage;
	}

	public AssetService getAssetService() {
		return _assetService;
	}
	public MapService getMapService() {
		if (_mapService == null) {
			_mapService = new MapService(game, this);
		}
		return _mapService;
	}
	public GamePreferences getPrefs() {
		return prefs;
	}
//	public IassetService getAssets() {
//		return _assetService.getAssets();
//	}
	
// /** @see ApplicationListener#resize(int, int) */
// public void resize(int width, int height) {
// //--- nothing more to do here... evrything is done in Game.java
//
// viewportHUD = game.getViewportHUD();
//
// if (viewportHUD != null) {
//
// stage.setViewport(game.getVirtualWidthHUD(), game.getVirtualHeightHUD(), true, viewportHUD.x, viewportHUD.y, viewportHUD.width,
// viewportHUD.height);
// }
// }

	public void show () {
	}

	public void hide () {
	}

	public void pause () {
// Gdx.app.log("abstractscreen", "paused");
// bubblr.musicManager.pause(); //-------- pause music ---------------
	}

	public void resume () {
// Gdx.app.log("abstractscreen", "resumed");
// bubblr.musicManager.play();
	}

	public void dispose () {
		// stage.dispose(); // dispose ist unsinn da die stage an game.java haengt
	}

	public void playSound (PeSound sound) {
		if (sound != null && soundOn == true) sound.play(soundVolume);
	}
}
