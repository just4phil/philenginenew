package de.philweb.philengine.screensmenus;

import com.badlogic.gdx.scenes.scene2d.Stage;

import de.philweb.philengine.common.Game;
import de.philweb.philengine.controllers.MgrControllerConfigManager;
import de.philweb.philengine.drawables.FixedNinePatchDrawable;
import de.philweb.philengine.drawables.FixedTextureRegionDrawable;

public abstract class AbstractSubMenu extends PhilengineTable {
 
	public static final float ALPHA = 0.33f;
	protected final static int ICONSIZE2 = 50; 	// ist die originale groesser der menuicons bei 800x480
	protected final static int backgroundWidth = 600; 	// ist die originale groesser der menuicons bei 800x480
	protected final static int backgroundHeight = 105;
	protected Game game;
	protected AbstractScreen screen;
	protected MgrMenuNavigationManager menuNavigationManager;
	protected MgrControllerConfigManager controllerConfigManager;
	protected Stage stage;
	protected FixedTextureRegionDrawable menuMarker = null;;
	protected int tablePadding = 3;
	PhilengineImageButton btnClose;
	public PhilengineImageButton openedNavigationButton = null;	// hier wird der button gespeichert, der nach dem oeffnen eines submenus aktiviert sein soll (nicht transparent)
	
	public boolean menuIsActive = false;
	public PhilengineImageButton parentButton;
	protected AbstractSubMenu CallerMenu = null;	// ist das menu, das ein anderes menu geoeffnet hat (wichtig fuer "back")
	public int menuID = -1;
	
	/**
	 * this constructor is only for empty submenuScreen (like controllerConfig)
	 * @author andre
	 */
	public AbstractSubMenu(Game game, AbstractScreen screen) {
		super(game.getStage());
		this.game = game;
		this.stage = game.getStage();
		this.screen = screen;
		this.menuNavigationManager = screen.menuNavigationManager;
		this.controllerConfigManager = game.getControllerConfigManager();
	}
	
	public AbstractSubMenu(Game game, Stage stage, MgrMenuNavigationManager menuNavigationManager, AbstractScreen screen,
			float posX, float posY, float outerPosX, float outerPosY, PhilengineImageButton parentButton,
			FixedNinePatchDrawable background, FixedTextureRegionDrawable menuMarker) {
		
		super(posX, posY, outerPosX, outerPosY, stage);

		this.game = game;
		this.stage = stage;
		this.menuNavigationManager = menuNavigationManager;
		this.controllerConfigManager = game.getControllerConfigManager();
		this.screen = screen;
		this.parentButton = parentButton;
				
		if (background != null) {
			setBackground(background);
			enableShadow(10, -10, 0.4f);
		}
		
		if (menuMarker != null) setNavigationMarker(menuMarker);
	}
	
//	public void mainMenuToFront() {
//		
//		CoolMainMenu coolMainMenu = (CoolMainMenu)screen;
//		coolMainMenu.mainMenu.toFront();
//	}
	
	public AbstractSubMenu setNavigationMarker(FixedTextureRegionDrawable marker) {
		menuMarker = marker;
		return this;
	}
	
	public AbstractSubMenu setOpenedNavigationButton(PhilengineImageButton button) {
		openedNavigationButton = button;
		return this;
	}
	
	public AbstractSubMenu setCallerMenu(AbstractSubMenu CallerMenu) {
		this.CallerMenu = CallerMenu;
		return this;
	}
	
	public AbstractSubMenu setActive(boolean active) {
		this.menuIsActive = active;
		return this;
	}
	
	
//	void addCloseButton() {
//		
//		btnClose = new PhilengineImageButton(Assets.closeMenu, this);
//		btnClose.addListener(new ChangeListener() {
//			@Override
//			public void changed(ChangeEvent event, Actor actor) {	AbstractSubMenu2.this.menuNavigationManager.closeSubMenu(); }
//		});
//		add(btnClose);
//		registerImageButton(btnClose);
//	}
	
	@Override
	public void update(float delta) {
		if (hasBeenMovedIn && screen.inputSource != null && controllerConfigManager.autoCheckNavCommands) {
			if (screen.inputSource.getInputForCommand(controllerConfigManager.commandMenuRight) == 1f) menuNavigationManager.navigateRight();;
			if (screen.inputSource.getInputForCommand(controllerConfigManager.commandMenuLeft) == 1f) menuNavigationManager.navigateLeft();;
			if (screen.inputSource.getInputForCommand(controllerConfigManager.commandMenuUp) == 1f) menuNavigationManager.navigateUp();
			if (screen.inputSource.getInputForCommand(controllerConfigManager.commandMenuDown) == 1f) menuNavigationManager.navigateDown();
			if (screen.inputSource.getInputForCommand(controllerConfigManager.commandEnterMenu) == 1f) menuNavigationManager.enterMenuEntry();
		}
	}

	//--- normalfall --------
	public void closeMenu() {
		if (CallerMenu != null) {
			menuNavigationManager.openMenu(CallerMenu, null);
		}
//		else menuNavigationManager.closeSubMenu();
	}

	//--- fuer den fall: scrollersubmenu wird geschlossen und infosubmenu geoeffnet (dann braucht info menu caller mainmenu)
	public void closeMenu(AbstractSubMenu caller) {
		if (CallerMenu != null) {
			menuNavigationManager.openMenu(CallerMenu, caller);	// hiermit wird callermenu für das gecallte menu gesetzt ;)
		}
//		else menuNavigationManager.closeSubMenu();
	}
	
	public AbstractSubMenu enableShadow(int offsetX, int offsetY, float alpha) {
		FixedNinePatchDrawable background;
		background = (FixedNinePatchDrawable)getBackground();
		background.enableShadow(offsetX, offsetY, alpha);
		return this;
	}
	
	public void disableShadow() {
		FixedNinePatchDrawable background;
		background = (FixedNinePatchDrawable)getBackground();
		background.disableShadow();
	}
	
	public MgrMenuNavigationManager getMenuNavigationManager() {
		 return menuNavigationManager;
	}
	
	// reihenfolge muss exakt wie index-reihenfolge der buttons sein
	public abstract void enterMenu(int buttonID, Object menuObject);
}
