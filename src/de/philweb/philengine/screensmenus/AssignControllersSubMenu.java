//
//package de.philweb.philengine.screensmenus;
//
//import com.badlogic.gdx.controllers.Controller;
//import com.badlogic.gdx.scenes.scene2d.Stage;
//import com.badlogic.gdx.scenes.scene2d.ui.Label;
//import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
//
//import de.philweb.philengine.common.Game;
//import de.philweb.philengine.controllers.AbstractController;
//import de.philweb.philengine.controllers.ControllerNone;
//import de.philweb.philengine.controllers.MgrControllerConfigManager;
//import de.philweb.philengine.drawables.FixedNinePatchDrawable;
//import de.philweb.philengine.drawables.FixedTextureRegionDrawable;
//
//public abstract class AssignControllersSubMenu extends AbstractSubMenu {
//
//	public final static int TYPENONE = 0;
//	public final static int TYPEAXIS = 1;
//	public final static int TYPEBUTTON = 2;
//	public final static int TYPEKEY = 3;
//
//	private final static int INITIALBUTTONINDEX = -999;
//		
//	AbstractSubMenu gScreen;
//
//	int counter;
//	boolean isProcessStarted = false;
//	int tank = 0;
//	AbstractController gameController = null;
//	Controller controller;
//	boolean readyToAssign = false; // if button is axis then wait until back to 0 !
//	long lastAxisValue;
//
//	int type;
//	int index;
//	float value;
//	LabelStyle labelStyle;
//
//	public AssignControllersSubMenu (Game game, Stage stage, MgrMenuNavigationManager menuNavigationManager, AbstractSubMenu screen,
//		int posX, int posY, FixedNinePatchDrawable fixedNinepatch, FixedTextureRegionDrawable menuMarker) {
//
//		super(game, stage, menuNavigationManager, screen, posX, posY, posX, 500, null, fixedNinepatch, menuMarker);
//
//		gScreen = screen;
//		menuID = 1;
//		setWidth(500);
//		setHeight(350);
//
//		labelStyle = new LabelStyle();
//		labelStyle.font = Assets.font_tiny;
//		labelStyle.font.setScale(game.getAssetService().getResolutionFontScale() * 1.0f);
//		
//		setupTable();
//		
//		navigationIsActiveEntry = 0;
//		menuNavigationManager.registerMenu(this);
//
//		// do after moveIn ---
////		// ---- determine unassigned controllers/tanks ----------
////		determineUnassignedTanks();
////
////		// ---- set alpha of buttons if game is not/purchased
////		tweakIcons();
////
////		// ---- reset controllerListener Values ----
////		initializeControllerValues();
////
////		// ---- check for unassigned controllers ----
////		assignProcedure();
//	}
//
//	void initializeControllerValues () {
//		controller = null;
//		index = INITIALBUTTONINDEX;
//		value = 0f;
//		type = TYPENONE;
//		gameController = null;
//		readyToAssign = false;
//	}
//	
//	// ---- determine unassigned controllers/tanks ----------
//	public void determineUnassignedTanks () {
//
//		arrayUnassignedTanks = new int[5];
//		counter = 0;
//
//		if (gScreen.gameWorld.tank1 == null)
//			arrayUnassignedTanks[0] = 2; // 2
//		else if (
//				gScreen.gameWorld.tank1.getController() == null || 
//					gScreen.gameWorld.tank1.getController().getDescription().equals(MgrControllerConfigManager.ControllerNONE) || 
//			gScreen.gameWorld.tank1.getController().controllerDisconnected ||
//			game.getControllerConfigManager().getMenuController() == null) {		//---- check menu controller too!!
//			arrayUnassignedTanks[0] = 1;
//			counter++;
//		} else
//			arrayUnassignedTanks[0] = 0;
//
//		if (gScreen.gameWorld.tank2 == null)
//			arrayUnassignedTanks[1] = 2;
//		else if (
//				gScreen.gameWorld.tank2.getController() == null || 
//					gScreen.gameWorld.tank2.getController().getDescription().equals(MgrControllerConfigManager.ControllerNONE) || 
//			gScreen.gameWorld.tank2.getController().controllerDisconnected) {
//			arrayUnassignedTanks[1] = 1;
//			counter++;
//		} else
//			arrayUnassignedTanks[1] = 0;
//
//		if (gScreen.gameWorld.tank3 == null)
//			arrayUnassignedTanks[2] = 2;
//		else if (
//				gScreen.gameWorld.tank3.getController() == null || 
//					gScreen.gameWorld.tank3.getController().getDescription().equals(MgrControllerConfigManager.ControllerNONE) || 
//			gScreen.gameWorld.tank3.getController().controllerDisconnected) {
//			arrayUnassignedTanks[2] = 1;
//			counter++;
//		} else
//			arrayUnassignedTanks[2] = 0;
//
//		if (gScreen.gameWorld.tank4 == null)
//			arrayUnassignedTanks[3] = 2;
//		else if (
//				gScreen.gameWorld.tank4.getController() == null || 
//					gScreen.gameWorld.tank4.getController().getDescription().equals(MgrControllerConfigManager.ControllerNONE) || 
//			gScreen.gameWorld.tank4.getController().controllerDisconnected) {
//			arrayUnassignedTanks[3] = 1;
//			counter++;
//		} else
//			arrayUnassignedTanks[3] = 0;
//		arrayUnassignedTanks[4] = counter;
//	}
//		
//	// ---- set alpha of buttons 
//	public void tweakIcons () {
//		if (arrayUnassignedTanks[0] == 2)
//			tank1.setFixedAlpha(0.1f);
//		else if (arrayUnassignedTanks[0] == 1)
//			tank1.setFixedAlpha(ALPHA);
//		else
//			tank1.setFixedAlpha(0);
//
//		if (arrayUnassignedTanks[1] == 2)
//			tank2.setFixedAlpha(0.1f);
//		else if (arrayUnassignedTanks[1] == 1)
//			tank2.setFixedAlpha(ALPHA);
//		else
//			tank2.setFixedAlpha(0);
//
//		if (arrayUnassignedTanks[2] == 2)
//			tank3.setFixedAlpha(0.1f);
//		else if (arrayUnassignedTanks[2] == 1)
//			tank3.setFixedAlpha(ALPHA);
//		else
//			tank3.setFixedAlpha(0);
//
//		if (arrayUnassignedTanks[3] == 2)
//			tank4.setFixedAlpha(0.1f);
//		else if (arrayUnassignedTanks[3] == 1)
//			tank4.setFixedAlpha(ALPHA);
//		else
//			tank4.setFixedAlpha(0);
//	}
//
//	public boolean buttonAndAxis (Controller controller, int type, int index, float value) {
//		this.controller = controller;
//		this.index = index;
//		this.value = value;
//		this.type = type;
//
//		if (type != TYPEKEY) { // ---- for real game controllers ---------
//
//			this.gameController = game.getControllerConfigManager().getInputController(controller);
//
//			if (type == TYPEBUTTON) {
//				readyToAssign = isConfigOK(gameController);
//			} else if (type == TYPEAXIS) {
//				if (Math.abs(value) < GameScreen.MINCONTROLLERAXISVALUE + 0.02f) readyToAssign = isConfigOK(gameController);
//			}
//		}
//
//		else if (type == TYPEKEY) { // ---- for key controllers like amazon fire tv REMOTE (controller == null) ---------
//
//			if (game.getAppStore() == Game.APPSTORE_AMAZON) {
//
//				this.gameController = game.getControllerConfigManager().getController(
//					MgrControllerConfigManager.ControllerFireTVRemote, MgrControllerConfigManager.ControllerFireTVRemote, 1);
//				if (this.gameController != null) readyToAssign = true;
//			}
//			
//			if (game.getAppStore() == Game.APPSTORE_DESKTOP) {
//
//				this.gameController = game.getControllerConfigManager().getController(
//					MgrControllerConfigManager.ControllerDesktopPL1, MgrControllerConfigManager.ControllerDesktopPL1, 1);
//				if (this.gameController != null) readyToAssign = true;
//			}
//		}
//
//		return readyToAssign;
//	}
//
//	boolean isConfigOK (AbstractController gameController) {
//
//		if (gameController.controllerConfig == null) {
//			initializeControllerValues();
//			game.getPlatformResolver().showToast("Only Amazon and OUYA controllers are supported at the moment!", false);
//			return false;
//		}
//		return true;
//	}
//
//	// --- check which player is next with an unassigned controller
//	public void assignProcedure () {
//		counter = arrayUnassignedTanks[4];
//		if (counter > 0) {
//			for (int i = 0; i < 4; i++) {
//				if (arrayUnassignedTanks[i] == 1) {
//					tank = i + 1;
//					break;
//				}
//			}
//		} else {
//			gScreen.assignControllersIsInProgress = false; // reset the lock in gamescreen
//			menuNavigationManager.openMenu(gScreen.inGameMenu, gScreen.inGameMenu);
//		}
//	}
//
//	@Override
//	public void update (float delta) {
//
//		if (tank > 0) {
//
//			if (isProcessStarted == false) {
//				menuNavigationManager.activateNavigationEntry(tank - 1);
//				game.getPlatformResolver().showToast("press a button on controller of tank " + tank, false);
//				isProcessStarted = true;
//			} else {
//				if (readyToAssign) {
//
//					switch (tank) {
//					case 1:
//						game.getControllerConfigManager().setInputController(tank, gameController);
//						gScreen.gameWorld.tank1.setController(game.getControllerConfigManager().getInputController(tank,
//							game.getCameraManager().getHUDCamera()));
//						game.getControllerConfigManager().setMenuController(game.getControllerConfigManager().getInputController(tank,
//							game.getCameraManager().getHUDCamera()));
//						gScreen.inputSource = game.getControllerConfigManager().getMenuController();
//						break;
//					case 2:
//						game.getControllerConfigManager().setInputController(tank, gameController);
//						gScreen.gameWorld.tank2.setController(game.getControllerConfigManager().getInputController(tank,
//							game.getCameraManager().getHUDCamera()));
//						break;
//					case 3:
//						game.getControllerConfigManager().setInputController(tank, gameController);
//						gScreen.gameWorld.tank3.setController(game.getControllerConfigManager().getInputController(tank,
//							game.getCameraManager().getHUDCamera()));
//						break;
//					case 4:
//						game.getControllerConfigManager().setInputController(tank, gameController);
//						gScreen.gameWorld.tank4.setController(game.getControllerConfigManager().getInputController(tank,
//							game.getCameraManager().getHUDCamera()));
//						break;
//					}
//
//					game.getPlatformResolver().showToast("ok, got it!", false);
//
//					tank = 0;
//					isProcessStarted = false;
//					
//					startProcess();
//				}
//			}
//		}
//	}
//
//	// -------------------------------
//	void reAssignAllTanks () {
//		game.getPlatformResolver().showToast("re-assign all controllers...", false);
//		tank = 0;
//		isProcessStarted = false;
//		gScreen.gameWorld.tank1.setController(new ControllerNone(game, MgrControllerConfigManager.ControllerNONE));
//		gScreen.gameWorld.tank2.setController(new ControllerNone(game, MgrControllerConfigManager.ControllerNONE));
//		gScreen.gameWorld.tank3.setController(new ControllerNone(game, MgrControllerConfigManager.ControllerNONE));
//		gScreen.gameWorld.tank4.setController(new ControllerNone(game, MgrControllerConfigManager.ControllerNONE));
//		
//		startProcess();
//	}
//
//	@Override
//	public void doThingsAfterMoveIn() {
//		
//		//---- inform google analytics
//		game.getPlatformResolver().sendTrackerScreen("AssignControllers");
//		
//		startProcess();
//	}
//		
//	public void startProcess() {
//		determineUnassignedTanks();	// ---- determine unassigned controllers/tanks ----------
//		tweakIcons();				// ---- set alpha of buttons if game is not/purchased
//		initializeControllerValues();// ---- reset controllerListener Values ----
//		assignProcedure();			// ---- check for unassigned controllers ----
//	}
//	
//	// ----------------------------------------
//	public abstract void setupTable();
//}
