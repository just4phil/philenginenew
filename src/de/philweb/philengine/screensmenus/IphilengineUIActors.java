package de.philweb.philengine.screensmenus;

import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public interface IphilengineUIActors {

	public void setNavigationIsActive(boolean isActive);
	public boolean getNavigationIsActive();
	
	public int getNavigationIndex();
	
	public void addEnterMenuListener(int enterMenuEntry, Object enterMenuObject, ChangeListener listener);
	public void addEnterMenuListener(int enterMenuEntry, Object enterMenuObject);
	public void addEnterMenuListener(int enterMenuEntry);
	
	public int getEnterMenuEntry();
	public Object getEnterMenuObject();}
