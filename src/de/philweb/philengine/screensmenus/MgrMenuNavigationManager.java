package de.philweb.philengine.screensmenus;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.Actor;

import de.philweb.philengine.common.Game;


public class MgrMenuNavigationManager {

	Game game;
//	AbstractScreen screen;
	public static int BUTTON = 1;
	public static int SELECTBOX = 2;
	public static int SLIDER = 3;
	public static int BUTTONGAME = 4;
	public static int IMAGEBUTTON = 5;
	
	public int navigationEntryCounter;
	int backupNavigationEntryCounter;
	public int navigationIsActiveEntry;
	int backupNavigationIsActiveEntry = -1;
//	public ArrayList<Actor> philengineMenuActors;
	public ArrayList<IphilengineUIActors> philengineMenuActors;
	public ArrayList<Integer> philengineMenuActorsTypes;
	public ArrayList<Actor> backupPhilengineMenuActors;
	public ArrayList<Integer> backupPhilengineMenuActorsTypes;
	private boolean changedNavigationItem;
	boolean selectBoxIsOpened;
//	PhilengineSelectBox openedSelectBox;
	
	public boolean subMenuTableIsOpened = false;
	public AbstractSubMenu openedsubMenuTable = null;
	
	//--- neu fuer hildenbrand-menus -----------
//	public ArrayList<AbstractSubMenu> MenuArray;
	public int activeMenuID = -1;	// -1 = null // 0 = mainmenu
	
	
	public MgrMenuNavigationManager(Game game) {
		
		this.game = game;
//		philengineMenuActors = new ArrayList<Actor>();
		philengineMenuActors = new ArrayList<IphilengineUIActors>();
		philengineMenuActorsTypes = new ArrayList<Integer>();
		navigationEntryCounter = 0;
		navigationIsActiveEntry = -1;
		backupNavigationEntryCounter = -1;
		changedNavigationItem = false;
		selectBoxIsOpened = false;
//		openedSelectBox = null;
		subMenuTableIsOpened = false;
		openedsubMenuTable = null;
		
		//--- neu fuer hildenbrand-menus -----------
//		MenuArray = new ArrayList<AbstractSubMenu>();
	}
	
	
//	public void registerMenu(AbstractSubMenu menu) {
//		
//		MenuArray.add(menu);
//	}
	

	public void deactivateNavigationEntry() {
		
		if (navigationIsActiveEntry > -1) {
			
			philengineMenuActors.get(navigationIsActiveEntry).setNavigationIsActive(false);
			
//			if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == IMAGEBUTTON) getImageButton(navigationIsActiveEntry).navigationIsActive = false;
//			if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SLIDER) getSlider(navigationIsActiveEntry).navigationIsActive = false;
//			if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SELECTBOX) getSelectBox(navigationIsActiveEntry).navigationIsActive = false;
		}
	}
	
	
	
	public void activateNavigationEntry(int index) {
		
		//----- zuerst den bisherigen Actor deaktivieren -------------
		deactivateNavigationEntry();
		
		//----- counter verstellen -------------
		if (index > navigationEntryCounter - 1) { 
			index = navigationEntryCounter - 1; 
		}
//		else index = navigationIsActiveEntry;
		else navigationIsActiveEntry	= index;
		
		//----- selektierten Actor aktivieren -------------
//		activateNavEntryWithoutDeactivate(index);
		
		philengineMenuActors.get(index).setNavigationIsActive(true);	// TODO: TESTEN ---------------------
		
//		if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == IMAGEBUTTON) getImageButton(index).navigationIsActive = true;
//		if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SELECTBOX) getSelectBox(index).navigationIsActive = true;
//		if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SLIDER) getSlider(index).navigationIsActive = true;

		//----- flag setzen, dass neuer auswahl getroffen wurde -------------
		changedNavigationItem = true;	// used in the menuscreen to maybe update automatic scrolling of a scrollpane
	}
	
	
	public void activateNextNavigationEntry() {
		
		if (selectBoxIsOpened == true) {
			
			activateNextSelectBoxEntry();
		}
		else {
			deactivateNavigationEntry();
			
			navigationIsActiveEntry ++;
			if (navigationIsActiveEntry > navigationEntryCounter - 1) navigationIsActiveEntry = 0;
			
			//----- selektierten Actor aktivieren -------------
			activateNavEntryWithoutDeactivate(navigationIsActiveEntry);
			changedNavigationItem = true;	// used in the menuscreen to maybe update automatic scrolling of a scrollpane
		}
	}
	
	
	public void activatePreviousNavigationEntry() {
		
		if (selectBoxIsOpened == true) {
			
			activatePreviousSelectBoxEntry();
		}
		else {
			if (navigationIsActiveEntry < 0) navigationIsActiveEntry = 0;
			
			deactivateNavigationEntry();
			
			navigationIsActiveEntry --;
			if (navigationIsActiveEntry < 0) navigationIsActiveEntry = navigationEntryCounter - 1;
			
			//----- selektierten Actor aktivieren -------------
			activateNavEntryWithoutDeactivate(navigationIsActiveEntry);
			changedNavigationItem = true;	// used in the menuscreen to maybe update automatic scrolling of a scrollpane
		}
	}
	
	
	
	public void navigateUp() {
		
		if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SLIDER &&	getSlider(navigationIsActiveEntry).isActivated == true) {
			
//			getSlider(navigationIsActiveEntry).incrementValue();	// wenn slider aktiv ist, soll nur rechts/links funktionieren
		}
		else {
			activateNextNavigationEntry();
		}
	}
	
	public void navigateDown() {
		
		if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SLIDER &&	getSlider(navigationIsActiveEntry).isActivated == true) {
			
//			getSlider(navigationIsActiveEntry).decrementValue();	// wenn slider aktiv ist, soll nur rechts/links funktionieren
		}
		else {
			activatePreviousNavigationEntry();
		}
	}
	
	public void navigateRight() {
		
		if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SLIDER &&	getSlider(navigationIsActiveEntry).isActivated == true) {
			
			getSlider(navigationIsActiveEntry).incrementValue();
		}
		else {
			activateNextNavigationEntry();
		}
	}
	
	public void navigateLeft() {
		
		if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SLIDER &&	getSlider(navigationIsActiveEntry).isActivated == true) {
			
			getSlider(navigationIsActiveEntry).decrementValue();
		}
		else {
			activatePreviousNavigationEntry();
		}
	}
	
	
	
	public void activateNextSelectBoxEntry() {
		
		navigationIsActiveEntry ++;
		if (navigationIsActiveEntry > navigationEntryCounter - 1) navigationIsActiveEntry = 0;
	
		changedNavigationItem = true;	// used in the menuscreen to maybe update automatic scrolling of a scrollpane
	}
	
	
	public void activatePreviousSelectBoxEntry() {
				
		navigationIsActiveEntry --;
		if (navigationIsActiveEntry < 0) navigationIsActiveEntry = navigationEntryCounter - 1;
		
		changedNavigationItem = true;	// used in the menuscreen to maybe update automatic scrolling of a scrollpane
	}
	
	
//	public void enterActiveMenuEntry() {
//		getOpenedMenu().enterMenu(getActiveMenuActor().getEnterMenuEntry(), null);
//	}
	
	public void enterMenuEntry() {
		
		if (selectBoxIsOpened == false) {
		
			if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == IMAGEBUTTON)	getImageButton(navigationIsActiveEntry).toggle();
			if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SLIDER) {
				
				if (getSlider(navigationIsActiveEntry).isActivated == true) {
					getSlider(navigationIsActiveEntry).isActivated = false;
				}
				else getSlider(navigationIsActiveEntry).isActivated = true;
			}
			
//			if (philengineMenuActorsTypes.get(navigationIsActiveEntry) == SELECTBOX) {
//				
//				getSelectBox(navigationIsActiveEntry).open();
//				toggleNavigation();
//			}
		}
		else {
//			getSelectBox(backupNavigationIsActiveEntry).select(navigationIsActiveEntry);
//			getSelectBox(backupNavigationIsActiveEntry).close();
			toggleNavigation();
		}
	}
	
	
	public void toggleNavigation() {
		
//		if (selectBoxIsOpened == false) {
//			selectBoxIsOpened = true;
//			openedSelectBox = getSelectBox(navigationIsActiveEntry);
//			backupNavigationIsActiveEntry = navigationIsActiveEntry;
//			navigationIsActiveEntry = openedSelectBox.getSelectionIndex();
//			backupNavigationEntryCounter = navigationEntryCounter;
//			navigationEntryCounter = openedSelectBox.listSize;
//		}
//		else {
//			selectBoxIsOpened = false;
//			openedSelectBox = null;
//			navigationIsActiveEntry = backupNavigationIsActiveEntry;
//			backupNavigationIsActiveEntry = -1;
//			navigationEntryCounter = backupNavigationEntryCounter;
//			backupNavigationEntryCounter = -1;
//		}
	}
	
	public void updateOpenedMenu(float deltaTime) {
		if (openedsubMenuTable != null) openedsubMenuTable.update(deltaTime);;
	}
	
	public AbstractSubMenu getOpenedMenu() {
		return openedsubMenuTable;
	}
		
	public void openIngameMenu(AbstractSubMenu menu) {
						
		openedsubMenuTable = menu;
		openedsubMenuTable.setActive(true);
		activeMenuID = openedsubMenuTable.menuID;
		deactivateNavigationEntry();
		navigationIsActiveEntry = openedsubMenuTable.navigationIsActiveEntry;
		activateNavEntryWithoutDeactivate(openedsubMenuTable.navigationIsActiveEntry);
		openedsubMenuTable.moveIn();
	}
	
	public void openMenu(AbstractSubMenu menu, AbstractSubMenu callerMenu) {
			
		// wird scheinbar nicht verwendet ??? (sollte dafuer sorgen dass ein menu geschlossen wird, wenn es geoffnet werden soll obwohl es bereits offen ist)
//		if (openedsubMenuTable == menu) {
//			
//			closeSubMenu();
//			return;
//		}
		
		deactivateNavigationEntry();
		
		if (activeMenuID > 0) {
			
			openedsubMenuTable.setActive(false);
			openedsubMenuTable.moveOut();
			subMenuTableIsOpened = false;
		}
		if (activeMenuID == 0) {
			
			openedsubMenuTable.setActive(false);
		}
				
		openedsubMenuTable = menu;
		if (openedsubMenuTable.menuID > 0) subMenuTableIsOpened = true; 	// dient dazu, dass bei exit erstmal ein offenes submenu geschlossen werden kann
		openedsubMenuTable.setActive(true);
		openedsubMenuTable.setCallerMenu(callerMenu);
		openedsubMenuTable.moveIn();
		activeMenuID = openedsubMenuTable.menuID;
		
		CopyMenuItems(menu);	// copy menuinhalte aus dem neuen menu in den menunavigationmanager
//		activateNavEntryWithoutDeactivate(menu.navigationIsActiveEntry);
		activateNavigationEntry(menu.navigationIsActiveEntry);
	}
	
	// copy menuinhalte aus dem neuen menu in den menunavigationmanager
	public void CopyMenuItems(AbstractSubMenu menu) {
		
		philengineMenuActors = menu.philengineMenuActors;
		philengineMenuActorsTypes = menu.philengineMenuActorsTypes;
		navigationIsActiveEntry = menu.navigationIsActiveEntry;
		navigationEntryCounter = menu.navigationEntryCounter;
	}
	
	// derzeit nicht mehr verwendet
//	public void closeSubMenu() {
//
//		if (subMenuTableIsOpened == true) {
//			subMenuTableIsOpened = false;
//			
//			deactivateNavigationEntry();
//			openedsubMenuTable.moveOut();
//			openedsubMenuTable.setActive(false);
//			
//			openedsubMenuTable = null;
//			navigationIsActiveEntry = backupNavigationIsActiveEntry;
//			backupNavigationIsActiveEntry = -1;
//			navigationEntryCounter = backupNavigationEntryCounter;
//			backupNavigationEntryCounter = -1;
//			philengineMenuActors = backupPhilengineMenuActors;
//			philengineMenuActorsTypes = backupPhilengineMenuActorsTypes;
//			
//			activateNavEntryWithoutDeactivate(navigationIsActiveEntry);
//		}
//	}
	 
	
	
	public void activateNavEntryWithoutDeactivate(int index) {
		
		//----- selektierten Actor aktivieren -------------
		philengineMenuActors.get(index).setNavigationIsActive(true);
		
		
//		if (philengineMenuActorsTypes.get(index) == IMAGEBUTTON) 	{
//			getImageButton(index).navigationIsActive = true;
//		}
//		if (philengineMenuActorsTypes.get(index) == SELECTBOX) 		{
//			getSelectBox(index).navigationIsActive = true;
//		}
//		if (philengineMenuActorsTypes.get(index) == SLIDER) 		{
//			getSlider(index).navigationIsActive = true;
//		}
	}
	
	
	public boolean isNavigationItemChanged() {	// used in the menuscreen to maybe update automatic scrolling of a scrollpane
	
		if (changedNavigationItem == true) {
			changedNavigationItem = false;		// reset
			return true;
		}
		else {
			return false;
		}	
	}
	
	
	public void clearNavigation() {
		
//		philengineMenuActors = new ArrayList<Actor>();
		philengineMenuActors = new ArrayList<IphilengineUIActors>();
		philengineMenuActorsTypes = new ArrayList<Integer>();
		navigationEntryCounter = 0;
		navigationIsActiveEntry = -1;
	}
	
	public PhilengineImageButton getImageButton(int navigationIsActiveEntry) {
		if (navigationIsActiveEntry == -1) return null;
		else return (PhilengineImageButton) philengineMenuActors.get(navigationIsActiveEntry);
	}

//	public PhilengineSelectBox getSelectBox(int navigationIsActiveEntry) {
//		if (navigationIsActiveEntry == -1) return null;
//		else return (PhilengineSelectBox) philengineMenuActors.get(navigationIsActiveEntry);
//	}
	
	public PhilengineSlider getSlider(int navigationIsActiveEntry) {
		if (navigationIsActiveEntry == -1) return null;
		else return (PhilengineSlider)philengineMenuActors.get(navigationIsActiveEntry);
	}
	
	public IphilengineUIActors getActiveMenuActor() {
		return (IphilengineUIActors)philengineMenuActors.get(navigationIsActiveEntry);
	}
//	public void closeOpenSubMenu() {
//		openedsubMenuTable.closeMenu();
//	}
	
}