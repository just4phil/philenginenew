package de.philweb.philengine.screensmenus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import de.philweb.philengine.common.Game;

public class PhilengineImageButton extends ImageButton implements IphilengineUIActors {
 
	Game game;
	AbstractSubMenu parentMenu = null;
	protected int navigationIndex;
	public boolean navigationIsActive;
//	public boolean navigationHasBeenOpened;
	Color color;
	float oldRed;
	float oldGreen;
	float oldBlue;
	float oldAlpha;
	float oldScaleX;
	float oldScaleY;
	String description = "";
	
//	FixedTextureRegionDrawable menuMarker;
//	FixedNinePatchDrawable ninePatchDrawable;
//	NinePatchDrawable ninePatchDrawableBackground;
	NinePatchDrawable navigationMarkerNinepatch = null;
	private int levelNo = 0;	//---- holds level number or gamestate id
	public float alpha = 1f;
	final static float ALPHAVELOCITY = 0.5f;
	float fixedAlpha = 0;		// ---- fixedAlpha wird von optionsbuttons gesetzt (wenn deaktiv)
	private int enterMenuEntry;		// used to store the entry for enterMenu
	private Object enterMenuObject;	// used to store the entry for enterMenu
	
	public PhilengineImageButton(Game game, Drawable drawable, AbstractSubMenu menu) {
		super(drawable);
		initialize(game, menu);
	}

	public PhilengineImageButton(Game game, Drawable drawable, String description, AbstractSubMenu menu) {
		super(drawable);
		initialize(game, menu);
		this.description = description;
	}
	
	void initialize(Game game, AbstractSubMenu menu) {
		this.game = game;
		this.parentMenu = menu;
		navigationIndex = -1;
		navigationIsActive = false;
//		navigationHasBeenOpened = false;
//		ninePatchDrawableBackground = new NinePatchDrawable(Assets.tableBackground9p);
//		menuMarker = marker;
	}
	
	//---- for rectangles (levelselector)
	public void setNavigationMarkerNinepatch(NinePatchDrawable ninepatch) {
		navigationMarkerNinepatch = ninepatch;
	}
	
	public String getDescription() {
		return description;
	}
	
	public int getLevelNo() {
		return levelNo;
	}
		
	//---- set level number or gamestate id
	public void setLevelNo(int no) {
		levelNo = no;
	}
	
	@Override
	public void addEnterMenuListener(int enterMenuEntry) {
		addEnterMenuListener(enterMenuEntry, null);
	}
	
	@Override
	public void addEnterMenuListener(int enterMenuEntry, Object enterMenuObject) {
		this.enterMenuEntry = enterMenuEntry;
		this.enterMenuObject = enterMenuObject;
		addEnterMenuListener(
				enterMenuEntry, 
				null, 
				new ChangeListener() {
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						parentMenu.enterMenu(getEnterMenuEntry(), getEnterMenuObject());
					}
				});
	}
	
	@Override
	public void addEnterMenuListener(int enterMenuEntry, Object enterMenuObject, ChangeListener listener) {
		this.enterMenuEntry = enterMenuEntry;
		this.enterMenuObject = enterMenuObject;
		addListener(listener);
	}
	
	@Override
	public int getEnterMenuEntry() {
		return enterMenuEntry;
	}
	@Override
	public Object getEnterMenuObject() {
		return enterMenuObject;
	}
	
	@Override
	public void act(float delta) {

		if (parentMenu != null && parentMenu.hasBeenMovedIn == true) {	// transparenz nur updaten wenn das menu im screen angezeigt wird
		
			if (parentMenu.menuIsActive == false && parentMenu.openedNavigationButton != this) {
				
				alpha = alpha - (ALPHAVELOCITY * delta);
				if (alpha < 0.25f) alpha = 0.25f;
			}
			else {
				if (alpha < 1f) {
					alpha = alpha + (ALPHAVELOCITY * delta);
					if (alpha > 1f) alpha = 1f;
				}
			}
		}
	}
	
	public void setImageDrawable(Drawable drawable) {
	
		getStyle().imageUp = drawable;
	}
		
	//-- fixedAlpha wird gesetzt von den optionsbutton (wenn deaktiv)
	public void setFixedAlpha(float alphaMultiplacator) {
		
		fixedAlpha = alphaMultiplacator;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {

		if (fixedAlpha > 0f) {
			super.draw(batch, fixedAlpha);
		}
		else super.draw(batch, parentAlpha * alpha);
		
		if (navigationIsActive && game.activateMenuSwitches == true) {
		
			batch.setColor(1f, 1f, 1f, 1f);	// alpha auf 1f zurueck da evtl durch halbtransparentes image sonst der marker auch halbtransparent ist
			
			if (navigationMarkerNinepatch != null) {	
				navigationMarkerNinepatch.draw(batch, getX(), getY(), getWidth(), getHeight());	// marker for levelselector (rectangle)
			}
			else if (parentMenu != null && parentMenu.menuMarker != null) {
				parentMenu.menuMarker.draw(batch, getX()-5, getY()-5, getWidth()+10, getHeight()+10, true); // marker for menubuttons (circle)
			}
			//---- draw text description if available --------------------
//			if (!description.equals("")) {
//					
//				ninePatchDrawableBackground.draw(batch, 
//						getX() + getWidth() + 10, 
//						getY(), 
//						Assets.font_small.getBounds(description).width + 15, 
//						Assets.font_small.getBounds(description).height + 15);
//				Assets.font_small.draw(batch, description, getX() + getWidth() + 15, getY() + getHeight() * 0.5f);
//			}
		}
	}

	@Override
	public void setNavigationIsActive (boolean isActive) {
		navigationIsActive = isActive;
	}

	@Override
	public boolean getNavigationIsActive () {
		return navigationIsActive;
	}
	
	@Override
	public int getNavigationIndex() {
		return navigationIndex;
	}
}
