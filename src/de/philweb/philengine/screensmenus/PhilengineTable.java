package de.philweb.philengine.screensmenus;

import java.util.ArrayList;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class PhilengineTable extends Table {

	Stage stage;
	public float posX;
	public float posY;
	public float outerPosX;
	public float outerPosY;
	public boolean hasBeenMovedIn = false;
	public Runnable runHasBeenMovedInTRUE;	//--- set boolean after animation has finished!
	public Runnable runHasBeenMovedInFALSE;	//--- set boolean after animation has finished!
//	public ArrayList<Actor> philengineMenuActors;			//---  liste wird im falle der aktivierung des submenues an den menunavgiegationmanager uebergeben
	public ArrayList<IphilengineUIActors> philengineMenuActors;			//---  liste wird im falle der aktivierung des submenues an den menunavgiegationmanager uebergeben
	public ArrayList<Integer> philengineMenuActorsTypes;	//---  liste wird im falle der aktivierung des submenues an den menunavgiegationmanager uebergeben
	public int navigationEntryCounter;
	public int navigationIsActiveEntry;
	
	public PhilengineTable (Skin skin, float posX, float posY, float outerPosX, float outerPosY) {
		
		super(skin);
		setPositions(posX, posY, outerPosX, outerPosY); 
		initialize(); 
	}
	
	/**
	 * this constructor is only for empty submenuScreen (like controllerConfig)
	 * @author andre
	 */
	public PhilengineTable (Stage stage) {
		super();
		this.stage = stage;
		setPositions(0, 0, 0, 0); 
		initialize(); 
	}
	
	public PhilengineTable (float posX, float posY, float outerPosX, float outerPosY, Stage stage) {
		 
		super();
		this.stage = stage;
		setPositions(posX, posY, outerPosX, outerPosY); 
		initialize(); 
	}
	
	
	public void setPositions(float posX, float posY, float outerPosX, float outerPosY) {
		this.posX = posX;
		this.posY = posY;
		this.outerPosX = outerPosX;
		this.outerPosY = outerPosY;
		
		setPosition(outerPosX, outerPosY);
	}
	
	void initialize() {
		philengineMenuActors = new ArrayList<IphilengineUIActors>();
		philengineMenuActorsTypes = new ArrayList<Integer>();
		navigationEntryCounter = 0;
		navigationIsActiveEntry = 0;
		
		//--- set boolean after animation has finished!
		runHasBeenMovedInTRUE = new Runnable() {
			public void run () {	
				setHasBeenMovedInTRUE();	}
		};
		//--- set boolean after animation has finished!
		runHasBeenMovedInFALSE = new Runnable() {
			public void run () {	
				setHasBeenMovedInFALSE();	}
		};	
	}
	
	
	public void setAlignCenter(int virtual_width_hud) {
		
		posX = (virtual_width_hud * 0.5f) - (getWidth() * 0.5f);
		outerPosX = posX;
		setPosition(outerPosX, outerPosY);
	}
	
	public void setNavigationIsActiveEntry(int entry) {
		if (entry > -1 && entry <= navigationEntryCounter) navigationIsActiveEntry = entry;
	}
	
	public void registerImageButton(PhilengineImageButton button) {
		
		button.navigationIndex = navigationEntryCounter;
		philengineMenuActors.add(button);
		philengineMenuActorsTypes.add(MgrMenuNavigationManager.IMAGEBUTTON);
		navigationEntryCounter ++;		
	}
	
	public void registerSlider(PhilengineSlider slider) {
		
		slider.navigationIndex = navigationEntryCounter;
		philengineMenuActors.add(slider);
		philengineMenuActorsTypes.add(MgrMenuNavigationManager.SLIDER);
		navigationEntryCounter ++;		
	}
	
	public void moveIn() {
		if (hasBeenMovedIn == false) {
			if (getStage() == null) {
				stage.addActor(this);
			}
			addAction(Actions.moveTo(posX, posY, 0.5f, Interpolation.exp10Out));
			addAction(Actions.run(runHasBeenMovedInTRUE));	//--- set boolean after animation has finished!
			doThingsAfterMoveIn();
		}
	}
	
	public void moveOut() {
		if (hasBeenMovedIn == true) {
			addAction(Actions.sequence(
					Actions.moveTo(outerPosX, outerPosY, 0.5f, Interpolation.exp10In) 
					, Actions.run(runHasBeenMovedInFALSE)	//--- set boolean after animation has finished!
					, Actions.removeActor(this)
					));
		}
	}
	
	public void moveIn(float duration, Interpolation interp) {
		if (hasBeenMovedIn == false) {
			addAction(Actions.moveTo(posX, posY, duration, interp));
			addAction(Actions.run(runHasBeenMovedInTRUE));	//--- set boolean after animation has finished!
			doThingsAfterMoveIn();
		}
	}
	
	public void moveOut(float duration, Interpolation interp) {
		if (hasBeenMovedIn == true) {
			addAction(Actions.moveTo(outerPosX, outerPosY, duration, interp));
			addAction(Actions.run(runHasBeenMovedInFALSE));	//--- set boolean after animation has finished!
		}
	}
		
	public void clearNavigation() {
		
//		philengineMenuActors = new ArrayList<Actor>();
		philengineMenuActors = new ArrayList<IphilengineUIActors>();
		philengineMenuActorsTypes = new ArrayList<Integer>();
		navigationEntryCounter = 0;
		navigationIsActiveEntry = 0;
	}
	
	public void doThingsAfterMoveIn() {
	}
	
	//--- set boolean after animation has finished!
	void setHasBeenMovedInTRUE() {
		hasBeenMovedIn = true;
	}
	//--- set boolean after animation has finished!
	void setHasBeenMovedInFALSE() {
		hasBeenMovedIn = false;
	}
	
	public void update(float delta) {
	}
}
